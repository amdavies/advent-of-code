using MoreLinq;
using MoreLinq.Extensions;

namespace AdventOfCode.Y2023.Solution;

using ParsedInputType = IEnumerable<(int id, IEnumerable<(int red, int green, int blue)> sets)>;

public class Day2 : APuzzleSolver<ParsedInputType>
{
    public Day2() : base(2, 2023, "Untitled")
    {
    }

    protected override ParsedInputType ParseInput() => this.Input.Split('\n').Select(l =>
    {
        var a = l.Split(new[] { "Game ", ": " }, StringSplitOptions.RemoveEmptyEntries);
        int id = int.Parse(a[0]);

        IEnumerable<(int red, int green, int blue)> sets = new List<(int red, int green, int blue)>();

        foreach (var set in a[1].Split("; "))
        {
            (int red, int green, int blue) balls = new();

            var c = set.Split(", ");
            foreach (var d in c)
            {
                var data = d.Split(' ');
                var count = int.Parse(data[0]);

                if (data[1] == "red") balls.red = count;
                if (data[1] == "green") balls.green = count;
                if (data[1] == "blue") balls.blue = count;
            }

            sets = MoreEnumerable.Append(sets, balls);
        }

        return (id, sets);
    });

    protected override async Task RunSolver()
    {
        int redLimit = 12;
        int greenLimit = 13;
        int blueLimit = 14;
        this.PartOneResult = this.ParsedInput.Where(game => game.sets.All(set =>
            {
                if (set.red > redLimit) return false;
                if (set.green > greenLimit) return false;
                if (set.blue > blueLimit) return false;
                return true;
            })
        ).Sum(game => game.id).ToString();


        this.PartTwoResult = this.ParsedInput.Sum(game =>
        {
            var maxRed = game.sets.Max(set => set.red);
            var maxGreen = game.sets.Max(set => set.green);
            var maxBlue = game.sets.Max(set => set.blue);

            return maxRed * maxGreen * maxBlue;
        }).ToString();
    }
}