using AdventOfCode.Enumeration;

namespace AdventOfCode.Y2023.Solution;

using ParsedInputType = IEnumerable<string>;

public class Day4 : APuzzleSolver<ParsedInputType>
{
    private bool UseTestInput = false;

    public Day4() : base(4, 2023, "Untitled")
    {
        if (UseTestInput) this.TestInput = @"Card 1: 41 48 83 86 17 | 83 86  6 31 17  9 48 53
Card 2: 13 32 20 16 61 | 61 30 68 82 17 32 24 19
Card 3:  1 21 53 59 44 | 69 82 63 72 16 21 14  1
Card 4: 41 92 73 84 69 | 59 84 76 51 58  5 54 83
Card 5: 87 83 26 28 32 | 88 30 70 12 93 22 82 36
Card 6: 31 18 13 56 72 | 74 77 10 23 35 67 36 11";
    }

    protected override ParsedInputType ParseInput() => this.Input.Split('\n');

    protected override async Task RunSolver()
    {
        int maxCard = this.ParsedInput.Count();
        Dictionary<int, int> cardCopyCount = new();

        double totalScore = 0;
        int totalCards = 0;

        int cardNo = 1;
        foreach (var card in this.ParsedInput)
        {
            var data = card.Split(new[] { ": ", " | " }, StringSplitOptions.RemoveEmptyEntries);

            var cardNumbers = data[1].ToIntArray(" ");
            var winningNumbers = data[2].ToIntArray(" ");

            var cardWinners = cardNumbers.Where(n => winningNumbers.Contains(n)).Count();

            if (cardWinners > 0)
            {
                totalScore += 2.Power(cardWinners - 1);
            }

            for (int copyNo = cardNo + 1; copyNo <= cardNo + cardWinners && copyNo <= maxCard; copyNo++)
            {
                cardCopyCount.Increment(copyNo, 1 + cardCopyCount.GetValueOrDefault(cardNo));
            }

            cardNo++;
        }

        this.PartOneResult = totalScore.ToString();
        this.PartTwoResult = (maxCard + cardCopyCount.Sum(c => c.Value)).ToString();
    }
}