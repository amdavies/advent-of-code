using System.Text.RegularExpressions;
using AdventOfCode.Grid;

namespace AdventOfCode.Y2023.Solution;

// using ParsedInputType = (Dictionary<int, MatchCollection> lineMatches, IEnumerable<Position> symbols);
using ParsedInputType = Tuple<Dictionary<int, MatchCollection>, List<Position>>;

public class Day3 : APuzzleSolver<ParsedInputType>
{
    public Day3() : base(3, 2023, "Untitled")
    {
    }

    protected override ParsedInputType ParseInput()
    {
        var symbolPositions = new List<Position>();
        var lineMatches = new Dictionary<int, MatchCollection>();

        int y = 0;
        foreach (var line in this.Input.Split('\n'))
        {
            var matches = Regex.Matches(line, @"\d+");
            lineMatches.Add(y, matches);

            for (int x = 0; x < line.Length; x++)
            {
                if (line[x] == '.' || line[x].IsNumber()) continue;
                symbolPositions.Add(new(x, y));
            }

            y++;
        }

        return (lineMatches, symbolPositions).ToTuple();
    }

    protected override async Task RunSolver()
    {
        int partSums = 0;
        foreach (var symbolPos in this.ParsedInput.Item2)
        {
            partSums += 0;
        }
        this.PartOneResult = "No Solution";
        this.PartTwoResult = "No Solution";
    }

    private IEnumerable<Position> getAdjacent(Position p)
    {
        return new[]
        {
            p + (-1, -1),
            p + (-1, 0),
            p + (-1, 1),
            p + (0, -1),
            p + (0, 1),
            p + (1, -1),
            p + (1, 0),
            p + (1, 1),
        };
    }
}