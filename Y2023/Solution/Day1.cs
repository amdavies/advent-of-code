using MoreLinq;

namespace AdventOfCode.Y2023.Solution;

using ParsedInputType = IEnumerable<string>;

public class Day1 : APuzzleSolver<ParsedInputType>
{
    public Day1() : base(1, 2023, "Untitled")
    {
    }

    protected override ParsedInputType ParseInput() => this.Input.Split("\n");

    protected override async Task RunSolver()
    {
        this.IsSolved = true;

        var numberChars = new[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
        var numberWords = new Dictionary<string, char>
        {
            { "one", '1' },
            { "two", '2' },
            { "three", '3' },
            { "four", '4' },
            { "five", '5' },
            { "six", '6' },
            { "seven", '7' },
            { "eight", '8' },
            { "nine", '9' },
        };

        int sum = 0;
        int wordSum = 0;
        foreach (var line in this.ParsedInput)
        {
            var nums = line.ToCharArray().Where(c => numberChars.Contains(c));
            sum += int.Parse(new string(new[] { nums.First(), nums.Last() }));

            if (numberWords.Keys.Any(line.Contains))
            {
                List<char> matches = new();
                for (int i = 0; i < line.Length; i++)
                {
                    if (numberChars.Contains(line[i]))
                    {
                        matches.Add(line[i]);
                        continue;
                    }

                    foreach (var word in numberWords)
                    {
                        int end = i + word.Key.Length;
                        if (end > line.Length) continue;
                        if (line.Substring(i, word.Key.Length) == word.Key)
                        {
                            matches.Add(word.Value);
                            break;
                        }
                    }
                }

                var lineNum = new string(new[] { matches.First(), matches.Last() });
                wordSum += int.Parse(lineNum);
            }
            else
            {
                wordSum += int.Parse(new string(new[] { nums.First(), nums.Last() }));
            }
        }

        this.PartOneResult = sum.ToString();
        this.PartTwoResult = wordSum.ToString();
    }
}