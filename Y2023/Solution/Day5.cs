using MoreLinq.Extensions;

namespace AdventOfCode.Y2023.Solution;

using ParsedInputType = IEnumerable<string>;

public class Day5 : APuzzleSolver<ParsedInputType>
{
    public Day5() : base(5, 2023, "Untitled")
    {
    }

    protected override ParsedInputType ParseInput() => this.Input.Split("\n\n");

    protected override async Task RunSolver()
    {
        this.PartOneResult = this.ParsedInput.Skip(1).First().Split('\n', 2).Last();
        this.PartTwoResult = "No Solution";
    }
}