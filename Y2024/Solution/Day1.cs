namespace AdventOfCode.Y2024.Solution;

using ParsedInputType = (IEnumerable<int> left, IEnumerable<int> right);

public class Day1 : APuzzleSolver<ParsedInputType>
{
    public Day1() : base(1, 2024, "Historian Hysteria")
    {
    }

    protected override ParsedInputType ParseInput()
    {
        List<int> left = new();
        List<int> right = new();

        foreach (var line in this.Input.Split("\n"))
        {
            var values = line.Split("   ");

            left.Add(int.Parse(values[0]));
            right.Add(int.Parse(values[1]));
        }

        left.Sort();
        right.Sort();

        return (left, right);
    }

    protected override async Task RunSolver()
    {
        this.IsSolved = true;

        var sum = this.ParsedInput.left.Zip(this.ParsedInput.right, (l, r) => (l - r).Abs()).Sum();
        this.PartOneResult = sum.ToString();

        var similarity = 0;
        foreach (var id in this.ParsedInput.left)
        {
            similarity += id * this.ParsedInput.right.Count(r => r == id);
        }
        this.PartTwoResult = similarity.ToString();
    }
}