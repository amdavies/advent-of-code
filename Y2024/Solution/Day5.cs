using MoreLinq;

namespace AdventOfCode.Y2024.Solution;

using ParsedInputType = (IEnumerable<(int before, int after)> rules, IEnumerable<int[]> groups);

public class Day5 : APuzzleSolver<ParsedInputType>
{
    private bool UseTestInput = false;

    public Day5() : base(5, 2024, "Print Queue")
    {
        if (UseTestInput) this.TestInput = @"47|53
97|13
97|61
97|47
75|29
61|13
75|53
29|13
97|29
53|29
61|53
97|53
61|29
47|13
75|47
97|75
47|61
75|61
47|29
75|13
53|13

75,47,61,53,29
97,61,53,29,13
75,29,13
75,97,47,61,53
61,13,29
97,13,75,29,47";
    }

    protected override ParsedInputType ParseInput()
    {
        var parts = this.Input.SplitBlock().ToArray();

        return (
            parts[0].Split("\n").Select(l => l.ToIntArray("|").ToTuple2()).ToArray(),
            parts[1].Split("\n").Select(l => l.ToIntArray(","))
                );
    }

    protected override async Task RunSolver()
    {
        int sum = 0;
        int fixedSum = 0;
        foreach (var group in this.ParsedInput.groups)
        {
            bool valid = true;
            var groupSet = group.ToHashSet();

            var rules = this.ParsedInput.rules.Where(rule => groupSet.Contains(rule.before) && groupSet.Contains(rule.after)).ToHashSet();
            foreach (var rule in rules)
            {
                var end = group.Skip(Array.IndexOf(group, rule.before) + 1);
                if (end.Contains(rule.after)) continue;
                valid = false;
                break;
            }

            if (valid)
            {
                sum += group.Skip(group.Length / 2).First();
            }
            else
            {
                var fixedGroup = group.ToList();
                fixedGroup.Sort((a, b) => rules.Contains((a, b)) ? 1 : -1);
                fixedSum += fixedGroup.Skip(fixedGroup.Count / 2).First();
            }
        }
        this.PartOneResult = sum.ToString();
        this.PartTwoResult = fixedSum.ToString();
    }
}