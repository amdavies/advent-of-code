using AdventOfCode.Enumeration;
using MoreLinq;

namespace AdventOfCode.Y2024.Solution;

using ParsedInputType = IEnumerable<(long target, long[] numbers)>;

public class Day7 : APuzzleSolver<ParsedInputType>
{
    private bool UseTestInput = false;

    public Day7() : base(7, 2024, "Untitled")
    {
        if (UseTestInput) this.TestInput = @"190: 10 19
3267: 81 40 27
83: 17 5
156: 15 6
7290: 6 8 6 15
161011: 16 10 13
192: 17 8 14
21037: 9 7 18 13
292: 11 6 16 20";
    }

    protected override ParsedInputType ParseInput() => this.Input.Split("\n").Select(l =>
    {
        var (target, rest) = l.Split(": ");

        return (long.Parse(target), rest.Split(" ").Select(long.Parse).ToArray());
    });

    protected override async Task RunSolver()
    {
        this.PartOneResult = this.ParsedInput.Where(eq => CanMakeFast(eq.target, eq.numbers)).Sum(eq => eq.target).ToString();
        this.PartTwoResult = this.ParsedInput.Where(eq => CanMakeFast(eq.target, eq.numbers, true)).Sum(eq => eq.target).ToString();
    }

    // Initial solution, really slow
    private bool CanMake(long target, long[] numbers, int step, long currentValue, bool allowConcat = false)
    {
        if (currentValue > target) return false;
        if (numbers.Length == step) return currentValue == target;

        return CanMake(target, numbers, step + 1, currentValue + numbers[step], allowConcat) ||
               (currentValue > 0 &&
                (CanMake(target, numbers, step + 1, currentValue * numbers[step], allowConcat) ||
                 (allowConcat && CanMake(target, numbers, step + 1, currentValue.Concat(numbers[step]), allowConcat)))
               );
    }

    // Works backwards from the target, can end searches much quicker
    private bool CanMakeFast(long target, long[] numbers, bool allowConcat = false)
    {
        if (numbers.Length == 1) return numbers[0] == target;

        var last = numbers[^1];
        var remaining = numbers[..^1];
        if (target % last == 0 && CanMakeFast(target / last, remaining, allowConcat)) return true;
        if (target - last > 0 && CanMakeFast(target - last, remaining, allowConcat)) return true;
        if (!allowConcat) return false;

        var tString = target.ToString();
        var lString = last.ToString();
        return tString.Length > lString.Length &&
               tString.EndsWith(lString) &&
               this.CanMakeFast(long.Parse(tString[..^lString.Length]), remaining, allowConcat);
    }
}