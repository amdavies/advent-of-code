using MoreLinq.Extensions;

namespace AdventOfCode.Y2024.Solution;

using ParsedInputType = IEnumerable<IEnumerable<int>>;

public class Day2 : APuzzleSolver<ParsedInputType>
{
    public Day2() : base(2, 2024, "Red-Nosed Reports")
    {
        /*
        this.TestInput = """
                         7 6 4 2 1
                         1 2 7 8 9
                         9 7 6 2 1
                         1 3 2 4 5
                         8 6 4 4 1
                         1 3 6 7 9
                         """;*/
    }

    protected override ParsedInputType ParseInput() => this.Input.Split("\n").Select(i => i.ToIntArray(" "));

    protected override async Task RunSolver()
    {
        this.PartOneResult = this.ParsedInput.Count(IsSafe).ToString();
        this.PartTwoResult = this.ParsedInput.Count(report =>
        {
            // Use part1
            var reportList = report.ToList();
            if (IsSafe(reportList)) return true;

            // Build list of reports with 1 value missing
            var options = new List<List<int>>();
            for (var toRemove = 0; toRemove < reportList.Count; toRemove++)
            {
                List<int> tmp = [];
                if (toRemove > 0)
                {
                    tmp.AddRange(reportList.Take(toRemove));
                }

                tmp.AddRange(reportList.Skip(toRemove + 1).Take(reportList.Count - toRemove - 1));
                options.Add(tmp);
            }

            return options.Any(this.IsSafe);
        }).ToString();
    }

    private bool IsSafe(IEnumerable<int> report)
    {
        var steps = report.Pairwise((a, b) => (difference: (a - b).Abs(), increment: a > b));
        if (steps.Any(step => step.difference > 3 || step.difference == 0)) return false;
        var incCount = steps.Count(step => step.increment);
        return incCount == 0 || incCount == steps.Count();
    }
}