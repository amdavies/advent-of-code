using AdventOfCode.Enumeration;
using AdventOfCode.Grid;

namespace AdventOfCode.Y2024.Solution;

using ParsedInputType = Dictionary<Position, char>;

public class Day6 : APuzzleSolver<ParsedInputType>
{
    private bool UseTestInput = false;

    private (int X, int Y) gridMax;

    public Day6() : base(6, 2024, "Guard Gallivant")
    {
        if (UseTestInput) this.TestInput = @"....#.....
.........#
..........
..#.......
.......#..
..........
.#..^.....
........#.
#.........
......#...";
    }

    protected override ParsedInputType ParseInput()
    {
        ParsedInputType map = new();
        var lines = this.Input.Split("\n");
        int x = 0;
        int y = lines.Length - 1;
        foreach (var line in lines)
        {
            x = 0;
            foreach (char c in line)
            {
                map.Add((x++, y), c);
            }

            y--;
        }

        this.gridMax = (x - 1, lines.Length - 1);
        return map;
    }

    protected override async Task RunSolver()
    {
        var map = this.ParsedInput;
        var guardNode = map.First(kvp => kvp.Value is not ('.' or '#'));
        var guardPos = guardNode.Key;
        var guardDir = guardNode.Value;
        map[guardPos] = '.'; // Remove guard from the actual map

        var visited = new HashSet<Position>();
        while (guardPos >= (0, 0) && guardPos <= this.gridMax)
        {
            visited.Add(guardPos);
            var nextPos = guardPos.AfterMove(guardDir);
            if (!map.ContainsKey(nextPos))
            {
                break;
            }

            while (map[nextPos] == '#')
            {
                guardDir = guardDir.RotateRight();
                nextPos = guardPos.AfterMove(guardDir);
            }

            guardPos = nextPos;
        }
        this.PartOneResult = visited.Count.ToString();

        // Make some infinite loops?
        var possibleObstacles = visited.Where(pos => pos != guardNode.Key);
        this.PartTwoResult = possibleObstacles.Count(obstacle =>
        {
            // Reset Guard
            guardPos = guardNode.Key;
            guardDir = guardNode.Value;

            // Run through map
            var visitedState = new HashSet<(Position, char)>();
            while (guardPos >= (0, 0) && guardPos <= this.gridMax)
            {
                Position nextPos = guardPos;
                do
                {
                    guardPos = nextPos;
                    nextPos = guardPos.AfterMove(guardDir);
                    if (!map.ContainsKey(nextPos))
                    {
                        return false;
                    }
                } while (nextPos != obstacle && map[nextPos] != '#');

                if ((obstacle == nextPos || map[nextPos] == '#') && !visitedState.Add((guardPos, guardDir)))
                {
                    // Been here in this direction before, loop detected
                    return true;
                }

                while (obstacle == nextPos || map[nextPos] == '#')
                {
                    guardDir = guardDir.RotateRight();
                    nextPos = guardPos.AfterMove(guardDir);
                }

                guardPos = nextPos;
            }

            return false;
        }).ToString();
    }
}