using AdventOfCode.Grid;

namespace AdventOfCode.Y2024.Solution;

using ParsedInputType = TwoD<char>;

public class Day4 : APuzzleSolver<ParsedInputType>
{
    public Day4() : base(4, 2024, "Ceres Search")
    {
        // this.TestInput = """
        //                  MMMSXXMASM
        //                  MSAMXMSMSA
        //                  AMXSXMAAMM
        //                  MSAMASMSMX
        //                  XMASAMXAMM
        //                  XXAMMXXAMA
        //                  SMSMSASXSS
        //                  SAXAMASAAA
        //                  MAMMMXMMMM
        //                  MXMXAXMASX
        //                  """;
    }

    protected override ParsedInputType ParseInput() => this.Input.To2DGrid();

    protected override async Task RunSolver()
    {
        Position[] allSteps =
        [
            (0, 1),
            (0, -1),
            (1, 0),
            (-1, 0),
            (1,1),
            (-1,-1),
            (1,-1),
            (-1,1),
        ];

        var grid = this.ParsedInput;

        this.PartOneResult = grid.Where(kvp => kvp.Value == 'X').Sum(kvp =>
        {
            return allSteps.Count(step => grid.PositionEquals(kvp.Key + step, 'M') &&
                                           grid.PositionEquals(kvp.Key + step * 2, 'A') &&
                                           grid.PositionEquals(kvp.Key + step * 3, 'S'));
        }).ToString();

        (int,int)[] xSteps =
        [
            (1,1),
            (-1,-1),
            (-1,1),
            (1,-1),
        ];
        this.PartTwoResult = grid.Where(kvp => kvp.Value == 'A').Sum(kvp =>
        {
            var count = xSteps.Count(pos => grid.PositionEquals(kvp.Key + pos, 'M') && grid.PositionEquals(kvp.Key - pos, 'S'));

            return count == 2 ? 1 : 0;
        }).ToString();
    }
}