using System.Text.RegularExpressions;

namespace AdventOfCode.Y2024.Solution;

using ParsedInputType = string;

public class Day3 : APuzzleSolver<ParsedInputType>
{
    public Day3() : base(3, 2024, "Mull It Over")
    {
        // this.TestInput = "xmul(2,4)&mul[3,7]!^don't()_mul(5,5)+mul(32,64](mul(11,8)undo()?mul(8,5))";
    }

    protected override ParsedInputType ParseInput() => this.Input;

    protected override async Task RunSolver()
    {
        var mults = new Regex(@"mul\(\d+,\d+\)");
        var ops = mults.Matches(this.ParsedInput);

        this.PartOneResult = ops.Sum(match =>
        {
            var vals = match.Value.Replace("mul(", "").Replace(")", "").Split(",").Select(int.Parse).ToList();

            return vals.First() * vals.Last();
        }).ToString();


        var conditions = new Regex(@"do\(\)|don\'t\(\)");
        var triggers = conditions.Matches(this.ParsedInput);

        this.PartTwoResult = ops.Where(match => IsEnabled(match.Index)).Sum(match =>
        {
            var vals = match.Value.Replace("mul(", "").Replace(")", "").Split(",").Select(int.Parse).ToArray();

            return vals[0] * vals[1];
        }).ToString();;

        bool IsEnabled(int index)
        {
            bool prev = true;
            foreach (Match trigger in triggers)
            {
                if (trigger.Index > index)
                {
                    return prev;
                }

                prev = trigger.Value == "do()";
            }
            return prev;
        }
    }
}