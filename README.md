# Advent of Code

This project contains my (final) solutions for advent of code. Check the git history if you're interested
in finding out how my original solutions looked before attempting to optimize.

## Usage

This project allows directing running of a day part from the command, or selecting through a CLI wizard.
All documentation below assumes you are operating within the `Runner` directory.

### Setup

Creation a `.AoCSession` file containing your adventofcode.com session cookie. This will allow automated
downloads of puzzle inputs. Alternatively, manually save inputs for all days in `input/{day}.in`.

### Running all days

Running all of the current solutions can be achieved with:

```bash
dotnet run all [year]
```

This will output the answer for both parts for every day. If no year is defined, the most recent year will be used.

### Running an individual day/part

Running the application with no arguments (`dotnet run`) will prompt for a year, day, and parts to run
using an interactive list than can be navigated with they keyboard arrow keys.

Alternatively, supplying a `{year}.{day}[.{part}]` argument such as `2019.5.2` or `2020.7` will run just the specified
day & part(s). There is also a `today` dynamic identifier to allow running the most recent solved day without
supplying the day number.

## Direction

It is intended this project is extended to contain solutions for more years of puzzles as they are created
as well as a unified library of utilities that will make puzzle solving easier for myself going forward.


