using NUnit.Framework;

namespace AdventOfCode.Y2022.Test;

public abstract class APuzzleTest<T> where T : APuzzle, new()
{
    private T Problem = new();

    [SetUp]
    public void Setup()
    {
        this.Problem = new();
    }

    #pragma warning disable NUnit1011 // The TestCaseSource argument does not specify an existing member
    [Test, TestCaseSource("Part1Data")]
    public void TestPart1(string input, string expected)
    {
        this.Problem.TestInput = input;
        Assert.That(this.Problem.Part1, Is.EqualTo(expected));
    }

    [Test, TestCaseSource("Part2Data")]
    public void TestPart2(string input, string expected)
    {
        this.Problem.TestInput = input;
        Assert.That(this.Problem.Part2, Is.EqualTo(expected));
    }
    #pragma warning restore NUnit1011
}