namespace AdventOfCode.Y2021.Test;

public class Day17Test : APuzzleTest<Day17>
{
    public static object[] Part1Data => new object[] {
        new[] { TestData, "45" },
        new[] { RealData, "9870" },
    };

    public static object[] Part2Data => new object[] {
        new[] { TestData, "112" },
        new[] { RealData, "5523" },
    };

    private static string TestData => @"target area: x=20..30, y=-10..-5";
    private static string RealData => @"target area: x=119..176, y=-141..-84";
}
