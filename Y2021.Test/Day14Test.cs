namespace AdventOfCode.Y2021.Test;

public class Day14Test : APuzzleTest<Day14>
{
    public static object[] Part1Data => new object[] {
        new[] { TestData, "1588" },
        new[] { RealData, "3009" },
    };

    public static object[] Part2Data => new object[] {
        new[] { TestData, "2188189693529" },
        new[] { RealData, "3459822539451" },
    };

    private static string TestData => @"NNCB

CH -> B
HH -> N
CB -> H
NH -> C
HB -> C
HC -> B
HN -> C
NN -> C
BH -> H
NC -> B
NB -> B
BN -> B
BB -> N
BC -> B
CC -> N
CN -> C";
    private static string RealData => @"BNBBNCFHHKOSCHBKKSHN

CH -> S
KK -> V
FS -> V
CN -> P
VC -> N
CB -> V
VK -> H
CF -> N
PO -> O
KC -> S
HC -> P
PP -> B
KO -> B
BK -> P
BH -> N
CC -> N
PC -> O
FK -> N
KF -> F
FH -> S
SS -> V
ON -> K
OV -> K
NK -> H
BO -> C
VP -> O
CS -> V
KS -> K
SK -> B
OP -> S
PK -> S
HF -> P
SV -> P
SB -> C
BC -> C
FP -> H
FC -> P
PB -> N
NV -> F
VO -> F
VH -> P
BB -> N
SF -> F
NB -> K
KB -> S
VV -> S
NP -> N
SO -> O
PN -> B
BP -> H
BV -> V
OB -> C
HV -> N
PF -> B
SP -> N
HN -> N
CV -> H
BN -> V
PS -> V
CO -> S
BS -> N
VB -> H
PV -> P
NN -> P
HS -> C
OS -> P
FB -> S
HO -> C
KH -> H
HB -> K
VF -> S
CK -> K
FF -> H
FN -> P
OK -> F
SC -> B
HH -> N
OH -> O
VS -> N
FO -> N
OC -> H
NF -> F
PH -> S
HK -> K
NH -> H
FV -> S
OF -> V
NC -> O
HP -> O
KP -> B
BF -> N
NO -> S
CP -> C
NS -> N
VN -> K
KV -> N
OO -> V
SN -> O
KN -> C
SH -> F";
}