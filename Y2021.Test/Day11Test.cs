using AdventOfCode.Y2021.Solution;

namespace AdventOfCode.Y2021.Test;

public class Day11Test : APuzzleTest<Day11>
{
    public static object[] Part1Data => new object[] {
        new[] { TestData, "1656" },
        new[] { RealData, "1585" },
    };

    public static object[] Part2Data => new object[] {
        new[] { TestData, "195" },
        new[] { RealData, "382" },
    };

    private static string TestData => @"5483143223
2745854711
5264556173
6141336146
6357385478
4167524645
2176841721
6882881134
4846848554
5283751526";
    private static string RealData => @"6636827465
6774248431
4227386366
7447452613
6223122545
2814388766
6615551144
4836235836
5334783256
4128344843";
}