using AdventOfCode.Y2021.Solution;

namespace AdventOfCode.Y2021.Test;

public class Day12Test : APuzzleTest<Day12>
{
    public static object[] Part1Data => new object[] {
        new[] { TestData, "10" },
        new[] { TestDataLarger, "19" },
        new[] { TestDataLargest, "226" },
        new[] { RealData, "4549" },
    };

    public static object[] Part2Data => new object[] {
        new[] { TestData, "36" },
        new[] { RealData, "120535" },
    };

    private static string TestData => @"start-A
start-b
A-c
A-b
b-d
A-end
b-end";
    
    private static string TestDataLarger => @"dc-end
HN-start
start-kj
dc-start
dc-HN
LN-dc
HN-end
kj-sa
kj-HN
kj-dc";

    private static string TestDataLargest => @"fs-end
he-DX
fs-he
start-DX
pj-DX
end-zg
zg-sl
zg-pj
pj-he
RW-he
fs-DX
pj-RW
zg-RW
start-pj
he-WI
zg-he
pj-fs
start-RW";
    
    private static string RealData => @"ma-start
YZ-rv
MP-rv
vc-MP
QD-kj
rv-kj
ma-rv
YZ-zd
UB-rv
MP-xe
start-MP
zd-end
ma-UB
ma-MP
UB-xe
end-UB
ju-MP
ma-xe
zd-UB
start-xe
YZ-end";
}