namespace AdventOfCode.Y2021.Test;

public class Day21Test : APuzzleTest<Day21>
{
    public static object[] Part1Data => new object[] {
        new[] { TestData, "739785" },
        new[] { RealData, "432450" },
    };

    public static object[] Part2Data => new object[] {
        new[] { TestData, "444356092776315" },
        new[] { RealData, "138508043837521" },
    };

    private static string TestData => @"Player 1 starting position: 4
Player 2 starting position: 8";
    private static string RealData => @"Player 1 starting position: 1
Player 2 starting position: 5";
}
