﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using Pastel;

namespace AdventOfCode.Y2019.Solution
{
    public class Day8 : APuzzle
    {
        public Day8() : base(8, 2019, "Space Image Format") { }

        protected override async Task<string> SolvePartOne()
        {
            const double layerSize = 25 * 6;
            var i = 0;
            var layers = this.Input
                .ToLookup(c => Math.Floor(i++ / layerSize))
                .Select(e => new string(e.ToArray()));

            var checkLayer = layers.OrderBy(l => l.Count(c => c == '0')).First();

            return (checkLayer.Count(c => c == '1') * checkLayer.Count(c => c == '2')).ToString();
        }

        protected override async Task<string> SolvePartTwo()
        {
            const double layerSize = 25 * 6;
            var i = 0;
            var layers = this.Input
                .ToLookup(c => Math.Floor(i++ / layerSize))
                .Select(e => new string(e.ToArray()))
                .ToArray();

            var colors = new Queue<Color>(new[]
            {
                Color.Red,
                Color.OrangeRed,
                Color.Yellow,
                Color.Green,
                Color.FromArgb(0, 127, 128),
                Color.DarkBlue,
            });

            string output = "\n";
            for (var y = 0; y < 6; y++) {
                var color = colors.Dequeue();
                for (var x = 0; x < 25; x++) {
                    var pixelNum = y * 25 + x;
                    var pixel = '2';
                    foreach (var layer in layers) {
                        if (layer[pixelNum] == '2') continue;

                        pixel = layer[pixelNum];
                        break;
                    }

                    output += " ".PastelBg(pixel == '1' ? color : Color.Black);
                }

                output += "\n";
            }

            // Return the image string
            return output;
        }
    }
}
