﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdventOfCode.Y2019.Solution
{
    public class Day16 : APuzzle
    {
        public Day16() : base(16, 2019, "Flawed Frequency Transmission") { }

        protected override async Task<string> SolvePartOne()
        {
            var phaseCount = 100;

            var value = this.Input.ToCharArray().Select(c => int.Parse(c.ToString())).ToArray();
            for (var phase = 0; phase < phaseCount; phase++) {
                var newValue = new int[value.Length];
                // Apply pattern to digit
                for (var i = 0; i < value.Length; i++) {
                    var sum = 0;
                    for (var digit = 0; digit < value.Length; digit++)
                        sum += value[digit] * GetMultiplier(i + 1, digit);

                    newValue[i] = Math.Abs(sum) % 10;
                }

                value = newValue;
            }

            return string.Join("", value).Substring(0, 8);
        }

        protected override async Task<string> SolvePartTwo()
        {
            var sb = new StringBuilder();
            for (var i = 0; i < 10000; i++) sb.Append(this.Input);

            var signal = sb.ToString();
            var outputOffset = int.Parse(signal.Substring(0, 7));

            // Due to "creeping zeroes", earlier digits can't affect later digits. Chop off everything before the offset
            long[] value = signal.Substring(outputOffset).ToLongArray();

            // By the same logic, everything else turns to a "* 1" so we can just start summing digits instead
            for (var phase = 0; phase < 100; phase++) {
                long[] newValue = new long[value.Length];
                var s = Math.Abs(value.Sum());
                for (var i = 0; i < value.Length; i++) {
                    newValue[i] = s % 10;
                    // Each digit is the sum of remaining digits, subtract the one we just calculated to save us recalculating
                    s -= value[i];
                }

                value = newValue;
            }

            return string.Join("", value).Substring(0, 8);
        }

        private static int GetMultiplier(int step, int digit)
        {
            digit = (digit + 1) % (step * 4);

            if (digit < step) return 0;

            if (digit < step * 2) return 1;

            if (digit < step * 3) return 0;

            return -1;
        }
    }
}
