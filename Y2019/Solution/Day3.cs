using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AdventOfCode.Grid;

namespace AdventOfCode.Y2019.Solution
{
    public class Day3 : APuzzle
    {
        private string[] wires = Array.Empty<string>();

        public Day3() : base(3, 2019, "Crossed Wires") { }

        private void CalculateWires()
        {
            if (this.wires.Length > 0) return;

            this.wires = this.Input.Split('\n');
        }

        /// <inheritdoc/>
        protected override async Task<string> SolvePartOne()
        {
            this.CalculateWires();

            TwoD<int> wireGrid = new();
            List<int> intersections = new();

            var wireNumber = 1;
            foreach (string wire in this.wires) {
                Position wirePosition = new(0, 0);
                string[] steps = wire.Split(',');

                foreach (string step in steps) {
                    var direction = step[0];
                    var distance = int.Parse(step.Substring(1));
                    var stepPos = wirePosition.AfterMove('X', 0);
                    wirePosition = wirePosition.AfterMove(direction, distance);

                    for (var i = 0; i < distance; i++) {
                        stepPos = stepPos.AfterMove(direction, 1);

                        if (wireGrid.ContainsKey(stepPos) && wireGrid[stepPos] != wireNumber) {
                            var dist = Math.Abs(stepPos.X) + Math.Abs(stepPos.Y);
                            intersections.Add(dist);
                        } else {
                            wireGrid.TryAdd(stepPos, wireNumber);
                        }
                    }
                }

                wireNumber++;
            }

            return intersections.Min().ToString();
        }

        /// <inheritdoc/>
        protected override async Task<string> SolvePartTwo()
        {
            this.CalculateWires();

            var wireGrid = new TwoD<(int wire, int steps)>();
            List<int> intersections = new();

            var wireNumber = 1;
            foreach (string wire in this.wires) {
                Position wirePosition = new(0, 0);
                string[] steps = wire.Split(',');
                var stepCount = 0;

                foreach (string step in steps) {
                    var direction = step[0];
                    var distance = int.Parse(step.Substring(1));
                    var stepPos = wirePosition.AfterMove('X', 0);
                    wirePosition = wirePosition.AfterMove(direction, distance);

                    for (var i = 0; i < distance; i++) {
                        stepCount++;
                        stepPos = stepPos.AfterMove(direction, 1);

                        if (wireGrid.ContainsKey(stepPos) && wireGrid[stepPos].wire != wireNumber)
                            intersections.Add(stepCount + wireGrid[stepPos].steps);
                        else if (!wireGrid.ContainsKey(stepPos)) wireGrid.TryAdd(stepPos, (wireNumber, stepCount));
                    }
                }

                wireNumber++;
            }

            return intersections.Min().ToString();
        }
    }
}
