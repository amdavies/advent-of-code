using System.Linq;
using System.Threading.Tasks;
using AdventOfCode.Y2019.Intcode;

namespace AdventOfCode.Y2019.Solution
{
    public class Day5 : APuzzle
    {
        public Day5() : base(5, 2019, "Sunny with a Chance of Asteroids") { }

        protected override async Task<string> SolvePartOne()
        {
            var vm = new Vm(this.Input, new InputNumber(1));
            vm.Run();

            return vm.Output.First(o => o > 0).ToString();
        }

        protected override async Task<string> SolvePartTwo()
        {
            var vm = new Vm(this.Input, new InputNumber(5));
            vm.Run();

            return vm.Output.First(o => o > 0).ToString();
        }
    }
}
