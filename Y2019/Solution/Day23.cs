using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using AdventOfCode.Y2019.Intcode;
using AdventOfCode.Y2019.Intcode.Networking;

namespace AdventOfCode.Y2019.Solution
{
    public class Day23 : APuzzle
    {
        public Day23() : base(23, 2019, "Category Six") { }

        protected override async Task<string> SolvePartOne()
        {
            var cts = new CancellationTokenSource();
            var network = new Network(cts);
            var machines = new List<Vm>();

            for (var i = 0; i < 50; i++) {
                var bus = new NetworkBus(i, network);
                machines.Add(new Vm(this.Input, bus, bus));
            }

            while (!cts.Token.IsCancellationRequested)
                foreach (var machine in machines)
                    machine.Step();

            return network.Vars[1].ToString();
        }

        protected override async Task<string> SolvePartTwo()
        {
            var cts = new CancellationTokenSource();
            var network = new Network(cts, true);
            var machines = new List<Vm>();

            for (var i = 0; i < 50; i++) {
                var bus = new NetworkBus(i, network);
                machines.Add(new Vm(this.Input, bus, bus));
            }

            while (!cts.Token.IsCancellationRequested)
                foreach (var machine in machines)
                    machine.Step();

            return network.Vars[1].ToString();
        }
    }
}