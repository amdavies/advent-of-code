﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;

namespace AdventOfCode.Y2019.Solution {
    /// <summary>
    /// Optimizations used as part of solve.
    ///     - Premap all step counts between any pair of keys using a flood fill, storing any doors in the way
    ///         - Assumes the shortest route between keys will always be the quickest including collection of required key
    ///         - There are scenarios where this assumption would be wrong, or result in a deadlock where the shortest paths are blocked
    ///     - Greedy caching based on current state to prevent recalculating known step counts
    ///         - Always operates on a current state => end basis
    ///     - Storing key data using a bitmask to speed up operations
    ///         - Cache key generation, requires no sorting
    ///         - No set lookups required, only bit level operations
    ///         - Required some hackery to make work with existing logic and handling of starting positions
    /// </summary>
    public class Day18 : APuzzle
    {
        private Dictionary<Point, char> Map = new();
        private Dictionary<KeySets, Dictionary<KeySets, (KeySets source, int steps, KeySets requiredKeys)>> KeySteps = new();
        private Dictionary<KeySets, Point> Keys = new();
        private List<Point> Doors = new();
        private Point Position;

        private readonly Dictionary<(KeySets, KeySets), int> pathCache = new();

        public Day18() : base(18, 2019, "Many-Worlds Interpretation") { }

        protected override async Task<string> SolvePartOne()
        {
            this.BuildMap();

            return this.FindKeysFrom(KeySets.Start5, KeySets.None).ToString();
        }

        protected override async Task<string> SolvePartTwo()
        {
            this.BuildMap(2);

            return this.FindKeysFrom(KeySets.Start1 | KeySets.Start2 | KeySets.Start3 | KeySets.Start4, KeySets.None).ToString();
        }

        private int FindKeysFrom(KeySets currentKeys, KeySets keysCollected)
        {
            var bestMin = 0;

            // Cache based on current keys (where we are), and which keys we've already selected
            var cacheKey = (currentKeys, keysCollected);

            if (this.pathCache.ContainsKey(cacheKey)) {
                return this.pathCache[cacheKey];
            }

            var options = this.KeySteps
                .Where(s => (currentKeys & s.Key) > 0)
                .SelectMany(s => s.Value)
                .Where(s => (keysCollected & s.Key) == 0 && (keysCollected & s.Value.requiredKeys) == s.Value.requiredKeys);

            foreach (var (key, info) in options) {
                int minSteps = info.steps;

                var newKeys = keysCollected | key;
                // If our current keyset doesn't contain all valid keys (Q is first special key)
                if (newKeys != KeySets.All) {
                    minSteps += this.FindKeysFrom((currentKeys & ~info.source) | key, newKeys);
                }

                if (bestMin == 0 || minSteps < bestMin) {
                    bestMin = minSteps;
                }
            }

            // Given a set of options, we can cache the best number of steps from that point
            this.pathCache.Add(cacheKey, bestMin);

            return bestMin;
        }

        private void BuildKeySteps(int part = 1)
        {
            this.KeySteps = new Dictionary<KeySets, Dictionary<KeySets, (KeySets source, int steps, KeySets requiredKeys)>>();
            if (part == 1) {
                this.FindKeySteps(this.Position, KeySets.Start5, false);
            } else {
                // Part 2 has 4 robots, each could be blocked from the start so we need to fetch all routes and then filter as we would for key => key
                this.FindKeySteps(new Point(39, 39), KeySets.Start1);
                this.FindKeySteps(new Point(41, 39), KeySets.Start2);
                this.FindKeySteps(new Point(39, 41), KeySets.Start3);
                this.FindKeySteps(new Point(41, 41), KeySets.Start4);
            }
            foreach (var (key, position) in this.Keys) {
                this.FindKeySteps(position, key);
            }
        }

        /// <summary>
        /// Find best step count between any two keys
        /// </summary>
        /// <param name="position">Starting position</param>
        /// <param name="key">Key name</param>
        /// <param name="ignoreDoors">If true, assume we have required keys and store the door in the required list</param>
        private void FindKeySteps(Point position, KeySets key, bool ignoreDoors = true)
        {
            var seen = new Dictionary<Point, (int steps, KeySets requiredKeys)>() {
                { position, (steps: 0, 0) }
            };
            var options = new Heap<(Point position, int steps)>(((point, point1) => point.steps - point1.steps));
            options.Insert((position, 0));

            while (options.Count > 0) {
                var point = options.Pop().position;

                var nextPoints = new[] {
                    new Point(point.X + 1, point.Y),
                    new Point(point.X - 1, point.Y),
                    new Point(point.X, point.Y + 1),
                    new Point(point.X, point.Y - 1),
                };

                foreach (Point nextPoint in nextPoints) {
                    if (this.Map.ContainsKey(nextPoint) && this.Map[nextPoint] != '#') {
                        if (!seen.ContainsKey(nextPoint)) {
                            // Skip this if it's a door that we're not ignoring
                            if (!ignoreDoors && this.Doors.Contains(nextPoint)) {
                                continue;
                            }

                            // Store point in list with doors required to get here
                            seen[nextPoint] = (
                                steps: seen[point].steps + 1,
                                requiredKeys: this.Doors.Contains(nextPoint)
                                    ? seen[point].requiredKeys | Enum.Parse<KeySets>(char.ToLower(this.Map[nextPoint]).ToString())
                                    : seen[point].requiredKeys
                            );

                            // If we found a key, store it
                            if (Enum.TryParse<KeySets>(this.Map[nextPoint].ToString(), out var keyFlag) && this.Keys.ContainsKey(keyFlag)) {
                                if (!this.KeySteps.ContainsKey(key)) {
                                    this.KeySteps[key] = new Dictionary<KeySets, (KeySets source, int steps, KeySets requiredKeys)>();
                                }

                                this.KeySteps[key][keyFlag] = (key, seen[nextPoint].steps, seen[nextPoint].requiredKeys);
                            }

                            options.Insert((nextPoint, seen[nextPoint].steps));
                        }
                    }
                }
            }
        }

        private void BuildMap(int part = 1)
        {
            // Reset data structures
            this.Map = new Dictionary<Point, char>();
            this.Keys = new Dictionary<KeySets, Point>();
            this.Doors = new List<Point>();

            int y = 0;
            foreach (var line in this.Input.Split('\n')) {
                int x = 0;

                foreach (var tile in line.ToCharArray()) {
                    var location = new Point(x, y);

                    if (tile == '@') {
                        this.Position = location;
                    }

                    if (tile >= 'a' && tile <= 'z') {
                        var keyFlag = Enum.Parse<KeySets>(tile.ToString());
                        this.Keys.Add(keyFlag, location);
                    }

                    if (tile >= 'A' && tile <= 'Z') {
                        this.Doors.Add(location);
                    }

                    this.Map.Add(location, tile);

                    x++;
                }

                y++;
            }

            // Part 2 rewrites the map, hardcode the change based on known positions
            if (part == 2) {
                // Add new walls, ignore new starting positions because we don't actually care what they look like in our map
                this.Map[new Point(40, 39)] = '#';
                this.Map[new Point(39, 40)] = '#';
                this.Map[new Point(40, 40)] = '#';
                this.Map[new Point(41, 40)] = '#';
                this.Map[new Point(40, 41)] = '#';
            }

            this.BuildKeySteps(part);
        }

        /// <summary>
        /// Create a flags enum for keys to allow really quick operations on a set of keys.
        /// </summary>
        [Flags]
        private enum KeySets
        {
            // ReSharper disable method UnusedMember.Local
            None = 0,
            All = 0x3FFFFFF,
            a = 0x1,
            b = 0x2,
            c = 0x4,
            d = 0x8,
            e = 0x10,
            f = 0x20,
            g = 0x40,
            h = 0x80,
            i = 0x100,
            j = 0x200,
            k = 0x400,
            l = 0x800,
            m = 0x1000,
            n = 0x2000,
            o = 0x4000,
            p = 0x8000,
            q = 0x10000,
            r = 0x20000,
            s = 0x40000,
            t = 0x80000,
            u = 0x100000,
            v = 0x200000,
            w = 0x400000,
            x = 0x800000,
            y = 0x1000000,
            z = 0x2000000,
            Start1 = 0x4000000,
            Start2 = 0x8000000,
            Start3 = 0x10000000,
            Start4 = 0x20000000,
            Start5 = 0x40000000,
        }
    }
}
