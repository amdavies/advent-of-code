using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AdventOfCode.Y2019.Intcode;

namespace AdventOfCode.Y2019.Solution
{
    public class Day7 : APuzzle
    {
        public Day7() : base(7, 2019, "Amplification Circuit") { }

        protected override async Task<string> SolvePartOne()
        {
            var options = new List<int> {0, 1, 2, 3, 4};
            var combinations = Permutate(options, options.Count);
            long largestOutput = 0;

            foreach (var phaseOrder in combinations) {
                var ioBus = new IoBus();
                var vm = new Vm(this.Input, new InputQueue(new long[] {phaseOrder[0], 0}), ioBus);
                ioBus.AddInput(phaseOrder[1]);
                vm.Run();

                var ioBus2 = new IoBus();
                var vm2 = new Vm(this.Input, ioBus, ioBus2);
                ioBus2.AddInput(phaseOrder[2]);
                vm2.Run();

                var ioBus3 = new IoBus();
                var vm3 = new Vm(this.Input, ioBus2, ioBus3);
                ioBus3.AddInput(phaseOrder[3]);
                vm3.Run();

                var ioBus4 = new IoBus();
                var vm4 = new Vm(this.Input, ioBus3, ioBus4);
                ioBus4.AddInput(phaseOrder[4]);
                vm4.Run();


                var vm5 = new Vm(this.Input, ioBus4);
                vm5.Run();

                largestOutput = Math.Max(vm5.Output.GetOutput(), largestOutput);
            }

            return largestOutput.ToString();
        }

        protected override async Task<string> SolvePartTwo()
        {
            var options = new List<int> {5, 6, 7, 8, 9};
            var combinations = Permutate(options, options.Count);
            long largestOutput = 0;

            foreach (var phaseOrder in combinations) {
                // Initialise Vms with inputs/outputs linked
                var io1 = new IoBus(new long[] {phaseOrder[0], 0});
                var io2 = new IoBus(new long[] {phaseOrder[1]});
                var io3 = new IoBus(new long[] {phaseOrder[2]});
                var io4 = new IoBus(new long[] {phaseOrder[3]});
                var io5 = new IoBus(new long[] {phaseOrder[4]});

                var vm = new Vm(this.Input, io1, io2);
                var vm2 = new Vm(this.Input, io2, io3);
                var vm3 = new Vm(this.Input, io3, io4);
                var vm4 = new Vm(this.Input, io4, io5);
                var vm5 = new Vm(this.Input, io5, io1);

                // Keep running them until they finish
                while (!vm5.HasHalted) {
                    vm.Run();
                    vm2.Run();
                    vm3.Run();
                    vm4.Run();
                    vm5.Run();
                }

                largestOutput = Math.Max(vm5.Output.GetOutput(), largestOutput);
            }

            return largestOutput.ToString();
        }

        private static void RotateRight(IList<int> sequence, int count)
        {
            var tmp = sequence[count - 1];
            sequence.RemoveAt(count - 1);
            sequence.Insert(0, tmp);
        }

        private static IEnumerable<List<int>> Permutate(List<int> sequence, int count)
        {
            if (count == 1)
                yield return sequence;
            else
                for (var i = 0; i < count; i++) {
                    foreach (var perm in Permutate(sequence, count - 1)) yield return perm;

                    RotateRight(sequence, count);
                }
        }
    }
}
