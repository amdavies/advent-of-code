﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdventOfCode.Y2019.Solution
{
    public class Day14 : APuzzle
    {
        public Day14() : base(14, 2019, "Space Stoichiometry") { }

        protected override async Task<string> SolvePartOne()
        {
            var reactions = this.GetReactions();

            return OreRequired(1, reactions).ToString();
        }

        protected override async Task<string> SolvePartTwo()
        {
            // Parse reactions
            var reactions = this.GetReactions();

            // Binary search for largest value below 1 tril ore
            var storedOre = 1000000000000;
            var minFuel = storedOre / OreRequired(1, reactions);
            var maxFuel = minFuel * 2;

            while (maxFuel > minFuel + 1) {
                var toRequest = (minFuel + maxFuel) / 2;
                if (OreRequired(toRequest, reactions) > storedOre)
                    maxFuel = toRequest;
                else
                    minFuel = toRequest;
            }

            return minFuel.ToString();
        }

        private static long OreRequired(long fuelRequired,
            Dictionary<string, ((string element, int quantity)[], int produced)> reactions)
        {
            var requirements = new Dictionary<string, long> {{"FUEL", fuelRequired}};

            while (true) {
                var toResolve = requirements
                    .Where(r => r.Key != "ORE" && r.Value > 0)
                    .Select(r => (element: r.Key, quantity: r.Value))
                    .ToArray();

                if (toResolve.Length == 0) break;

                foreach (var requirement in toResolve) {
                    var (need, producedCount) = reactions[requirement.element];
                    var reactionCount = (long) Math.Ceiling(requirement.quantity / (double) producedCount);

                    foreach (var input in need)
                        AddRequirement(input.element, input.quantity * reactionCount, requirements);

                    requirements[requirement.element] -= producedCount * reactionCount;
                }
            }

            return requirements["ORE"];
        }

        private static void AddRequirement(string element, long quantity, Dictionary<string, long> requirements)
        {
            requirements[element] = requirements.GetValueOrDefault(element, 0) + quantity;
        }

        private Dictionary<string, ((string element, int quantity)[] need, int produced)> GetReactions()
        {
            var reactions = this.Input
                .Split('\n')
                .Select(l => l.Split(" => "))
                .Select(s =>
                {
                    string output = s[1].Split(' ')[1];

                    var input = s[0]
                        .Split(", ")
                        .Select(e => (element: e.Split(' ')[1], quantity: int.Parse(e.Split(' ')[0])))
                        .ToArray();

                    return (key: output, value: (need: input, produced: int.Parse(s[1].Split(' ')[0])));
                })
                .ToDictionary(d => d.key, d => d.value);
            return reactions;
        }
    }
}