using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdventOfCode.Y2019.Solution
{
    public class Day6 : APuzzle
    {
        public Day6() : base(6, 2019, "Universal Orbit Map") { }

        protected override async Task<string> SolvePartOne()
        {
            var directOrbits = this.Input.Split('\n')
                .Select(delegate(string orbitString)
                    {
                        var orbit = orbitString.Split(')');
                        return (orbiter: orbit[1], orbited: orbit[0]);
                    }
                )
                .ToDictionary(s => s.orbiter);

            return directOrbits
                .Select(s => s.Value)
                .Sum(orbit =>
                {
                    var count = 1;
                    while (true) {
                        if (!directOrbits.TryGetValue(orbit.orbited, out var subOrbit)) return count;

                        count++;
                        orbit = subOrbit;
                    }
                })
                .ToString();
        }

        protected override async Task<string> SolvePartTwo()
        {
            var directOrbits = this.Input.Split('\n')
                .Select(s => s.Split(')'))
                .Select(s => (orbiter: s[1], orbited: s[0]))
                .ToDictionary(s => s.orbiter);

            // I think the following logic can be simplified or minified

            // Find all parent nodes
            var santa = directOrbits["SAN"];
            var santaNodes = new Dictionary<string, int>();
            var count = 0;
            while (true) {
                if (!directOrbits.TryGetValue(santa.orbited, out var subOrbit)) break;

                santaNodes.Add(santa.orbited, count++);
                santa = subOrbit;
            }

            // Search until a common node is found
            var me = directOrbits["YOU"];
            count = 0;
            while (true) {
                if (santaNodes.ContainsKey(me.orbited)) return (santaNodes[me.orbited] + count).ToString();
                if (!directOrbits.TryGetValue(me.orbited, out var subOrbit)) break;

                count++;
                me = subOrbit;
            }

            return "Unknown";
        }
    }
}
