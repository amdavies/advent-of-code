﻿using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Threading.Tasks;

namespace AdventOfCode.Y2019.Solution
{
    public class Day22 : APuzzle
    {
        public Day22() : base(22, 2019, "Slam Shuffle") { }

        protected override async Task<string> SolvePartOne()
        {
            var deckSize = 10007;

            var deck = new List<int>();

            for (var i = 0; i < deckSize; i++) deck.Add(i);

            foreach (var instruction in this.Input.Split('\n')) {
                if (instruction == "deal into new stack") {
                    deck = this.fromStack(deck);
                    continue;
                }

                if (instruction.Contains("cut")) deck = this.cutCards(deck, int.Parse(instruction.Replace("cut ", "")));

                if (instruction.Contains("deal"))
                    deck = this.dealCards(deck, int.Parse(instruction.Replace("deal with increment ", "")));
            }

            return deck.IndexOf(2019).ToString();
        }

        protected override async Task<string> SolvePartTwo()
        {
            BigInteger deckSize = 119315717514047;
            BigInteger repeats = 101741582076661;
            var indexToFind = 2020;

            BigInteger offset = 0;
            BigInteger incrementBase = 1;

            foreach (var instruction in this.Input.Split('\n')) {
                if (instruction == "deal into new stack") {
                    // Order is reversed
                    offset -= incrementBase;
                    incrementBase *= -1;
                    continue;
                }

                if (instruction.Contains("cut")) {
                    // Offsets the list, based on current increment
                    var cutSize = int.Parse(instruction.Replace("cut ", ""));

                    offset += cutSize * incrementBase;
                }

                if (instruction.Contains("deal")) {
                    BigInteger dealSize = int.Parse(instruction.Replace("deal with increment ", ""));

                    incrementBase *= this.ModInv(dealSize, deckSize);
                }
            }

            // Calculate increment + offset after all repeats
            offset *= this.ModInv(1 - incrementBase, deckSize);
            incrementBase = BigInteger.ModPow(incrementBase, repeats, deckSize);

            // All operations just tweak offsets and multipliers, and cards start in order
            return (((indexToFind * incrementBase + (1 - incrementBase) * offset) % deckSize + deckSize) % deckSize)
                .ToString();
        }

        private BigInteger ModInv(BigInteger value, BigInteger cardCount)
        {
            return BigInteger.ModPow(value, cardCount - 2, cardCount);
        }

        private List<int> dealCards(List<int> deck, int steps)
        {
            var table = new int[deck.Count];

            for (var index = 0; index < deck.Count; index++) table[steps * index % deck.Count] = deck[index];

            return table.ToList();
        }

        private List<int> cutCards(List<int> deck, int length)
        {
            if (length < 0) length += deck.Count;


            var cut = deck.Take(length);
            deck = deck.Skip(length).ToList();
            deck.AddRange(cut);

            return deck;
        }

        private List<int> fromStack(List<int> deck)
        {
            deck.Reverse();

            return deck;
        }
    }
}