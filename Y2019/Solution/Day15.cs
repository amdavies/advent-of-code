﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using AdventOfCode.Y2019.Intcode;

namespace AdventOfCode.Y2019.Solution
{
    public class Day15 : APuzzle
    {
        public static bool ShowOutput = false;
        private readonly Lazy<Vm> VmLoader;

        private Dictionary<Point, Result> Maze = new();
        private int TargetDistance;
        private Point TargetPoint;

        public Day15() : base(15, 2019, "Oxygen System")
        {
            this.VmLoader = new Lazy<Vm>(() => new Vm(this.Input));
        }

        private Vm Vm => this.VmLoader.Value;

        protected override async Task<string> SolvePartOne()
        {
            this.Maze = this.BuildMaze();
            DrawPoint(new Point(21, 21), (Result) 99, "0");

            return this.TargetDistance.ToString();
        }

        protected override async Task<string> SolvePartTwo()
        {
            this.Maze = this.BuildMaze();
            // Starting at target, navigate through the maze, storing steps taken
            var maze = this.FillMaze(this.TargetPoint);

            return maze.Max(p => p.Value).ToString();
        }

        private Dictionary<Point, Result> BuildMaze()
        {
            var origin = new Point(21, 21);

            var maze = new Dictionary<Point, Result>();

            this.MoveFrom(maze, origin, 1);

            return maze;
        }

        private Dictionary<Point, int> FillMaze(Point startingPoint)
        {
            var levels = new Dictionary<Point, int>
            {
                {startingPoint, 0},
            };
            var options = new Stack<Point>();
            options.Push(startingPoint);

            while (options.Count > 0) {
                var point = options.Pop();

                var nextPoints = new[]
                {
                    new Point(point.X + 1, point.Y),
                    new Point(point.X - 1, point.Y),
                    new Point(point.X, point.Y + 1),
                    new Point(point.X, point.Y - 1),
                };

                foreach (var nextPoint in nextPoints)
                    if (this.Maze[nextPoint] != Result.Wall)
                        if (!levels.ContainsKey(nextPoint)) {
                            levels[nextPoint] = levels[point] + 1;
                            options.Push(nextPoint);
                        }
            }

            return levels;
        }

        private void MoveFrom(Dictionary<Point, Result> maze, Point point, int step)
        {
            var newPoint = new Point(point.X, point.Y - 1);
            if (!maze.ContainsKey(newPoint)) {
                var result = this.MoveInDirection(Direction.North);
                maze.Add(newPoint, result);

                DrawPoint(newPoint, result, step.ToString().Last().ToString());

                if (result != Result.Wall) {
                    this.MoveFrom(maze, newPoint, step + 1);
                    // Move back
                    this.MoveInDirection(Direction.South);

                    if (result == Result.OxygenSystem) {
                        this.TargetPoint = newPoint;
                        this.TargetDistance = step;
                    }
                }
            }

            newPoint = new Point(point.X, point.Y + 1);
            if (!maze.ContainsKey(newPoint)) {
                var result = this.MoveInDirection(Direction.South);
                maze.Add(newPoint, result);

                DrawPoint(newPoint, result, step.ToString().Last().ToString());

                if (result != Result.Wall) {
                    this.MoveFrom(maze, newPoint, step + 1);
                    // Move back
                    this.MoveInDirection(Direction.North);

                    if (result == Result.OxygenSystem) {
                        this.TargetPoint = newPoint;
                        this.TargetDistance = step;
                    }
                }
            }

            newPoint = new Point(point.X + 1, point.Y);
            if (!maze.ContainsKey(newPoint)) {
                var result = this.MoveInDirection(Direction.East);
                maze.Add(newPoint, result);

                DrawPoint(newPoint, result, step.ToString().Last().ToString());

                if (result != Result.Wall) {
                    this.MoveFrom(maze, newPoint, step + 1);
                    // Move back
                    this.MoveInDirection(Direction.West);

                    if (result == Result.OxygenSystem) {
                        this.TargetPoint = newPoint;
                        this.TargetDistance = step;
                    }
                }
            }

            newPoint = new Point(point.X - 1, point.Y);
            if (!maze.ContainsKey(newPoint)) {
                var result = this.MoveInDirection(Direction.West);
                maze.Add(newPoint, result);

                DrawPoint(newPoint, result, step.ToString().Last().ToString());

                if (result != Result.Wall) {
                    this.MoveFrom(maze, newPoint, step + 1);
                    // Move back
                    this.MoveInDirection(Direction.East);

                    if (result == Result.OxygenSystem) {
                        this.TargetPoint = newPoint;
                        this.TargetDistance = step;
                    }
                }
            }
        }

        private Result MoveInDirection(Direction direction)
        {
            this.Vm.Input.AddInput((long) direction);
            this.Vm.Run();
            return (Result) this.Vm.Output.GetOutput();
        }

        private static void DrawPoint(Point point, Result result, string msg = " ")
        {
            if (!ShowOutput) return;

            Console.SetCursorPosition(point.X, point.Y);
            switch (result) {
                case Result.Wall:
                    Console.BackgroundColor = ConsoleColor.Gray;
                    break;
                case Result.Path:
                    Console.BackgroundColor = ConsoleColor.Black;
                    break;
                case Result.OxygenSystem:
                    Console.BackgroundColor = ConsoleColor.Blue;
                    break;
                default:
                    Console.BackgroundColor = ConsoleColor.Red;
                    break;
            }

            Console.Write(' ');
            Console.ResetColor();
        }

        private enum Direction
        {
            North = 1,
            South = 2,
            West = 3,
            East = 4,
        }

        private enum Result
        {
            Wall = 0,
            Path = 1,
            OxygenSystem = 2,
        }
    }
}