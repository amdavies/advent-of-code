using System.Linq;
using System.Threading.Tasks;

namespace AdventOfCode.Y2019.Solution
{
    public class Day4 : APuzzle
    {
        private int Max;
        private int Min;

        public Day4() : base(4, 2019, "Secure Container") { }

        protected override async Task<string> SolvePartOne()
        {
            this.Min = int.Parse(this.Input.Split('-')[0]);
            this.Max = int.Parse(this.Input.Split('-')[1]);
            var validCount = 0;

            for (var password = this.Min; password <= this.Max; password++) {
                (int[] charCount, var invalid) = CountDigits(password.ToString());

                if (charCount.Max() >= 2 && !invalid) validCount++;
            }

            return validCount.ToString();
        }

        protected override async Task<string> SolvePartTwo()
        {
            this.Min = int.Parse(this.Input.Split('-')[0]);
            this.Max = int.Parse(this.Input.Split('-')[1]);
            var validCount = 0;

            for (var password = this.Min; password <= this.Max; password++) {
                (int[] charCount, var invalid) = CountDigits(password.ToString());

                if (charCount.Contains(2) && !invalid) validCount++;
            }

            return validCount.ToString();
        }

        private static (int[], bool) CountDigits(string password)
        {
            var invalid = false;

            var prev = 0;
            int[] charCount = new int[10];
            foreach (var digit in password) {
                var num = int.Parse(digit.ToString());

                charCount[num]++;

                if (num < prev) {
                    invalid = true;
                    break;
                }

                prev = num;
            }

            return (charCount, invalid);
        }
    }
}