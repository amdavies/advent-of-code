﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;
using AdventOfCode.Y2019.Intcode;
using Pastel;

namespace AdventOfCode.Y2019.Solution
{
    public enum PanelColor
    {
        Black = 0,
        White = 1,
    }

    public enum TurnDirection
    {
        Left = 0,
        Right = 1,
    }

    public enum Direction
    {
        Up = 0,
        Right = 1,
        Down = 2,
        Left = 3,
    }

    public class Day11 : APuzzle
    {
        public Day11() : base(11, 2019, "Space Police") { }

        protected override async Task<string> SolvePartOne()
        {
            Dictionary<(int x, int y), PanelColor> painted = new();
            var pos = (x: 0, y: 0);
            var facing = Direction.Up;

            var robot = new Vm(this.Input);

            while (!robot.HasHalted) {
                var panelColor = painted.ContainsKey(pos) ? painted[pos] : PanelColor.Black;

                robot.Input.AddInput((long) panelColor);
                robot.Run();

                painted[pos] = (PanelColor) robot.Output.GetOutput();
                var direction = (TurnDirection) robot.Output.GetOutput();

                (pos, facing) = this.TurnMove(direction, pos, facing);
            }

            return painted.Keys.Count.ToString();
        }

        protected override async Task<string> SolvePartTwo()
        {
            Dictionary<(int x, int y), PanelColor> painted = new()
            {
                {(0, 0), PanelColor.White},
            };
            var pos = (x: 0, y: 0);
            var facing = Direction.Up;

            var robot = new Vm(this.Input);

            while (!robot.HasHalted) {
                var panelColor = painted.ContainsKey(pos) ? painted[pos] : PanelColor.Black;

                robot.Input.AddInput((long) panelColor);
                robot.Run();

                painted[pos] = (PanelColor) robot.Output.GetOutput();
                var direction = (TurnDirection) robot.Output.GetOutput();

                (pos, facing) = this.TurnMove(direction, pos, facing);
            }

            var mostLeft = painted.Min(s => s.Key.x);
            var mostRight = painted.Max(s => s.Key.x);
            var mostUp = painted.Min(s => s.Key.y);
            var mostDown = painted.Max(s => s.Key.y);

            string output = "\n";
            for (var y = mostUp; y <= mostDown; y++) {
                for (var x = mostLeft; x <= mostRight; x++)
                    if (painted.ContainsKey((x, y)))
                        output += " ".PastelBg(painted[(x, y)] == PanelColor.White ? Color.White : Color.Black);
                    else
                        output += " ".PastelBg(Color.Black);

                output += "\n";
            }

            return output;
        }

        private ((int x, int y), Direction) TurnMove(TurnDirection direction, (int x, int y) pos, Direction facing)
        {
            var turned = (int) facing + (direction == TurnDirection.Left ? -1 : 1);
            turned = turned < 0 ? 4 + turned : turned % 4;
            var newFacing = (Direction) turned;

            switch (newFacing) {
                case Direction.Up:
                    return ((pos.x, pos.y - 1), newFacing);
                case Direction.Down:
                    return ((pos.x, pos.y + 1), newFacing);
                case Direction.Right:
                    return ((pos.x + 1, pos.y), newFacing);
                case Direction.Left:
                    return ((pos.x - 1, pos.y), newFacing);
                default:
                    throw new Exception($"Unknown facing: {newFacing}");
            }
        }
    }
}
