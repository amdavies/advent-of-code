﻿using System.Linq;
using System.Threading.Tasks;
using AdventOfCode.Y2019.Intcode;

namespace AdventOfCode.Y2019.Solution
{
    public class Day21 : APuzzle
    {
        public Day21() : base(21, 2019, "Springdroid Adventure") { }

        protected override async Task<string> SolvePartOne()
        {
            var script = @"NOT C J
NOT A T
OR T J
AND D J
WALK
";
            var vm = new Vm(this.Input, this.fromSpringScript(script));
            vm.Run();

            var finalOutput = vm.Output.Last();
            if (finalOutput > 128) return vm.Output.Last().ToString();

            return string.Join("", vm.Output.Select(l => (char) l));
        }

        protected override async Task<string> SolvePartTwo()
        {
            string sprintScript = @"NOT H T
OR  C T
AND B T
NOT T J
NOT A T
OR T J
AND D J
RUN
";
            var vm = new Vm(this.Input, this.fromSpringScript(sprintScript));
            vm.Run();

            var finalOutput = vm.Output.Last();
            if (finalOutput > 128) return vm.Output.Last().ToString();

            return string.Join("", vm.Output.Select(l => (char) l));
        }

        /// <summary>
        ///     Converts a human readable springscript to an ascii intcode interpreter input queue
        /// </summary>
        /// <param name="script"></param>
        /// <returns></returns>
        private IInput fromSpringScript(string script)
        {
            return new InputQueue(script.ToCharArray().Select(c => (long) c));
        }
    }
}