﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AdventOfCode.Y2019.Intcode;

namespace AdventOfCode.Y2019.Solution
{
    public class Day25 : APuzzle
    {
        public Day25() : base(25, 2019, "Cryostasis") { }

        protected override async Task<string> SolvePartOne()
        {
            const string fetchAllInstructions = @"west
north
north
take prime number
south
east
take space law space brochure
west
south
take hologram
east
north
north
take astrolabe
south
take polygon
south
south
east
take weather machine
west
south
take manifold
west
take mouse
north
north
drop mouse
drop manifold
drop weather machine
drop polygon
drop astrolabe
drop hologram
drop space law space brochure
drop prime number
";
            var input = new StdIn();
            var output = new StdOut();
            input.BufferMultiple(new Queue<long>(fetchAllInstructions.ToCharArray().Select(c => (long) c)));

            var vm = new Vm(this.Input, input, output);

            vm.Run();

            return output.line
                .Replace("\"Oh, hello! You should be able to get in by typing ", "")
                .Replace(" on the keypad at the main airlock.\"\n", "");
        }

        protected override async Task<string> SolvePartTwo() => "We made it! 50 Stars!";

        private class StdIn : IInput
        {
            private Queue<long> buffer = new();

            private readonly string[] items =
            {
                "manifold",
                "weather machine",
                "prime number",
                "polygon",
                "astrolabe",
                "mouse",
                "hologram",
                "space law space brochure",
            };

            public int Count => this.GetCount();

            public void ClearInputs()
            {
                throw new NotImplementedException();
            }

            public void AddInput(long input)
            {
                throw new NotImplementedException();
            }

            public long? GetNextInput()
            {
                if (this.buffer.Count != 0) return this.buffer.Dequeue();

                var dropAll = @"drop mouse
drop manifold
drop weather machine
drop polygon
drop astrolabe
drop hologram
drop space law space brochure
drop prime number
";
                var input = "";
                for (var i = 1; i <= 256; i++) {
                    if ((i & 1) > 0) input += $"take {this.items[0]}\n";

                    if ((i & 2) > 0) input += $"take {this.items[1]}\n";

                    if ((i & 4) > 0) input += $"take {this.items[2]}\n";

                    if ((i & 8) > 0) input += $"take {this.items[3]}\n";

                    if ((i & 16) > 0) input += $"take {this.items[4]}\n";

                    if ((i & 32) > 0) input += $"take {this.items[5]}\n";

                    if ((i & 64) > 0) input += $"take {this.items[6]}\n";

                    if ((i & 128) > 0) input += $"take {this.items[7]}\n";

                    input += "inv\n";
                    input += "east\n";

                    input += dropAll;
                }

                this.buffer = new Queue<long>(input.ToCharArray().Select(c => (long) c).ToArray());

                return this.buffer.Dequeue();
            }

            public int GetCount()
            {
                if (this.buffer.Count == 0) {
                    var input = Console.ReadLine() + "\n";
                    this.buffer = new Queue<long>(input.ToCharArray().Select(c => (long) c).ToArray());
                }

                return this.buffer.Count;
            }

            public void BufferMultiple(Queue<long> queue)
            {
                this.buffer = queue;
            }
        }

        private class StdOut : IOutput
        {
            public string line = "";
            private bool lineEnded;

            public int Count { get; }

            public IEnumerator<long> GetEnumerator()
            {
                throw new NotImplementedException();
            }

            IEnumerator IEnumerable.GetEnumerator()
            {
                return this.GetEnumerator();
            }

            public void WriteOutput(long output)
            {
                if (this.lineEnded) {
                    this.line = "";
                    this.lineEnded = false;
                }

                this.line += (char) output;

                if ((char) output == '\n') this.lineEnded = true;
            }

            public long GetOutput()
            {
                throw new NotImplementedException();
            }
        }
    }
}
