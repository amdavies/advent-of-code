﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdventOfCode.Y2019.Solution
{
    public class Day12 : APuzzle
    {
        public Day12() : base(12, 2019, "The N-Body Problem") { }

        protected override async Task<string> SolvePartOne()
        {
            long stepCount = 1000;
            var moons = this.Input
                .Split('\n')
                .Select(delegate(string s)
                {
                    var pos = (x: 0, y: 0, z: 0);

                    var points = s.Replace("<x=", "")
                        .Replace(" y=", "")
                        .Replace(" z=", "")
                        .Replace(">", "")
                        .Replace("x=", "")
                        .Split(',');

                    pos.x = int.Parse(points[0]);
                    pos.y = int.Parse(points[1]);
                    pos.z = int.Parse(points[2]);

                    return (pos, vel: (x: 0, y: 0, z: 0));
                })
                .ToArray();

            for (long step = 0; step < stepCount; step++) moons = SimulateMoons(moons);

            var output = moons.Sum(m =>
                (Math.Abs(m.pos.x) + Math.Abs(m.pos.y) + Math.Abs(m.pos.z)) *
                (Math.Abs(m.vel.x) + Math.Abs(m.vel.y) + Math.Abs(m.vel.z)));

            return output.ToString();
        }

        private static ((int x, int y, int z) pos, (int x, int y, int z) vel)[] SimulateMoons(
            ((int x, int y, int z) pos, (int x, int y, int z) vel)[] moons)
        {
            // Calculate velocity. This is 50% efficient, each interaction is calculated twice
            for (var index = 0; index < moons.Length; index++) {
                var moon = moons[index];
                foreach (var pairMoon in moons.Where(s => s != moon)) {
                    moon.vel.x += moon.pos.x < pairMoon.pos.x ? 1 : moon.pos.x > pairMoon.pos.x ? -1 : 0;
                    moon.vel.y += moon.pos.y < pairMoon.pos.y ? 1 : moon.pos.y > pairMoon.pos.y ? -1 : 0;
                    moon.vel.z += moon.pos.z < pairMoon.pos.z ? 1 : moon.pos.z > pairMoon.pos.z ? -1 : 0;
                }

                moons[index] = moon;
            }

            // Apply velocity
            for (var index = 0; index < moons.Length; index++) {
                var moon = moons[index];

                moon.pos.x += moon.vel.x;
                moon.pos.y += moon.vel.y;
                moon.pos.z += moon.vel.z;

                moons[index] = moon;
            }

            return moons;
        }

        protected override async Task<string> SolvePartTwo()
        {
            var moons = this.Input
                .Split('\n')
                .Select(delegate(string s)
                {
                    var pos = (x: 0, y: 0, z: 0);

                    var points = s.Replace("<x=", "")
                        .Replace(" y=", "")
                        .Replace(" z=", "")
                        .Replace(">", "")
                        .Replace("x=", "")
                        .Split(',');

                    pos.x = int.Parse(points[0]);
                    pos.y = int.Parse(points[1]);
                    pos.z = int.Parse(points[2]);

                    return (pos, vel: (x: 0, y: 0, z: 0));
                })
                .ToArray();

            var xStates = new HashSet<(int, int, int, int, int, int, int, int)>();
            long xCount = 0;
            while (true) {
                for (var index = 0; index < moons.Length; index++) {
                    var moon = moons[index];
                    foreach (var pairMoon in moons.Where(s => s != moon))
                        moon.vel.x += moon.pos.x < pairMoon.pos.x ? 1 : moon.pos.x > pairMoon.pos.x ? -1 : 0;

                    moons[index] = moon;
                }

                // Apply velocity
                for (var index = 0; index < moons.Length; index++) moons[index].pos.x += moons[index].vel.x;

                var state = (moons[0].pos.x, moons[1].pos.x, moons[2].pos.x, moons[3].pos.x, moons[0].vel.x,
                    moons[1].vel.x, moons[2].vel.x, moons[3].vel.x);
                if (xStates.Contains(state)) break;

                xStates.Add(state);
                xCount++;
            }

            var yStates = new HashSet<(int, int, int, int, int, int, int, int)>();
            long yCount = 0;
            while (true) {
                for (var index = 0; index < moons.Length; index++) {
                    var moon = moons[index];
                    foreach (var pairMoon in moons.Where(s => s != moon))
                        moon.vel.y += moon.pos.y < pairMoon.pos.y ? 1 : moon.pos.y > pairMoon.pos.y ? -1 : 0;

                    moons[index] = moon;
                }

                // Apply velocity
                for (var index = 0; index < moons.Length; index++) moons[index].pos.y += moons[index].vel.y;

                var state = (moons[0].pos.y, moons[1].pos.y, moons[2].pos.y, moons[3].pos.y, moons[0].vel.y,
                    moons[1].vel.y, moons[2].vel.y, moons[3].vel.y);
                if (yStates.Contains(state)) break;

                yStates.Add(state);
                yCount++;
            }

            var zStates = new HashSet<(int, int, int, int, int, int, int, int)>();
            long zCount = 0;
            while (true) {
                for (var index = 0; index < moons.Length; index++) {
                    var moon = moons[index];
                    foreach (var pairMoon in moons.Where(s => s != moon))
                        moon.vel.z += moon.pos.z < pairMoon.pos.z ? 1 : moon.pos.z > pairMoon.pos.z ? -1 : 0;

                    moons[index] = moon;
                }

                // Apply velocity
                for (var index = 0; index < moons.Length; index++) moons[index].pos.z += moons[index].vel.z;

                var state = (moons[0].pos.z, moons[1].pos.z, moons[2].pos.z, moons[3].pos.z, moons[0].vel.z,
                    moons[1].vel.z, moons[2].vel.z, moons[3].vel.z);
                if (zStates.Contains(state)) break;

                zStates.Add(state);
                zCount++;
            }

            return lcm(xCount, lcm(yCount, zCount)).ToString();
        }

        private static long lcm(long a, long b)
        {
            return a * b / gcd(a, b);
        }

        private static long gcd(long a, long b)
        {
            while (b != 0) {
                var tmp = b;
                b = a % b;
                a = tmp;
            }

            return a;
        }
    }
}