﻿using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Numerics;
using System.Threading.Tasks;

namespace AdventOfCode.Y2019.Solution
{
    /// <summary>
    ///     Requires manually updating input to make sure there are no repeated portal combinations, such as NO and ON
    /// </summary>
    public class Day20 : APuzzle
    {
        private Vector3 End;
        private Dictionary<Vector2, char> Map = new();

        private Dictionary<Vector2, (bool isInner, Vector2 destination)> PortalLinks =
            new();

        private Dictionary<Vector2, List<(int steps, Vector2 destination)>> PortalSteps =
            new();

        private Vector3 Start;

        public Day20() : base(20, 2019, "Donut Maze") { }

        protected override async Task<string> SolvePartOne()
        {
            if (this.Map.Count == 0) this.BuildMap();

            return this.FindSteps().ToString();
        }

        protected override async Task<string> SolvePartTwo()
        {
            if (this.Map.Count == 0) this.BuildMap();

            return this.FindSteps(true).ToString();
        }

        private int FindSteps(bool verifyDepth = false)
        {
            // Start is at AA
            var seen = new HashSet<Vector3>
            {
                this.Start,
            };
            var options = new Heap<(Vector3 position, int steps)>((point, point1) => point.steps - point1.steps);
            options.Insert((this.Start, 0));

            var down = new Vector3(0, 0, 1);
            var up = new Vector3(0, 0, -1);

            while (options.Count > 0) {
                var option = options.Pop();
                var point = option.position;
                var mapPoint = new Vector2(point.X, point.Y);

                // Check for win condition after pop, this will be the lowest step count solution
                if (point == this.End) return option.steps;

                // Get options for following portals
                foreach (var nextStep in this.PortalSteps[mapPoint]) {
                    var destination = new Vector3(nextStep.destination, point.Z);

                    if (!seen.Contains(destination)) {
                        options.Insert((destination, option.steps + nextStep.steps));
                        seen.Add(destination);
                    }
                }

                // Get option for going through the current portal
                if (this.PortalLinks.ContainsKey(mapPoint)) {
                    var portal = this.PortalLinks[mapPoint];

                    var destination = new Vector3(portal.destination, point.Z);
                    if (verifyDepth) {
                        destination += portal.isInner ? down : up;
                        if (destination.Z >= 0 && !seen.Contains(destination)) {
                            options.Insert((destination, option.steps + 1));
                            seen.Add(destination);
                        }
                    } else {
                        if (!seen.Contains(destination)) {
                            options.Insert((destination, option.steps));
                            seen.Add(destination);
                        }
                    }
                }
            }

            return 0;
        }

        private void BuildMap()
        {
            // Initialise data structures
            this.Map = new Dictionary<Vector2, char>();
            var portalParts = new Dictionary<Vector2, char>();

            var y = 0;
            foreach (var line in this.Input.Split('\n')) {
                var x = 0;

                foreach (var tile in line.ToCharArray()) {
                    var location = new Vector2(x, y);
                    x++;

                    if (tile == '#' || tile == ' ') continue;

                    if (tile >= 'A' && tile <= 'Z') portalParts.Add(location, tile);

                    this.Map.Add(location, tile);
                }

                y++;
            }

            var portals = this.findPortals(portalParts);
            this.PortalLinks = this.linkPortals(portals);
            this.findPortalSteps();
        }

        private List<(string, Vector2, Vector2)> findPortals(Dictionary<Vector2, char> portalParts)
        {
            var portals = new List<(string, Vector2, Vector2)>();
            // Build each portal combination
            foreach (var (position, letter) in portalParts) {
                // Other letter in portal won't match, but will be on an adjacent tile

                var checkPoints = new[]
                {
                    new Vector2(position.X + 1, position.Y),
                    new Vector2(position.X - 1, position.Y),
                    new Vector2(position.X, position.Y + 1),
                    new Vector2(position.X, position.Y - 1),
                };

                foreach (var checkPoint in checkPoints)
                    if (this.Map.ContainsKey(checkPoint) && this.Map[checkPoint] != '.') {
                        // Found a portal, need to work out:
                        // The portal string (or something to identify it uniquely)
                        // The adjacent walkable tile (there should only be one)
                        // For now, store both the points and the combined letters sorted alphabetically
                        var portalName = string.Concat(new[] {this.Map[checkPoint], letter}.OrderBy(c => c));
                        var positions = new[] {position, checkPoint}.OrderBy(p => p.Y).ThenBy(p => p.X).ToArray();
                        var definition = (portalName, positions[0], positions[1]);

                        if (!portals.Contains(definition)) portals.Add((portalName, positions[0], positions[1]));

                        break;
                    }
            }

            return portals;
        }

        private Dictionary<Vector2, (bool, Vector2)> linkPortals(
            List<(string name, Vector2 first, Vector2 second)> portals)
        {
            // Calculate maze borders
            var minX = (int) this.Map.Where(s => s.Value == '.').Min(s => s.Key.X);
            var maxX = (int) this.Map.Where(s => s.Value == '.').Max(s => s.Key.X);
            var minY = (int) this.Map.Where(s => s.Value == '.').Min(s => s.Key.Y);
            var maxY = (int) this.Map.Where(s => s.Value == '.').Max(s => s.Key.Y);

            var mazeBorders = new Rectangle(new Point(minX + 1, minY + 1), new Size(maxX - minX - 1, maxY - minY - 1));

            // Generate links
            var links = new Dictionary<Vector2, (bool, Vector2)>();
            var portalsCalculated = new HashSet<string>();
            portals = portals.OrderBy(p => p.name).ToList();
            foreach (var portal in portals) {
                if (portalsCalculated.Contains(portal.name)) continue;

                // Find walkable tiles
                var firstWalkable = new Vector2(-1, -1);
                foreach (var position in new[] {portal.first, portal.second}) {
                    var checkPoints = new[]
                    {
                        new Vector2(position.X + 1, position.Y),
                        new Vector2(position.X - 1, position.Y),
                        new Vector2(position.X, position.Y + 1),
                        new Vector2(position.X, position.Y - 1),
                    };

                    foreach (var checkPoint in checkPoints)
                        if (this.Map.ContainsKey(checkPoint) && this.Map[checkPoint] == '.') {
                            firstWalkable = checkPoint;
                            break;
                        }

                    if (firstWalkable.X > 0) break;
                }

                if (portal.name == "AA") {
                    this.Start = new Vector3(firstWalkable, 0);
                    continue;
                }

                if (portal.name == "ZZ") {
                    this.End = new Vector3(firstWalkable, 0);
                    continue;
                }

                // Find counterpart with walkable tiles
                var counterpart = portals.First(p => p.name == portal.name && p.first != portal.first);

                var secondWalkable = new Vector2(-1, -1);
                foreach (var position in new[] {counterpart.first, counterpart.second}) {
                    var checkPoints = new[]
                    {
                        new Vector2(position.X + 1, position.Y),
                        new Vector2(position.X - 1, position.Y),
                        new Vector2(position.X, position.Y + 1),
                        new Vector2(position.X, position.Y - 1),
                    };

                    foreach (var checkPoint in checkPoints)
                        if (this.Map.ContainsKey(checkPoint) && this.Map[checkPoint] == '.') {
                            secondWalkable = checkPoint;
                            break;
                        }

                    if (secondWalkable.X > 0) break;
                }

                // Check if the starting portal is outside the maze
                var isInner = mazeBorders.Contains((int) portal.first.X, (int) portal.first.Y);

                // Add links for each direction
                links.Add(firstWalkable, (isInner, secondWalkable));
                links.Add(secondWalkable, (!isInner, firstWalkable));

                // Add to seen list to prevent finding twice
                portalsCalculated.Add(portal.name);
            }

            return links;
        }

        /// <summary>
        ///     Find all possible portals from a given location
        /// </summary>
        private void findPortalSteps()
        {
            var directions = new[]
            {
                new Vector2(1, 0),
                new Vector2(-1, 0),
                new Vector2(0, 1),
                new Vector2(0, -1),
            };

            var startOptions = this.PortalLinks
                .Select(p => p.Key)
                .Append(new Vector2(this.Start.X, this.Start.Y))
                .Append(new Vector2(this.End.X, this.End.Y))
                .ToArray();

            this.PortalSteps = new Dictionary<Vector2, List<(int steps, Vector2 destination)>>();

            foreach (var portalLocation in startOptions) {
                this.PortalSteps[portalLocation] = new List<(int steps, Vector2 destination)>();

                var seen = new List<Vector2>
                {
                    portalLocation,
                };
                var options = new Heap<(Vector2 position, int steps)>((point, point1) => point.steps - point1.steps);
                options.Insert((portalLocation, 0));

                while (options.Count > 0) {
                    var option = options.Pop();
                    var point = option.position;
                    var mapPoint = new Vector2(point.X, point.Y);

                    foreach (var direction in directions) {
                        var nextPoint = point + direction;
                        if (seen.Contains(nextPoint)) continue;

                        if (this.Map.ContainsKey(nextPoint) && this.Map[nextPoint] == '.') {
                            seen.Add(nextPoint);
                            options.Insert((nextPoint, option.steps + 1));

                            if (this.PortalLinks.ContainsKey(nextPoint) || new Vector3(nextPoint, 0) == this.End)
                                this.PortalSteps[portalLocation].Add((option.steps + 1, nextPoint));
                        }
                    }
                }
            }
        }
    }
}
