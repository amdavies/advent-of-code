using System.Threading.Tasks;
using AdventOfCode.Y2019.Intcode;

namespace AdventOfCode.Y2019.Solution
{
    public class Day2 : APuzzle
    {
        public Day2() : base(2, 2019, "1202 Program Alarm") { }

        protected override async Task<string> SolvePartOne()
        {
            var vm = new Vm(this.Input);
            vm.Memory[1] = 12;
            vm.Memory[2] = 2;
            vm.Run();

            return vm.Memory[0].ToString();
        }

        protected override async Task<string> SolvePartTwo()
        {
            var vm = new Vm(this.Input);
            for (var noun = 0; noun < 100; noun++)
            for (var verb = 0; verb < 100; verb++) {
                vm.ResetState();
                vm.Memory[1] = noun;
                vm.Memory[2] = verb;
                vm.Run();

                if (vm.Memory[0] == 19690720) return (100 * noun + verb).ToString();
            }

            return "Answer not found";
        }
    }
}
