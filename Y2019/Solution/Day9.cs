﻿using System.Threading.Tasks;
using AdventOfCode.Y2019.Intcode;

namespace AdventOfCode.Y2019.Solution
{
    public class Day9 : APuzzle
    {
        public Day9() : base(9, 2019, "Sensor Boost") { }

        protected override async Task<string> SolvePartOne()
        {
            var vm = new Vm(this.Input, new InputNumber(1));
            vm.Run();

            return vm.Output.GetOutput().ToString();
        }

        protected override async Task<string> SolvePartTwo()
        {
            var vm = new Vm(this.Input, new InputNumber(2));
            vm.Run();

            return vm.Output.GetOutput().ToString();
        }
    }
}