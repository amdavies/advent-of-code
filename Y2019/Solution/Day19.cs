﻿using System.Threading.Tasks;
using AdventOfCode.Y2019.Intcode;

namespace AdventOfCode.Y2019.Solution
{
    public class Day19 : APuzzle
    {
        public Day19() : base(19, 2019, "Tractor Beam") { }

        protected override async Task<string> SolvePartOne()
        {
            var totalPoints = 0;
            var x = 0;
            for (var y = 0; y < 50; y++) {
                while (!this.CheckCoordinate(x, y) && x < 50) x++;

                if (x == 50) {
                    x = 0;
                    continue;
                }

                var x2 = x;

                while (x2 < 50 && this.CheckCoordinate(x2++, y)) totalPoints++;
            }


            return totalPoints.ToString();
        }

        protected override async Task<string> SolvePartTwo()
        {
            var x = 0;
            var y = 150;
            while (true) {
                while (!this.CheckCoordinate(x, y)) x++;

                // Test corners
                if (this.CheckCoordinate(x + 99, y - 99)) return (x * 10000 + (y - 99)).ToString();

                y++;
            }
        }

        private bool CheckCoordinate(int x, int y)
        {
            if (x < 0 || y < 0) return false;

            var vm = new Vm(this.Input, new InputQueue(new long[] {x, y}));
            vm.Run();

            return vm.Output.GetOutput() == 1;
        }
    }
}