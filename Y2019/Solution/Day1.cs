using System;
using System.Linq;
using System.Threading.Tasks;

namespace AdventOfCode.Y2019.Solution
{
    public class Day1 : APuzzle
    {
        public Day1() : base(1, 2019, "The Tyranny of the Rocket Equation") { }

        protected override async Task<string> SolvePartOne()
        {
            return this.Input
                .Split('\n')
                .Sum(s => Math.Floor(double.Parse(s) / 3) - 2)
                .ToString();
        }

        protected override async Task<string> SolvePartTwo()
        {
            string[] values = this.Input.Split('\n');

            double output = 0;

            foreach (string valueString in values) {
                var fuelCost = Math.Floor(double.Parse(valueString) / 3) - 2;
                while (fuelCost > 0) {
                    output += fuelCost;

                    fuelCost = Math.Floor(fuelCost / 3) - 2;
                }
            }

            return output.ToString();
        }
    }
}
