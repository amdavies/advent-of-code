﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdventOfCode.Y2019.Solution
{
    public class Day10 : APuzzle
    {
        public Day10() : base(10, 2019, "Monitoring Station") { }

        protected override async Task<string> SolvePartOne()
        {
            var lines = this.Input.Split('\n');
            var asteroids = new List<(int x, int y)>();

            for (var y = 0; y < lines.Length; y++) {
                var line = lines[y];
                for (var x = 0; x < line.Length; x++)
                    if (line[x] == '#')
                        asteroids.Add((x, y));
            }

            var bestVisible = 0;

            foreach (var asteroid in asteroids) {
                var visible = asteroids
                    .Except(new[] {asteroid})
                    .Select(a => (x: a.x - asteroid.x, y: a.y - asteroid.y))
                    .GroupBy(a => Math.Atan2(-a.y, a.x))
                    .ToArray();

                if (visible.Length > bestVisible) bestVisible = visible.Length;
            }

            return bestVisible.ToString();
        }

        protected override async Task<string> SolvePartTwo()
        {
            var lines = this.Input.Split('\n');
            var asteroids = new List<(int x, int y)>();

            for (var y = 0; y < lines.Length; y++) {
                var line = lines[y];
                for (var x = 0; x < line.Length; x++)
                    if (line[x] == '#')
                        asteroids.Add((x, y));
            }

            var bestVisible = 0;
            var bestAsteroid = (x: -1, y: -1);
            IGrouping<double, (int x, int y)>[]? bestGroup = null;

            foreach (var asteroid in asteroids) {
                var visible = asteroids
                    .Except(new[] {asteroid})
                    .Select(a => (x: a.x - asteroid.x, y: a.y - asteroid.y))
                    .GroupBy(a => Math.Atan2(-a.y, a.x))
                    .ToArray();

                if (visible.Length > bestVisible) {
                    bestVisible = visible.Length;
                    bestAsteroid = asteroid;
                    bestGroup = visible;
                }
            }

            if (bestGroup == null) return "Failure: No asteroid group found";

            var vaporizationList = bestGroup
                .Select(g => new
                {
                    Angle = g.Key,
                    Targets = new Queue<(int x, int y)>(g.OrderBy(a => Math.Sqrt(a.x * a.x + a.y * a.y))),
                })
                .OrderBy(g => g.Angle > Math.PI / 2)
                .ThenByDescending(g => g.Angle)
                .ToArray();

            var numberVaporized = 0;
            while (numberVaporized < 200)
                foreach (var group in vaporizationList) {
                    var targets = @group.Targets;
                    if (targets.Count > 0) {
                        var toVaporize = targets.Dequeue();
                        numberVaporized++;
                        if (numberVaporized == 200)
                            return ((toVaporize.x + bestAsteroid.x) * 100 + toVaporize.y + bestAsteroid.y).ToString();
                    }
                }

            return "Failure: Unable to vaporize 200 asteroids";
        }
    }
}
