﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AdventOfCode.Y2019.Intcode;

namespace AdventOfCode.Y2019.Solution
{
    public class Day13 : APuzzle
    {
        public Day13() : base(13, 2019, "Care Package") { }

        protected override async Task<string> SolvePartOne()
        {
            var screen = new Dictionary<(long x, long y), Tile>();
            var vm = new Vm(this.Input);

            vm.Run();

            while (vm.Output.Count > 0) {
                var x = vm.Output.GetOutput();
                var y = vm.Output.GetOutput();
                var tile = (Tile) vm.Output.GetOutput();

                screen.Add((x, y), tile);
            }

            return screen.Count(s => s.Value == Tile.Block).ToString();
        }

        protected override async Task<string> SolvePartTwo()
        {
            var screen = new Dictionary<(long x, long y), Tile>();
            long ball = 0;
            long paddle = 0;
            long score = 0;

            var vm = new Vm(this.Input);
            vm.Memory[0] = 2;

            while (!vm.HasHalted) {
                vm.Run();

                while (vm.Output.Count > 0) {
                    var x = vm.Output.GetOutput();
                    var y = vm.Output.GetOutput();
                    var value = vm.Output.GetOutput();

                    if (x == -1 && y == 0) {
                        score = value;
                    } else {
                        screen[(x, y)] = (Tile) value;

                        if ((Tile) value == Tile.Ball) ball = x;

                        if ((Tile) value == Tile.Paddle) paddle = x;
                    }
                }

                // Program is either waiting for input or halted, assume for former. Potential waste of "processing"
                long joystick = 0;
                if (ball < paddle)
                    joystick = -1;
                else if (ball > paddle) joystick = 1;
                vm.Input.AddInput(joystick);
            }

            return score.ToString();
        }

        private static void WriteTileChar(Tile output)
        {
            switch (output) {
                case Tile.Ball:
                    Console.BackgroundColor = ConsoleColor.Red;
                    break;
                case Tile.Wall:
                    Console.BackgroundColor = ConsoleColor.White;
                    break;
                case Tile.Block:
                    Console.BackgroundColor = ConsoleColor.Green;
                    break;
                case Tile.Paddle:
                    Console.BackgroundColor = ConsoleColor.Blue;
                    break;
            }

            Console.Write(' ');
            Console.ResetColor();
        }

        private enum Tile
        {
            Empty = 0,
            Wall = 1,
            Block = 2,
            Paddle = 3,
            Ball = 4,
        }
    }
}