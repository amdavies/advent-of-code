﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Threading.Tasks;

namespace AdventOfCode.Y2019.Solution
{
    public class Day24 : APuzzle
    {
        public Day24() : base(24, 2019, "Planet of Discord") { }

        protected override async Task<string> SolvePartOne()
        {
            var state = new Dictionary<Vector2, bool>();

            var y = 0;
            foreach (var line in this.Input.Split('\n')) {
                var x = 0;
                foreach (var space in line.ToCharArray()) {
                    state[new Vector2(x, y)] = space == '#';
                    x++;
                }

                y++;
            }

            var seen = new HashSet<string>
            {
                string.Join("", state.Select(state => state.Value ? "#" : ".")),
            };

            while (true) {
                // Game of Life
                var newState = new Dictionary<Vector2, bool>();
                foreach (var (position, hasBugs) in state) {
                    var positions = new[]
                    {
                        new Vector2(position.X + 1, position.Y),
                        new Vector2(position.X - 1, position.Y),
                        new Vector2(position.X, position.Y + 1),
                        new Vector2(position.X, position.Y - 1),
                    };
                    var adjacent = 0;
                    foreach (var neightbour in positions)
                        if (state.ContainsKey(neightbour) && state[neightbour])
                            adjacent++;
                    if (!hasBugs) // Check if it needs infesting
                        newState[position] = adjacent == 1 || adjacent == 2;
                    else // Check if it dies
                        newState[position] = adjacent == 1;
                }

                state = newState;

                // Verify state
                string stateString = string.Join("", state.Select(state => state.Value ? "#" : "."));
                if (seen.Contains(stateString)) {
                    var rating = 0;
                    for (var i = 0; i < stateString.Length; i++)
                        if (stateString[i] == '#')
                            rating += (int) Math.Pow(2, i);

                    return rating.ToString();
                }

                seen.Add(stateString);
            }
        }

        protected override async Task<string> SolvePartTwo()
        {
            var state = new Dictionary<Vector3, bool>();

            var y = 0;
            foreach (var line in this.Input.Split('\n')) {
                var x = 0;
                foreach (var space in line.ToCharArray()) {
                    if (x == 2 && y == 2) {
                        x++;
                        continue;
                    }

                    state[new Vector3(x, y, 0)] = space == '#';
                    x++;
                }

                y++;
            }

            for (var minute = 0; minute < 200; minute++) {
                // Add extras as required
                state = this.addBufferLayers(state);

                // Game of Life
                var newState = new Dictionary<Vector3, bool>();
                foreach (var (position, hasBugs) in state) {
                    var neighbours = this.getNeighbours(position);

                    var adjacent = neighbours.Where(v => state.ContainsKey(v) && state[v]).Count();
                    if (!hasBugs) // Check if it needs infesting
                        newState[position] = adjacent == 1 || adjacent == 2;
                    else // Check if it dies
                        newState[position] = adjacent == 1;
                }

                state = newState;
            }

            return state.Count(s => s.Value).ToString();
        }

        private List<Vector3> getNeighbours(Vector3 position)
        {
            var neighbours = new List<Vector3>
            {
                new(position.X + 1, position.Y, position.Z),
                new(position.X - 1, position.Y, position.Z),
                new(position.X, position.Y + 1, position.Z),
                new(position.X, position.Y - 1, position.Z),
            };

            if (Math.Abs(position.X - 2) < double.Epsilon && Math.Abs(position.Y - 2) < double.Epsilon
            ) // Centre position is the next layer
                return new List<Vector3>();

            // Neighbours tile in layer above
            if (position.Y == 0) neighbours.Add(new Vector3(2, 1, position.Z - 1));
            if (Math.Abs(position.Y - 4) < double.Epsilon) neighbours.Add(new Vector3(2, 3, position.Z - 1));
            if (position.X == 0) neighbours.Add(new Vector3(1, 2, position.Z - 1));
            if (Math.Abs(position.X - 4) < double.Epsilon) neighbours.Add(new Vector3(3, 2, position.Z - 1));

            // Neighbours exist in layer below
            if (position.X == 2 && position.Y == 1) {
                neighbours.Add(new Vector3(0, 0, position.Z + 1));
                neighbours.Add(new Vector3(1, 0, position.Z + 1));
                neighbours.Add(new Vector3(2, 0, position.Z + 1));
                neighbours.Add(new Vector3(3, 0, position.Z + 1));
                neighbours.Add(new Vector3(4, 0, position.Z + 1));
            }

            if (position.X == 2 && position.Y == 3) {
                neighbours.Add(new Vector3(0, 4, position.Z + 1));
                neighbours.Add(new Vector3(1, 4, position.Z + 1));
                neighbours.Add(new Vector3(2, 4, position.Z + 1));
                neighbours.Add(new Vector3(3, 4, position.Z + 1));
                neighbours.Add(new Vector3(4, 4, position.Z + 1));
            }

            if (position.Y == 2 && position.X == 1) {
                neighbours.Add(new Vector3(0, 0, position.Z + 1));
                neighbours.Add(new Vector3(0, 1, position.Z + 1));
                neighbours.Add(new Vector3(0, 2, position.Z + 1));
                neighbours.Add(new Vector3(0, 3, position.Z + 1));
                neighbours.Add(new Vector3(0, 4, position.Z + 1));
            }

            if (position.Y == 2 && position.X == 3) {
                neighbours.Add(new Vector3(4, 0, position.Z + 1));
                neighbours.Add(new Vector3(4, 1, position.Z + 1));
                neighbours.Add(new Vector3(4, 2, position.Z + 1));
                neighbours.Add(new Vector3(4, 3, position.Z + 1));
                neighbours.Add(new Vector3(4, 4, position.Z + 1));
            }

            return neighbours;
        }

        private Dictionary<Vector3, bool> addBufferLayers(Dictionary<Vector3, bool> currentState)
        {
            var minActiveZ = currentState.Where(s => s.Value).Min(s => s.Key.Z);
            var maxActiveZ = currentState.Where(s => s.Value).Max(s => s.Key.Z);

            var minZ = currentState.Min(s => s.Key.Z);
            var maxZ = currentState.Max(s => s.Key.Z);

            // On bottom layer, add another beneath us
            if (Math.Abs(minZ - minActiveZ) < double.Epsilon) {
                var z = minZ - 1;
                for (var y = 0; y < 5; y++)
                for (var x = 0; x < 5; x++) {
                    if (x == 2 && y == 2) continue;
                    currentState.Add(new Vector3(x, y, z), false);
                }
            }

            // On top layer, add one above
            if (Math.Abs(maxZ - maxActiveZ) < double.Epsilon) {
                var z = maxZ + 1;
                for (var y = 0; y < 5; y++)
                for (var x = 0; x < 5; x++) {
                    if (x == 2 && y == 2) continue;
                    currentState.Add(new Vector3(x, y, z), false);
                }
            }

            return currentState;
        }
    }
}