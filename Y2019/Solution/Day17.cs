﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Threading.Tasks;
using AdventOfCode.Y2019.Intcode;

namespace AdventOfCode.Y2019.Solution
{
    public class Day17 : APuzzle
    {
        private readonly Lazy<Vm> CpuLoader;

        private Dictionary<Vector2, char> Map = new();

        private Vector2 Start;

        public Day17() : base(17, 2019, "Set and Forget")
        {
            this.CpuLoader = new Lazy<Vm>(this.InitialiseCpu);
        }

        private Vm Cpu => this.CpuLoader.Value;

        protected override async Task<string> SolvePartOne()
        {
            this.BuildMap();

            var sum = 0;
            foreach ((var location, var ascii) in this.Map) {
                // If this point is surrounded by #, then it's an intersection
                var edges = new[]
                {
                    new Vector2(location.X - 1, location.Y),
                    new Vector2(location.X + 1, location.Y),
                    new Vector2(location.X, location.Y - 1),
                    new Vector2(location.X, location.Y + 1),
                };

                var invalid = false;
                foreach (var edge in edges)
                    if (!this.Map.ContainsKey(edge) || this.Map[edge] != '#')
                        invalid = true;

                if (!invalid) sum += (int) (location.X * location.Y);
            }

            return sum.ToString();
        }

        protected override async Task<string> SolvePartTwo()
        {
            this.BuildMap();

            // Store previous nodes to prevent backtracking
            var steps = this.CalculateSteps();

            var instructions = this.CompressInstructions(steps);

            this.Cpu.ResetState();
            this.Cpu.Input = new InputQueue(instructions.ToCharArray().Select(c => (long) c));
            this.Cpu.Memory[0] = 2;

            this.Cpu.Run();

            // Most outputs are map + input prompts, final output is "dust" quantity
            return this.Cpu.Output.Last().ToString();
        }

        private List<(char, int)> CalculateSteps()
        {
            var seen = new HashSet<Vector2>
            {
                this.Start,
            };

            var possibleDirections = new[]
            {
                new Vector2(-1, 0),
                new Vector2(0, -1),
                new Vector2(1, 0),
                new Vector2(0, 1),
            };

            var steps = new List<(char, int)>();

            var currentHeading = 0;
            switch (this.Map[this.Start]) {
                case '<':
                    currentHeading = 0;
                    break;
                case '^':
                    currentHeading = 1;
                    break;
                case '>':
                    currentHeading = 2;
                    break;
                case 'v':
                    currentHeading = 3;
                    break;
            }

            var currentPosition = this.Start;

            while (true) {
                var currentStep = (turn: ' ', steps: 0);
                var turns = new[]
                {
                    ('L', heading: currentHeading - 1 < 0 ? 3 : currentHeading - 1),
                    ('R', heading: (currentHeading + 1) % 4),
                };

                var newDirection = new Vector2(-1, -1);
                foreach (var turn in turns) {
                    var direction = possibleDirections[turn.heading];
                    if (this.Map.ContainsKey(currentPosition + direction)
                        && this.Map[currentPosition + direction] == '#'
                        && !seen.Contains(currentPosition + direction)
                    ) {
                        currentHeading = turn.heading;
                        newDirection = possibleDirections[turn.heading];
                        currentStep.turn = turn.Item1;
                    }
                }

                // If we couldn't find anywhere else to go, it must be the end?
                if (newDirection == new Vector2(-1, -1)) break;

                // Run forwards until we hit a wall
                while (this.Map.ContainsKey(currentPosition + newDirection) &&
                       this.Map[currentPosition + newDirection] == '#') {
                    currentPosition += newDirection;
                    currentStep.steps++;
                }

                steps.Add(currentStep);
            }

            return steps;
        }

        private string CompressInstructions(List<(char turn, int steps)> steps)
        {
            string fullInstructions = string.Join(',', steps.Select(s => $"{s.turn},{s.steps}"));

            for (var a = 1; a <= 8; a++) {
                string aInstruction = string.Join(',', steps.GetRange(0, a).Select(s => $"{s.turn},{s.steps}"));

                if (aInstruction.Length > 20) continue;

                var aReplaced = fullInstructions.Replace(aInstruction, "A");

                var aCount = aReplaced.TakeWhile(d => new[] {'A', ','}.Contains(d)).Count(d => d == 'A');

                for (var b = 1; b <= 8; b++) {
                    string bInstruction = string.Join(',',
                        steps.GetRange(a * aCount, b).Select(s => $"{s.turn},{s.steps}"));

                    if (bInstruction.Length > 20) continue;

                    var bReplaced = aReplaced.Replace(bInstruction, "B");

                    aCount = bReplaced.TakeWhile(d => new[] {'A', 'B', ','}.Contains(d)).Count(d => d == 'A');
                    var bCount = bReplaced.TakeWhile(d => new[] {'A', 'B', ','}.Contains(d)).Count(d => d == 'B');

                    for (var c = 1; c <= 8; c++) {
                        string cInstruction = string.Join(',',
                            steps.GetRange(a * aCount + b * bCount, c).Select(s => $"{s.turn},{s.steps}"));

                        if (cInstruction.Length > 20) continue;

                        var cReplaced = bReplaced.Replace(cInstruction, "C");

                        if (!cReplaced.ToCharArray().Any(l => !new[] {'A', 'B', 'C', ','}.Contains(l)))
                            return $"{cReplaced}\n{aInstruction}\n{bInstruction}\n{cInstruction}\nn\n";
                    }
                }
            }

            return "It failed";
        }

        /// <summary>
        ///     Initialise the CPU with the instruction set provided in the Input
        /// </summary>
        private Vm InitialiseCpu()
        {
            var cpu = new Vm(this.Input);
            return cpu;
        }

        /// <summary>
        ///     Build up the map
        /// </summary>
        private void BuildMap()
        {
            if (this.Map.Count > 0) return;

            this.Map = new Dictionary<Vector2, char>();
            var x = 0;
            var y = 0;
            while (!this.Cpu.HasHalted) {
                this.Cpu.Run();

                while (this.Cpu.Output.Count > 0) {
                    var ascii = (char) this.Cpu.Output.GetOutput();

                    if (ascii == '\n') {
                        y++;
                        x = 0;

                        continue;
                    }

                    var location = new Vector2(x++, y);
                    if (new[] {'^', '<', '>', 'v'}.Contains(ascii)) this.Start = location;

                    this.Map.Add(location, ascii);
                }
            }
        }
    }
}