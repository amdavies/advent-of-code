using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace AdventOfCode.Y2019.Intcode.Networking {
    public class Network {
        private readonly CancellationTokenSource cts;
        private readonly bool runNat;

        private int currentVar;
        public readonly long[] Vars = new long[2];

        private readonly Dictionary<int, Queue<long>> messageQueue = new();
        private readonly Dictionary<int, NetworkBus> machines = new();
        private long lastY = -1;

        public Network(CancellationTokenSource cts, bool runNat = false)
        {
            this.cts = cts;
            this.runNat = runNat;
        }

        public void RegisterBus(int networkId, NetworkBus bus)
        {
            this.messageQueue[networkId] = new Queue<long>(new long[] {networkId});
            this.machines[networkId] = bus;
        }

        public int MessageCount(int networkId)
        {
            if (!this.messageQueue.ContainsKey(networkId)) {
                return 0;
            }

            var count = this.messageQueue[networkId].Count;

            // Only running NAT on an empty request from device 0 speeds up the entire process
            if (count == 0 && this.runNat && networkId == 0 && this.Vars[1] > 0 && this.machines[networkId].IsIdling && this.machines.All(b => b.Value.IsIdling)) {
                // If we have a repeated Y value, stop execution
                if (this.lastY == this.Vars[1]) {
                    this.cts.Cancel();

                    return count;
                }

                // Send the data to machine 0 to restart the process
                this.SendMessage(0, this.Vars[0]);
                this.SendMessage(0, this.Vars[1]);

                // Store the Y value that was sent
                this.lastY = this.Vars[1];

                // Reset vars so we don't resend the same messages
                this.Vars[1] = 0;
            }

            return count;

        }

        public long GetMessage(int networkId)
        {
            return this.messageQueue.ContainsKey(networkId) ? this.messageQueue[networkId].Dequeue() : 0;
        }

        public void SendMessage(int networkId, long data)
        {
            // Message targeting NAT
            if (networkId == 255) {
                this.Vars[this.currentVar] = data;
                this.currentVar = (this.currentVar + 1) % 2;

                if (!this.runNat && this.Vars[1] > 0) {
                    this.cts.Cancel();
                }

                return;
            }

            // Safety check
            if (!this.messageQueue.ContainsKey(networkId)) {
                return;
            }

            this.messageQueue[networkId].Enqueue(data);
        }
    }
}
