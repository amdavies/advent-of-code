using System;
using System.Collections;
using System.Collections.Generic;

namespace AdventOfCode.Y2019.Intcode.Networking {
    public class NetworkBus : IInput, IOutput {
        public bool IsIdling;

        private readonly int networkId;
        private readonly Network network;
        private readonly Queue<long> buffer = new();

        public int Count => this.network.MessageCount(this.networkId);

        public NetworkBus(int networkId, Network network)
        {
            network.RegisterBus(networkId, this);
            this.network = network;
            this.networkId = networkId;
        }

        public void WriteOutput(long output)
        {
            this.IsIdling = false;
            this.buffer.Enqueue(output);

            if (this.buffer.Count == 3) {
                var target = (int)this.buffer.Dequeue();

                this.network.SendMessage(target, this.buffer.Dequeue());
                this.network.SendMessage(target, this.buffer.Dequeue());
            }
        }

        public long GetOutput()
        {
            throw new NotImplementedException();
        }

        public void ClearInputs()
        {
            throw new NotImplementedException();
        }

        public void AddInput(long input)
        {
            throw new NotImplementedException();
        }

        public long? GetNextInput()
        {
            if (this.Count != 0) {
                return this.network.GetMessage(this.networkId);
            }

            this.IsIdling = true;
            return -1;

        }

        public IEnumerator<long> GetEnumerator()
        {
            throw new NotImplementedException();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }
    }
}
