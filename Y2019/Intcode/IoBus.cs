using System.Collections;
using System.Collections.Generic;

namespace AdventOfCode.Y2019.Intcode {
    /// <summary>
    /// Handler for input and output on a shared bus
    /// </summary>
    public class IoBus : IInput, IOutput {
        private Queue<long> storage;

        public int Count => this.storage.Count;


        public IoBus()
        {
            this.storage = new Queue<long>();
        }

        public IoBus(IEnumerable<long> data)
        {
            this.storage = new Queue<long>(data);
        }

        public void ClearInputs()
        {
            this.storage = new Queue<long>();
        }

        public void AddInput(long input)
        {
            this.storage.Enqueue(input);
        }

        public long? GetNextInput()
        {
            if (this.storage.Count == 0) {
                return null;
            }

            return this.storage.Dequeue();
        }

        public void WriteOutput(long output)
        {
            this.storage.Enqueue(output);
        }

        public long GetOutput()
        {
            return this.storage.Dequeue();
        }

        public IEnumerator<long> GetEnumerator()
        {
            return this.storage.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }
    }
}
