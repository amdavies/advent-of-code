using System.Collections;
using System.Collections.Generic;

namespace AdventOfCode.Y2019.Intcode {
    public class OutputQueue : IOutput {
        private Queue<long> queue = new();

        public int Count => this.queue.Count;

        public void WriteOutput(long output)
        {
            this.queue.Enqueue(output);
        }

        public long GetOutput()
        {
            return this.queue.Dequeue();
        }

        public IEnumerator<long> GetEnumerator()
        {
            return this.queue.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.GetEnumerator();
        }
    }
}
