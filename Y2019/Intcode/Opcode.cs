namespace AdventOfCode.Y2019.Intcode
{
    enum Opcode
    {
        Add = 1,

        Multiply = 2,

        Store = 3,

        Read = 4,

        JumpTrue = 5,

        JumpFalse = 6,

        Less = 7,

        Eq = 8,
        
        AdjustBase = 9,
        
        Stop = 99
    }
}