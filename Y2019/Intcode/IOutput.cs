using System.Collections.Generic;

namespace AdventOfCode.Y2019.Intcode {
    public interface IOutput : IEnumerable<long> {
        int Count { get; }

        void WriteOutput(long output);
        long GetOutput();
    }
}
