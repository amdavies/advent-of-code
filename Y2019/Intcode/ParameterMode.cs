namespace AdventOfCode.Y2019.Intcode
{
    enum ParameterMode
    {
        Position = 0,
        Immediate = 1,
        Relative = 2,
    }
}