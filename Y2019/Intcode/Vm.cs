using System;
using System.Collections.Generic;
using System.Linq;

namespace AdventOfCode.Y2019.Intcode
{
    public class Vm
    {
        public IInput Input;
        public readonly IOutput Output;

        private readonly bool DebugMode;

        private List<long> initialMemory = new();
        public List<long> Memory = new();

        private int pointer;
        private int relativeBase;

        private readonly Dictionary<Opcode, Func<int, bool>> commands;

        public bool HasHalted;

        public Vm(string instructions) : this(instructions, new InputQueue(), new OutputQueue()) { }

        public Vm(string instructions, IInput inputQueue) : this(instructions, inputQueue, new OutputQueue()) { }

        public Vm(string instructions, IInput inputQueue, IOutput outputQueue)
        {
            this.Input = inputQueue;
            this.Output = outputQueue;
            this.DebugMode = false;

            this.SetInstructions(instructions);

            // Initialise valid commands
            this.commands = new Dictionary<Opcode, Func<int, bool>>() {
                {Opcode.Add, this.Add},
                {Opcode.Multiply, this.Multiply},
                {Opcode.Store, this.Store},
                {Opcode.Read, this.Read},
                {Opcode.JumpTrue, this.JumpNotZero},
                {Opcode.JumpFalse, this.JumpZero},
                {Opcode.Less, this.Less},
                {Opcode.Eq, this.Eq},
                {Opcode.AdjustBase, this.AdjustBase},
                {Opcode.Stop, this.Halt},
            };
        }

        /// <summary>
        /// Resets CPU state to initial instruction set
        /// </summary>
        public void ResetState()
        {
            this.pointer = 0;
            this.HasHalted = false;
            this.Memory = new List<long>(this.initialMemory.ToArray());
        }

        private void SetInstructions(string instructions)
        {
            this.initialMemory = Array.ConvertAll(instructions.Split(','), long.Parse).ToList();
            this.Memory = new List<long>(this.initialMemory.ToArray());
        }

        /// <summary>
        /// Run a single opcode step
        /// </summary>
        public void Step()
        {
            int modes = (int) this.Memory[this.pointer] / 100;
            Opcode op = (Opcode) (this.Memory[this.pointer] % 100);

            this.commands[op](modes);
        }

        public void Run()
        {
            bool pause = false;
            while (this.pointer >= 0 && this.pointer < this.Memory.Count && !pause && !this.HasHalted) {
                int modes = (int) this.Memory[this.pointer] / 100;
                Opcode op = (Opcode) (this.Memory[this.pointer] % 100);

                pause = this.commands[op](modes);
            }
        }

        /// <summary>
        /// Gets the memory positions for all parameters
        /// </summary>
        /// <param name="modes">The mode values for the command</param>
        /// <param name="count">Number of positions to calculate</param>
        /// <returns></returns>
        private int[] GetPositions(int modes, int count)
        {
            int[] positions = new int[count];
            for (int i = 0; i < count; i++)
            {
                int position = (int) this.Memory[this.pointer + i + 1];
                if ((ParameterMode)(modes % 10) == ParameterMode.Immediate)
                {
                    position = this.pointer + i + 1;
                }

                if ((ParameterMode) (modes % 10) == ParameterMode.Relative)
                {
                    position = (int) (this.relativeBase + (this.Memory[this.pointer + i + 1]));
                }
                this.FixList(position);
                positions[i] = position;

                modes /= 10;
            }

            return positions;
        }

        /// <summary>
        /// Updates the list to have the correct number of entries to write to index
        /// </summary>
        /// <param name="index">Index requiring write access</param>
        private void FixList(int index)
        {
            while (this.Memory.Count <= index)
            {
                this.Memory.Add(0);
            }
        }

        /// <summary>
        /// Adds together numbers read from two positions and stores the result in a third position
        /// </summary>
        /// <param name="modes">Modes for each position</param>
        /// <returns>Should the program halt</returns>
        private bool Add(int modes)
        {
            this.dumpOperation(Opcode.Add, 3, modes);
            var positions = this.GetPositions(modes, 3);

            this.FixList(positions[2]);
            var result = this.Memory[positions[0]] + this.Memory[positions[1]];
            this.Memory[positions[2]] = result;
            this.pointer += 4;

            return false;
        }

        /// <summary>
        /// Multiplies numbers read from two positions and stores the result in a third position
        /// </summary>
        /// <param name="modes">Modes for each position</param>
        /// <returns>Should the program halt</returns>
        private bool Multiply(int modes)
        {
            this.dumpOperation(Opcode.Multiply, 3, modes);
            var positions = this.GetPositions(modes, 3);

            this.FixList(positions[2]);
            this.Memory[positions[2]] = this.Memory[positions[0]] * this.Memory[positions[1]];
            this.pointer += 4;

            return false;
        }

        /// <summary>
        /// Stores an input value to position 1. Currently only reads system id.
        /// </summary>
        /// <param name="modes">Modes for each parameter</param>
        /// <returns>Should the program halt</returns>
        private bool Store(int modes)
        {
            this.dumpOperation(Opcode.Store, 1, modes);
            var v = this.GetPositions(modes, 1)[0];

            // Pause for input if we have none
            var input = this.Input.GetNextInput();
            if (input == null) {
                return true;
            }

            this.Memory[v] = input.Value;

            this.pointer += 2;

            return false;
        }

        /// <summary>
        /// Outputs the value at position 1 and pauses execution
        /// </summary>
        /// <param name="modes">Modes for each parameter</param>
        /// <returns>Should the program halt</returns>
        private bool Read(int modes)
        {
            this.dumpOperation(Opcode.Read, 1, modes);
            var v = this.GetPositions(modes, 1)[0];
            this.pointer += 2;

            this.Output.WriteOutput(this.Memory[v]);

            return false;
        }

        /// <summary>
        /// Sets pointer to position identified in second parameter if first parameter is non-zero
        /// </summary>
        /// <param name="modes">Modes for each parameter</param>
        /// <returns>Should the program halt</returns>
        private bool JumpNotZero(int modes)
        {
            this.dumpOperation(Opcode.JumpTrue, 2, modes);
            var positions = this.GetPositions(modes, 2);

            if (this.Memory[positions[0]] != 0)
            {
                this.pointer = (int) this.Memory[positions[1]];
            }
            else
            {
                this.pointer += 3;
            }

            return false;
        }

        /// <summary>
        /// Sets pointer to position identified in second parameter if first parameter is zero
        /// </summary>
        /// <param name="modes">Modes for each parameter</param>
        /// <returns>Should the program halt</returns>
        private bool JumpZero(int modes)
        {
            this.dumpOperation(Opcode.JumpFalse, 2, modes);
            var positions = this.GetPositions(modes, 2);
            if (this.Memory[positions[0]] == 0)
            {
                this.pointer = (int) this.Memory[positions[1]];
            }
            else
            {
                this.pointer += 3;
            }

            return false;
        }

        /// <summary>
        /// Stores if parameter 1 is less than parameter 2 in position 3
        /// </summary>
        /// <param name="modes">Modes for each parameter</param>
        /// <returns>Should the program halt</returns>
        private bool Less(int modes)
        {
            this.dumpOperation(Opcode.Less, 3, modes);
            var positions = this.GetPositions(modes, 3);
            this.FixList(positions[2]);
            this.Memory[positions[2]] = this.Memory[positions[0]] < this.Memory[positions[1]] ? 1 : 0;
            this.pointer += 4;

            return false;
        }

        /// <summary>
        /// Stores if parameter 1 is equal to parameter 2 in position 3
        /// </summary>
        /// <param name="modes">Modes for each parameter</param>
        /// <returns>Should the program pause</returns>
        private bool Eq(int modes)
        {
            this.dumpOperation(Opcode.Eq, 3, modes);
            var positions = this.GetPositions(modes, 3);
            this.FixList(positions[2]);

            this.Memory[positions[2]] = (this.Memory[positions[0]] == this.Memory[positions[1]]) ? 1 : 0;
            this.pointer += 4;

            return false;
        }

        /// <summary>
        /// Adjusts the relative base by the value in parameter 1
        /// </summary>
        /// <param name="modes">Modes for each parameter</param>
        /// <returns>Should the program pause</returns>
        private bool AdjustBase(int modes)
        {
            this.dumpOperation(Opcode.AdjustBase, 0, modes);

            var position = this.GetPositions(modes, 1)[0];
            this.relativeBase += (int) this.Memory[position];

            this.pointer += 2;

            return false;
        }


        /// <summary>
        /// Halts program execution
        /// </summary>
        /// <param name="modes">Unused</param>
        /// <returns>Always true</returns>
        private bool Halt(int modes)
        {
            this.dumpOperation(Opcode.Stop, 0, modes);
            this.HasHalted = true;

            return true;
        }


        private static HashSet<int> usedOperations = new();
        private void dumpOperation(Opcode operation, int paramCount, int modes)
        {
            if (!this.DebugMode) {
                return;
            }

            // Write intcode + params
            Console.Write($"( {this.Memory[this.pointer]}");
            for (int i = 1; i <= paramCount; i++) {
                Console.Write($", {this.Memory[this.pointer + i]}");
            }
            Console.WriteLine(")");

            var positions = this.GetPositions(modes, paramCount);
            foreach (var position in positions) {
                Console.WriteLine($"Position: {position}, Value: {this.Memory[position]}");
            }

            return;
        }
    }
}
