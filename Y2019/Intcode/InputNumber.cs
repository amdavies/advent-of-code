namespace AdventOfCode.Y2019.Intcode {
    public class InputNumber : IInput {
        protected long Number;

        public int Count => 1;

        public InputNumber(long number)
        {
            this.Number = number;
        }

        public void ClearInputs()
        {

        }

        public void AddInput(long input)
        {

        }

        public long? GetNextInput()
        {
            return this.Number;
        }
    }
}
