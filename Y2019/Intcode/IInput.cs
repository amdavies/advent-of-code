namespace AdventOfCode.Y2019.Intcode
{
    public interface IInput
    {
        int Count { get; }

        void ClearInputs();

        void AddInput(long input);

        long? GetNextInput();
    }
}
