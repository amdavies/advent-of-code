using System.Collections.Generic;

namespace AdventOfCode.Y2019.Intcode {
    public class InputQueue : IInput {
        private Queue<long> queue = new();

        public int Count => this.queue.Count;

        public InputQueue() {}

        public InputQueue(IEnumerable<long> data)
        {
            this.queue = new Queue<long>(data);
        }

        public void ClearInputs()
        {
            this.queue = new Queue<long>();
        }

        public void AddInput(long input)
        {
            this.queue.Enqueue(input);
        }

        public long? GetNextInput()
        {
            if (this.queue.Count == 0) {
                return null;
            }

            return this.queue.Dequeue();
        }
    }
}
