﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdventOfCode.Y2020.Solution
{
    using ParsedType = IEnumerable<Food>;

    public class Day21 : APuzzle<ParsedType>
    {
        /// <inheritdoc/>
        public Day21() : base(21, 2020, "Allergen Assessment") { }

        /// <inheritdoc/>
        protected override ParsedType ParseInput()
        {
            var lines = this.Input.Split('\n');

            return lines.Select(l =>
            {
                var parts = l.Split(" (contains ");

                return new Food(
                    parts[0].Split(' '),
                    parts[1].TrimEnd(')').Split(", ")
                );
            });
        }

        /// <inheritdoc/>
        protected override async Task<string> SolvePartOne()
        {
            var foods = this.ParsedInput.ToList();

            HashSet<string> riskyIngredients = GetPossibleAllergens().SelectMany(r => r.Value).Distinct().ToHashSet();

            return foods.SelectMany(f => f.Ingredients).Count(i => !riskyIngredients.Contains(i)).ToString();
        }

        /// <inheritdoc/>
        protected override async Task<string> SolvePartTwo()
        {
            var riskyIngredients = this.GetPossibleAllergens();
            Dictionary<string, string> badIngredients = new();
            while (riskyIngredients.Any(a => a.Value.Count == 1)) {
                var (allergen, possible) = riskyIngredients.First(a => a.Value.Count == 1);
                string ingredient = possible[0];
                foreach (var a in riskyIngredients.Where(i => i.Value.Count > 1)) {
                    a.Value.Remove(ingredient);
                }

                badIngredients.Add(allergen, ingredient);
                riskyIngredients.Remove(allergen);
            }

            return string.Join(',', badIngredients.OrderBy(i => i.Key).Select(i => i.Value));
        }

        private Dictionary<string, List<string>> GetPossibleAllergens()
        {
            var foods = this.ParsedInput.ToList();

            var foodByAllergen = foods.SelectMany(f => f.Allergens.Select(a => (a, f))).ToLookup(a => a.a, a => a.f);
            var ingredientByAllergen = foodByAllergen
                .ToDictionary(
                    g => g.Key,
                    g => g.Skip(1).Aggregate(g.First().Ingredients.AsEnumerable(), (i, g2) => i.Intersect(g2.Ingredients))
                        .ToList()
                );
            return ingredientByAllergen;
        }
    }

    public record Food(string[] Ingredients, string[] Allergens);
}
