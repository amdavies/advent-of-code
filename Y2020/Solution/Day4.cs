﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace AdventOfCode.Y2020.Solution
{
    using ParsedType = IEnumerable<string>;

    public class Day4 : APuzzle
    {
        private readonly string[] requiredFields = {
            "byr:",
            "iyr:",
            "eyr:",
            "hgt:",
            "hcl:",
            "ecl:",
            "pid:",
        };

        private readonly string[] validEyeColours =
        {
            "amb",
            "blu",
            "brn",
            "gry",
            "grn",
            "hzl",
            "oth",
        };

        /// <summary>
        /// Parsed input stream
        /// </summary>
        private ParsedType ParsedInput => this.parsedInputLoader.Value;

        /// <summary>
        /// Lazy loader for parsed input
        /// </summary>
        private readonly Lazy<ParsedType> parsedInputLoader;

        public Day4() : base(4, 2020, "Passport Processing")
        {
            this.parsedInputLoader = new Lazy<ParsedType>(this.ParseInput);
        }

        /// <summary>
        /// Parse the input string to be more useful
        /// </summary>
        private ParsedType ParseInput()
        {
            return this.Input.Split("\n\n");
        }

        /// <inheritdoc/>
        protected override async Task<string> SolvePartOne()
        {
            return this.ParsedInput.Count(p => this.requiredFields.All(p.Contains)).ToString();
        }

        /// <inheritdoc/>
        protected override async Task<string> SolvePartTwo()
        {
            return this.ParsedInput.Count(p => this.requiredFields.All(field => this.IsFieldValid(field, p))).ToString();
        }

        /// <summary>
        ///     Verify if a required field is valid within the passport string
        /// </summary>
        /// <param name="field">The field name (incl :)</param>
        /// <param name="passport">The full password string</param>
        private bool IsFieldValid(string field, string passport)
        {
            if (!passport.Contains(field)) {
                return false;
            }

            var value = passport.Split(field)[1].Split(' ', '\n')[0];

            return field switch
            {
                "byr:" => int.Parse(value) is >= 1920 and <= 2002,
                "iyr:" => int.Parse(value) is >= 2010 and <= 2020,
                "eyr:" => int.Parse(value) is >= 2020 and <= 2030,
                "hgt:" => value.Length >= 4 &&
                          (
                              (int.Parse(value.Substring(0, value.Length - 2)) is >= 59 and <= 76 && value.EndsWith("in")) ||
                              (int.Parse(value.Substring(0, value.Length - 2)) is >= 150 and <= 193 && value.EndsWith("cm"))
                          ),
                "hcl:" => value[0] == '#' && value.Length == 7 && value.Skip(1).All(c => "0123456789abcdef".Contains(c)),
                "ecl:" => this.validEyeColours.Contains(value),
                "pid:" => value.Length == 9 && value.All(d => "0123456789".Contains(d)),
                _ => false,
            };
        }
    }
}
