﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdventOfCode.Y2020.Solution
{
    using ParsedType = IList<char[]>;

    public class Day11 : APuzzle
    {
        /// <summary>
        /// Parsed input stream
        /// </summary>
        private ParsedType ParsedInput => this.parsedInputLoader.Value;

        /// <summary>
        /// Valid direction vectors to scan
        /// </summary>
        private readonly (int, int)[] directions = {
            (-1, -1),
            (-1, 0),
            (-1, 1),
            (0, -1),
            (0, 1),
            (1, -1),
            (1, 0),
            (1, 1),
        };

        /// <summary>
        /// Maximum X position in the input
        /// </summary>
        private int maxX;

        /// <summary>
        /// Maximum y position in the input
        /// </summary>
        private int maxY;

        /// <summary>
        /// Lazy loader for parsed input
        /// </summary>
        private readonly Lazy<ParsedType> parsedInputLoader;

        public Day11() : base(11, 2020, "Seating System")
        {
            this.parsedInputLoader = new Lazy<ParsedType>(this.ParseInput);
        }

        /// <summary>
        /// Parse the input string to be more useful
        /// </summary>
        private ParsedType ParseInput()
        {
            var rows = this.Input.Split('\n').Select(s => s.ToCharArray()).ToArray();
            this.maxX = rows[0].Length;
            this.maxY = rows.Length;

            return rows;
        }

        /// <inheritdoc/>
        protected override async Task<string> SolvePartOne()
        {
            return this.CountStabilized(1, 4);
        }

        /// <inheritdoc/>
        protected override async Task<string> SolvePartTwo()
        {
            return this.CountStabilized(this.ParsedInput.Count, 5);
        }

        private string CountStabilized(int range, int threshold)
        {
            ParsedType seats = new List<char[]>(this.ParsedInput);

            while (true) {
                bool changes = false;
                ParsedType nextSeats = new List<char[]>(seats.Select(c => c.ToArray()));

                for (int y = 0; y < this.maxY; y++) {
                    for (int x = 0; x < this.maxX; x++) {
                        if (seats[y][x] is '.') {
                            continue;
                        }

                        bool isOccupied = seats[y][x] is '#';

                        bool nextVal = this.IsUnderThreshold(x, y, range, seats, isOccupied ? threshold : 1);

                        if (nextVal == isOccupied) continue;

                        changes = true;
                        nextSeats[y][x] = nextVal ? '#' : 'L';
                    }
                }

                if (!changes) {
                    return seats.Sum(row => row.Count(c => c == '#')).ToString();
                }

                seats = new List<char[]>(nextSeats.Select(c => c.ToArray()));
            }
        }

        private int Look(int x, int y, (int x, int y) direction, int range, ParsedType seats)
        {
            var (xMove, yMove) = direction;
            for (int step = 0; step < range; step++) {
                x += xMove;
                y += yMove;

                if (x < 0 || y < 0 || x >= this.maxX || y >= this.maxY) {
                    return 0;
                }

                if (seats[y][x] is not '.') {
                    return seats[y][x] is '#' ? 1 : 0;
                }
            }

            return 0;

        }

        private bool IsUnderThreshold(int x, int y, int range, ParsedType seats, int max)
        {
            int visible = 0;
            foreach (var direction in this.directions) {
                visible += this.Look(x, y, direction, range, seats);

                if (visible >= max) {
                    return false;
                }
            }

            return true;
        }
    }
}
