﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace AdventOfCode.Y2020.Solution
{
    using ParsedType = Dictionary<string, Dictionary<string, int>>;

    public class Day7 : APuzzle
    {
        /// <summary>
        /// Parsed input stream
        /// </summary>
        private ParsedType ParsedInput => this.parsedInputLoader.Value;

        /// <summary>
        /// Lazy loader for parsed input
        /// </summary>
        private readonly Lazy<ParsedType> parsedInputLoader;

        public Day7() : base(07, 2020, "Handy Haversacks")
        {
            this.parsedInputLoader = new Lazy<ParsedType>(this.ParseInput);
        }

        /// <summary>
        /// Parse the input string to be more useful
        /// </summary>
        private ParsedType ParseInput()
        {
            Regex rx = new(@"(.*) bags contain(?: (\d .*?) bag(?:s)?[,.])*");

            return this.Input.Split('\n')
                .Select(r => rx.Match(r))
                .ToDictionary(
                    b => b.Groups[1].Value,
                    b => b.Groups[2].Captures
                        .Select(c => c.Value.Split(' ', 2))
                        .ToDictionary(i => i[1], i => int.Parse(i[0]))
                );
        }

        /// <inheritdoc/>
        protected override async Task<string> SolvePartOne()
        {
            bool ContainsGold(string colour) =>
                this.ParsedInput[colour].ContainsKey("shiny gold") ||
                this.ParsedInput[colour].Any(bag => ContainsGold(bag.Key));

            return this.ParsedInput.Count(bag => ContainsGold(bag.Key)).ToString();
        }

        /// <inheritdoc/>
        protected override async Task<string> SolvePartTwo()
        {
            int RequiredBags(string colour) => this.ParsedInput[colour].Sum(v => v.Value * (1 + RequiredBags(v.Key)));

            return RequiredBags("shiny gold").ToString();
        }
    }
}
