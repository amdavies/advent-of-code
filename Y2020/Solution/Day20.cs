﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace AdventOfCode.Y2020.Solution
{
    using ParsedType = IEnumerable<Tile20>;

    public class Day20 : APuzzle<ParsedType>
    {
        /// <inheritdoc/>
        public Day20() : base(20, 2020, "Jurassic Jigsaw") { }

        /// <inheritdoc/>
        protected override ParsedType ParseInput()
        {
            return this.Input.Split("\n\n").Select(i =>
            {
                var lines = i.Split('\n');
                return new Tile20(int.Parse(lines[0].Replace("Tile ", "").Replace(":", "")), lines.Skip(1).ToArray());
            });
        }

        /// <inheritdoc/>
        protected override async Task<string> SolvePartOne()
        {
            int gridSize = (int) Math.Sqrt(this.ParsedInput.Count());
            var grid = this.BuildGrid();

            return (grid[(0, 0)].Id * grid[(0, gridSize - 1)].Id * grid[(gridSize - 1, 0)].Id *
                    grid[(gridSize - 1, gridSize - 1)].Id).ToString();
        }

        /// <inheritdoc/>
        protected override async Task<string> SolvePartTwo()
        {
            string[] monster =
            {
                "                  # ",
                "#    ##    ##    ###",
                " #  #  #  #  #  #   ",
            };
            int gridSize = (int) Math.Sqrt(this.ParsedInput.Count());
            var grid = this.BuildGrid();
            List<string> image = new();
            for (int tileY = 0; tileY < gridSize; tileY++) {
                for (int cellY = 1; cellY < 9; cellY++) {
                    string row = "";
                    for (int tileX = 0; tileX < gridSize; tileX++) {
                        row += grid[(tileX, tileY)].InnerRow(cellY);
                    }

                    image.Add(row);
                }
            }

            // My puzzle doesn't need rotations and has easy to find monsters... the below is actually good enough to get the answer
            // Regex reg = new(@"#....##....##....###");
            // int estimatedCount = reg.Matches(string.Join('\n', image)).Count;
            // return (image.Sum(r => r.Count(c => c is '#')) - (estimatedCount * 15)).ToString();

            Tile20 imageTile = new(0, image);
            for (int pos = 0; pos < 9; pos++) {
                int monsterCount = 0;

                for (int y = 0; y < imageTile.Size - 3; y++) {
                    for (int x = 0; x < imageTile.Size - 20; x++) {
                        bool match = true;
                        while (match) {
                            for (int monsterY = 0; monsterY < 3 && match; monsterY++) {
                                for (int monsterX = 0; monsterX < 20 && match; monsterX++) {
                                    if (monster[monsterY][monsterX] is '#' && imageTile.Cell(x + monsterX, y + monsterY) is not '#') {
                                        match = false;
                                    }
                                }
                            }

                            if (match) {
                                monsterCount++;
                                break;
                            }
                        }
                    }
                }

                if (monsterCount > 0) {
                    return (image.Sum(r => r.Count(c => c is '#')) - (monsterCount * 15)).ToString();
                }
                imageTile.NextPosition();
            }

            throw new Exception("Couldn't find the monster");
        }

        private Dictionary<(int x, int y), Tile20> BuildGrid()
        {
            int gridSize = (int) Math.Sqrt(this.ParsedInput.Count());
            List<Tile20> remainingTiles = this.ParsedInput.ToList();

            Tile20? FindAdjacentTile(string? edgeAbove, string? edgeLeft)
            {
                foreach (var tile in remainingTiles) {
                    for (var pos = 0; pos < 8; pos++) {
                        bool matchesAbove = edgeAbove != null
                            ? tile.TopEdge == edgeAbove
                            : !remainingTiles.Any(t => t.Id != tile.Id && t.AllEdges.Contains(tile.TopEdge));
                        bool matchesLeft = edgeLeft != null
                            ? tile.LeftEdge == edgeLeft
                            : !remainingTiles.Any(t => t.Id != tile.Id && t.AllEdges.Contains(tile.LeftEdge));

                        if (matchesAbove && matchesLeft) {
                            return tile;
                        }

                        tile.NextPosition();
                    }
                }

                return null;
            }

            Dictionary<(int x, int y), Tile20> grid = new();
            for (int y = 0; y < gridSize; y++) {
                for (int x = 0; x < gridSize; x++) {
                    string? edgeAbove = y == 0 ? null : grid[(x, y - 1)].BottomEdge;
                    string? edgeLeft = x == 0 ? null : grid[(x - 1, y)].RightEdge;

                    Tile20? tile = FindAdjacentTile(edgeAbove, edgeLeft);

                    grid[(x, y)] = tile ?? throw new Exception($"My grid is wrong! Failed after {grid.Count} tiles");
                    remainingTiles.Remove(tile);
                }
            }

            return grid;
        }
    }

    public class Tile20
    {
        public readonly long Id;
        public readonly List<string> AllEdges = new();
        public readonly int Size;

        private readonly IReadOnlyList<string> image;
        private int position;
        private bool flipped;
        private readonly int maxPos;

        public Tile20(long id, IReadOnlyList<string> image)
        {
            this.Id = id;
            this.image = image;

            this.Size = image.Count;
            this.maxPos = image.Count - 1;
            this.CalculateEdges();
        }

        public void NextPosition()
        {
            this.position++;
            if (this.position == 4) {
                this.position = 0;
                this.flipped = !this.flipped;
            }
        }

        private void CalculateEdges()
        {
            this.AllEdges.Add(this.image[0]);
            this.AllEdges.Add(new string(this.image.Select(s => s[this.maxPos]).ToArray()));
            this.AllEdges.Add(new string(this.image[this.maxPos].Reverse().ToArray()));
            this.AllEdges.Add(new string(this.image.Reverse().Select(s => s[0]).ToArray()));

            this.AllEdges.Add(new string(this.image[0].Reverse().ToArray()));
            this.AllEdges.Add(new string(this.image.Reverse().Select(s => s[this.maxPos]).ToArray()));
            this.AllEdges.Add(this.image[this.maxPos]);
            this.AllEdges.Add(new string(this.image.Select(s => s[0]).ToArray()));
        }

        private string GetEdge(int x, int y, int deltaX, int deltaY)
        {
            string edge = "";
            for (int i = 0; i <= this.maxPos; i++) {
                edge += this.Cell(x, y);
                x += deltaX;
                y += deltaY;
            }

            return edge;
        }

        public char Cell(int x, int y)
        {
            (x, y) = this.position switch
            {
                0 => (x, y),
                1 => (this.maxPos - y, x),
                2 => (this.maxPos - x, this.maxPos - y),
                3 => (y, this.maxPos - x),
                _ => throw new Exception("I made an oopsie"),
            };

            if (this.flipped) {
                y = this.maxPos - y;
            }

            return this.image[y][x];
        }

        /*public string TopEdge => this.AllEdges[this.position + (this.flipped ? 4 : 0)];
        public string RightEdge => this.AllEdges[(this.position + 1) % 4 + (this.flipped ? 4 : 0)];
        public string BottomEdge => this.AllEdges[(this.position + 2) % 4 + (this.flipped ? 0 : 4)];
        public string LeftEdge => this.AllEdges[(this.position + 3) % 4 + (this.flipped ? 0 : 4)];*/
        public string TopEdge => GetEdge(0, 0, 1, 0);
        public string LeftEdge => GetEdge(0, 0, 0, 1);
        public string RightEdge => GetEdge(this.maxPos, 0, 0, 1);
        public string BottomEdge => GetEdge(0, this.maxPos, 1, 0);

        public string InnerRow(int rowNum)
        {
            string row = "";
            for (int x = 1; x < 9; x++) {
                row += this.Cell(x, rowNum);
            }

            return row;
        }
    }
}
