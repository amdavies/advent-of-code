﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdventOfCode.Y2020
{
    using ParsedType = IEnumerable<string>;

    public class Day24 : APuzzle<ParsedType>
    {
        private readonly Dictionary<string, (int q, int r)> directions = new()
        {
            {"self", (0, 0)},
            {"e", (1, 0)},
            {"se", (0, -1)},
            {"sw", (-1, -1)},
            {"w", (-1, 0)},
            {"nw", (0, 1)},
            {"ne", (1, 1)},
        };

        /// <inheritdoc/>
        public Day24() : base(24, 2020, "Lobby Layout") { }

        /// <inheritdoc/>
        protected override ParsedType ParseInput()
        {
            return this.Input.Split('\n');
        }

        /// <inheritdoc/>
        protected override async Task<string> SolvePartOne() => this.BuildGrid().Count(kvp => kvp.Value).ToString();

        /// <inheritdoc/>
        protected override async Task<string> SolvePartTwo()
        {
            var hexGrid = this.BuildGrid();
            var flippedTiles = hexGrid.Where(kvp => kvp.Value).Select(kvp => kvp.Key).ToHashSet();

            for (int i = 0; i < 100; i++) {
                HashSet<(int q, int r)> nextSet = new();
                foreach (var tile in flippedTiles.SelectMany(this.GetNeighbours).ToHashSet()) {
                    var flippedNeighbours = this.GetNeighbours(tile).Count(t => flippedTiles.Contains(t) && t != tile);

                    if (flippedNeighbours is 2 || flippedNeighbours is 1 && flippedTiles.Contains(tile)) {
                        nextSet.Add(tile);
                    }
                }

                flippedTiles = nextSet;
            }

            return flippedTiles.Count.ToString();
        }

        private Dictionary<(int q, int r), bool> BuildGrid()
        {
            Dictionary<(int q, int r), bool> hexGrid = new();

            foreach (string instructionLine in this.ParsedInput) {
                var tile = (q: 0, r: 0);
                string instruction = instructionLine;
                while (instruction is not "") {
                    foreach (var (direction, vector) in this.directions) {
                        if (!instruction.StartsWith(direction)) continue;

                        instruction = instruction.Substring(direction.Length);
                        tile = (tile.q + vector.q, tile.r + vector.r);
                    }
                }

                hexGrid[tile] = !hexGrid.GetValueOrDefault(tile);
            }

            return hexGrid;
        }

        private HashSet<(int q, int r)> GetNeighbours((int q, int r) tile) => this.directions.Values.Select(vector => (tile.q + vector.q, tile.r + vector.r)).ToHashSet();
    }
}
