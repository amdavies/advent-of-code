﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdventOfCode.Y2020.Solution
{
    using ParsedType = IEnumerable<string>;

    public class Day6 : APuzzle
    {
        /// <summary>
        /// Parsed input stream
        /// </summary>
        private ParsedType ParsedInput => this.parsedInputLoader.Value;

        /// <summary>
        /// Lazy loader for parsed input
        /// </summary>
        private readonly Lazy<ParsedType> parsedInputLoader;

        public Day6() : base(06, 2020, "Custom Customs")
        {
            this.parsedInputLoader = new Lazy<ParsedType>(this.ParseInput);
        }

        /// <summary>
        /// Parse the input string to be more useful
        /// </summary>
        private ParsedType ParseInput() => this.Input.Split("\n\n");

        /// <inheritdoc/>
        protected override async Task<string> SolvePartOne() => this.ParsedInput.Sum(s => s.Replace("\n", "")
            .Distinct()
            .Count()
        ).ToString();

        /// <inheritdoc/>
        protected override async Task<string> SolvePartTwo() => this.ParsedInput.Sum(s => s.Split('\n')
            .Select(p => p.ToCharArray())
            .Aggregate<IEnumerable<char>>((prev, next) => prev.Intersect(next))
            .Count()
        ).ToString();
    }
}
