﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Threading.Tasks;
using AdventOfCode.Grid;

namespace AdventOfCode.Y2020.Solution
{
    using ParsedType = IEnumerable<(char Direction, int Distance)>;

    public class Day12 : APuzzle<ParsedType>
    {
        private Dictionary<char, Position> vectors = new()
        {
            {'N', new (0, -1)},
            {'S', new (0, 1)},
            {'E', new (1, 0)},
            {'W', new (-1, 0)},
        };

        public Day12() : base(12, 2020, "Rain Risk")
        {
        }

        /// <summary>
        /// Parse the input string to be more useful
        /// </summary>
        protected override ParsedType ParseInput()
        {
            return this.Input.Split('\n').Select(s => (s[0], int.Parse(s[1..(s.Length)])));
        }

        /// <inheritdoc/>
        protected override async Task<string> SolvePartOne()
        {
            char[] headings = {
                'N', 'E', 'S', 'W'
            };
            int heading = 1;

            Position position = new(0, 0);

            foreach ((char direction, int distance) in this.ParsedInput) {
                switch (direction) {
                    case 'N':
                    case 'S':
                    case 'E':
                    case 'W':
                        position = position.AfterMove(direction, distance);
                        break;
                    case 'F':
                        position = position.AfterMove(headings[heading], distance);
                        break;
                    case 'L':
                        heading = (heading + (4 - distance / 90)) % 4;
                        break;
                    case 'R':
                        heading = (heading + (distance / 90)) % 4;
                        break;
                }
            }

            return position.DistanceTo((0,0)).ToString();
        }

        /// <inheritdoc/>
        protected override async Task<string> SolvePartTwo()
        {
            Position ship = new(0, 0);
            Position waypoint = new(10, -1);

            foreach ((char direction, int distance) in this.ParsedInput) {
                switch (direction) {
                    case 'N':
                    case 'S':
                    case 'E':
                    case 'W':
                        waypoint = waypoint.AfterMove(direction, distance);
                        break;
                    case 'F':
                        ship += (waypoint * distance);
                        break;
                    // Had to hardcode rotations as I was eventually getting rounding errors in my solution using correct maths
                    case 'L':
                        waypoint = distance switch
                        {
                            90 => new (+waypoint.Y, -waypoint.X),
                            180 => -waypoint,
                            270 => new (-waypoint.Y, +waypoint.X),
                            _ => waypoint,
                        };
                        break;
                    case 'R':
                        waypoint = distance switch
                        {
                            270 => new (+waypoint.Y, -waypoint.X),
                            180 => -waypoint,
                            90 => new (-waypoint.Y, +waypoint.X),
                            _ => waypoint,
                        };
                        break;
                }
            }

            return ship.DistanceTo((0,0)).ToString();
        }
    }
}
