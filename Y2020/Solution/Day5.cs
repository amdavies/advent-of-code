﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdventOfCode.Y2020.Solution
{
    using ParsedType = IEnumerable<int>;

    public class Day5 : APuzzle
    {
        /// <summary>
        /// Parsed input stream
        /// </summary>
        private ParsedType ParsedInput => this.parsedInputLoader.Value;

        /// <summary>
        /// Lazy loader for parsed input
        /// </summary>
        private readonly Lazy<ParsedType> parsedInputLoader;

        public Day5() : base(05, 2020, "Binary Boarding")
        {
            this.parsedInputLoader = new Lazy<ParsedType>(this.ParseInput);
        }

        /// <summary>
        /// Parse the input string to be more useful
        /// </summary>
        private ParsedType ParseInput() => this.Input.Split('\n').Select(pass =>
            Convert.ToInt32(
                pass.Replace('B', '1')
                    .Replace('R', '1')
                    .Replace('F', '0')
                    .Replace('L', '0'),
                2)
        );

        /// <inheritdoc/>
        protected override async Task<string> SolvePartOne()
        {
            return this.ParsedInput.Max().ToString();
        }

        /// <inheritdoc/>
        protected override async Task<string> SolvePartTwo()
        {
            foreach (var pass in this.ParsedInput) {
                if (!this.ParsedInput.Contains(pass + 1) && this.ParsedInput.Contains(pass + 2)) {
                    return (pass + 1).ToString();
                }
            }

            return "Not found";
        }
    }
}
