﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdventOfCode.Y2020.Solution
{
    using ParsedType = List<long>;

    public class Day25 : APuzzle<ParsedType>
    {
        /// <inheritdoc/>
        public Day25() : base(25, 2020, "Combo Breaker") { }

        /// <inheritdoc/>
        protected override ParsedType ParseInput()
        {
            return this.Input.Split('\n').Select(long.Parse).ToList();
        }

        /// <inheritdoc/>
        protected override async Task<string> SolvePartOne()
        {
            int loop = 0;
            long value = 1;
            while (value != this.ParsedInput[0]) {
                value = this.Calculate(value, 7);
                loop++;
            }

            value = 1;
            for (int i = 0; i < loop; i++) {
                value = this.Calculate(value, this.ParsedInput[1]);
            }

            return value.ToString();
        }

        private int CalcLoop(long key)
        {
            int loop = 0;
            long value = 1;
            while (value != key) {
                value = this.Calculate(value, 7);
                loop++;
            }

            return loop;
        }

        private long Calculate(long value, long seed) => (value * seed) % 20201227;

        /// <inheritdoc/>
        protected override async Task<string> SolvePartTwo() => "You Win!";
    }
}
