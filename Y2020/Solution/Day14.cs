﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdventOfCode.Y2020.Solution
{
    using ParsedType = IEnumerable<string>;

    public class Day14 : APuzzle
    {
        /// <summary>
        /// Parsed input stream
        /// </summary>
        private ParsedType ParsedInput => this.parsedInputLoader.Value;

        /// <summary>
        /// Lazy loader for parsed input
        /// </summary>
        private readonly Lazy<ParsedType> parsedInputLoader;

        private readonly List<char[]> floatingMap = new();

        public Day14() : base(14, 2020, "Docking Data")
        {
            this.parsedInputLoader = new Lazy<ParsedType>(this.ParseInput);

            for (int i = 0; i < 512; i++) {
                this.floatingMap.Add(Convert.ToString(i, 2).PadLeft(9, '0').Reverse().ToArray());
            }
        }

        /// <summary>
        /// Parse the input string to be more useful
        /// </summary>
        private ParsedType ParseInput()
        {
            return this.Input.Split('\n');
        }

        /// <inheritdoc/>
        protected override async Task<string> SolvePartOne()
        {
            string mask = "";
            Dictionary<int, char[]> mem = new();

            foreach (var line in this.ParsedInput) {
                var parts = line.Split(" = ");

                if (parts[0] == "mask") {
                    mask = parts[1];
                    continue;
                }

                int index = int.Parse(parts[0].Replace("mem[", "").Replace("]", ""));
                char[] bitString = Convert.ToString(int.Parse(parts[1]), 2).PadLeft(36, '0').ToCharArray();
                for (int i = 0; i < mask.Length; i++) {
                    if (mask[i] is not 'X') {
                        bitString[i] = mask[i];
                    }
                }

                mem[index] = bitString;
            }

            return mem.Sum(ca => Convert.ToInt64(string.Join("", ca.Value), 2)).ToString();
        }

        /// <inheritdoc/>
        protected override async Task<string> SolvePartTwo()
        {
            string mask = "";
            Dictionary<long, long> mem = new();

            foreach (var line in this.ParsedInput) {
                var parts = line.Split(" = ");

                if (parts[0] == "mask") {
                    mask = parts[1];
                    continue;
                }

                int index = int.Parse(parts[0].Replace("mem[", "").Replace("]", ""));
                char[] indexBitString = Convert.ToString(index, 2).PadLeft(36, '0').ToCharArray();
                for (int i = 0; i < mask.Length; i++) {
                    if (mask[i] is not '0') {
                        indexBitString[i] = mask[i];
                    }
                }

                foreach (string address in BuildAddress(string.Join("", indexBitString))) {
                    mem[Convert.ToInt64(address, 2)] = int.Parse(parts[1]);
                }
            }

            return mem.Sum(ca => ca.Value).ToString();
        }

        private IEnumerable<string> BuildAddress(string bitString)
        {
            if (!bitString.Contains('X')) {
                return new List<string>() {bitString};
            }

            List<string> addresses = new();
            IEnumerable<int> indexes = Enumerable.Range(0, bitString.Length).Where(i => bitString[i] is 'X');
            for (int i = 0; i < Math.Pow(2, indexes.Count()); i++) {
                char[] address = bitString.ToCharArray();
                int j = 0;
                foreach (int index in indexes) {
                    address[index] = this.floatingMap[i][j++];
                }

                addresses.Add(new string(address));
            }

            return addresses;
        }
    }
}
