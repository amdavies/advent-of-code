﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdventOfCode.Y2020.Solution
{
    // using ParsedType = IEnumerable<string>;


    public class Day13 : APuzzle
    {
        private record Problem(int Time, int[] BusIds);

        /// <summary>
        /// Parsed input stream
        /// </summary>
        private Problem ParsedInput => this.parsedInputLoader.Value;

        /// <summary>
        /// Lazy loader for parsed input
        /// </summary>
        private readonly Lazy<Problem> parsedInputLoader;

        public Day13() : base(13, 2020, "Shuttle Search")
        {
            this.parsedInputLoader = new Lazy<Problem>(this.ParseInput);
        }

        /// <summary>
        /// Parse the input string to be more useful
        /// </summary>
        private Problem ParseInput()
        {
            var parts = this.Input.Split('\n');

            return new Problem(
                int.Parse(parts[0]),
                parts[1].Split(',').Where(s => s is not "x").Select(int.Parse).ToArray()
            );
        }

        /// <inheritdoc/>
        protected override async Task<string> SolvePartOne()
        {
            long minWaitTime = this.ParsedInput.Time;
            int minWaitBus = 0;
            foreach (var bus in this.ParsedInput.BusIds) {
                long timeLeft = GetTimeLeft(bus, this.ParsedInput.Time);

                if (timeLeft < minWaitTime) {
                    minWaitTime = timeLeft;
                    minWaitBus = bus;
                }
            }
            return (minWaitTime * minWaitBus).ToString();
        }

        /// <inheritdoc/>
        protected override async Task<string> SolvePartTwo()
        {
            var allBusIds = this.Input.Split('\n')[1].Split(',');
            long time = 0;
            long stepSize = 1;
            for (int i = 0; i < allBusIds.Length; i++) {
                if (allBusIds[i] is "x") continue;

                int bus = int.Parse(allBusIds[i]);
                int requiredOffset = i % bus;

                int stepCount = 0;
                while (GetTimeLeft(bus, time) != requiredOffset) {
                    stepCount++;
                    time += stepSize;
                }

                // Console.WriteLine($"Found {bus} ({requiredOffset}) at {time} in {stepCount} steps, now stepping at {stepSize} * {bus} = {stepSize * bus}");
                stepSize *= bus;
            }

            return time.ToString();
        }

        private static long GetTimeLeft(int bus, long time) => (bus - time % bus) % bus;
    }
}
