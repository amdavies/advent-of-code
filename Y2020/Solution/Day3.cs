﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AdventOfCode.Grid;

namespace AdventOfCode.Y2020.Solution
{
    using ParsedType = IReadOnlyList<string>;

    public class Day3 : APuzzle
    {
        /// <summary>
        /// Parsed input stream
        /// </summary>
        private ParsedType ParsedInput => this.parsedInputLoader.Value;

        /// <summary>
        /// Lazy loader for parsed input
        /// </summary>
        private readonly Lazy<ParsedType> parsedInputLoader;

        public Day3() : base(03, 2020, "Toboggan Trajectory")
        {
            this.parsedInputLoader = new Lazy<ParsedType>(this.ParseInput);
        }

        /// <summary>
        /// Parse the input string to be more useful
        /// </summary>
        private ParsedType ParseInput() => this.Input.Split('\n').ToList();

        /// <inheritdoc/>
        protected override async Task<string> SolvePartOne() => CountTrees(3, 1).ToString();

        /// <inheritdoc/>
        protected override async Task<string> SolvePartTwo()
        {
            return new[]
            {
                CountTrees(1, 1),
                CountTrees(3, 1),
                CountTrees(5, 1),
                CountTrees(7, 1),
                CountTrees(1, 2),
            }.Aggregate(1ul, (a, b) => a * b).ToString();
        }

        private ulong CountTrees(int stepRight, int stepDown)
        {
            var map = this.ParsedInput;
            int rowSize = map[0].Length;

            Position position = new(0, 0);
            ulong trees = 0;

            while (position.Y < map.Count) {
                if (map[position.Y][position.X] == '#') {
                    trees++;
                }

                position = position with {
                    Y = position.Y + stepDown,
                    X = (position.X + stepRight) % rowSize,
                    };
            }

            return trees;
        }
    }
}
