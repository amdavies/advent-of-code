﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdventOfCode.Y2020.Solution
{
    using ParsedType = IReadOnlyList<Instruction>;

    public class Day8 : APuzzle
    {
        /// <summary>
        /// Parsed input stream
        /// </summary>
        private ParsedType ParsedInput => this.parsedInputLoader.Value;

        /// <summary>
        /// Lazy loader for parsed input
        /// </summary>
        private readonly Lazy<ParsedType> parsedInputLoader;

        public Day8() : base(08, 2020, "Handheld Halting")
        {
            this.parsedInputLoader = new Lazy<ParsedType>(this.ParseInput);
        }

        /// <summary>
        /// Parse the input string to be more useful
        /// </summary>
        private ParsedType ParseInput()
        {
            return this.Input.Split('\n').Select(l =>
            {
                var parts = l.Split(' ');

                return new Instruction(parts[0], int.Parse(parts[1]));
            }).ToArray();
        }

        /// <inheritdoc/>
        protected override async Task<string> SolvePartOne()
        {
            long accumulator = 0;
            int pointer = 0;
            HashSet<int> run = new();

            while (true) {
                if (!run.Add(pointer)) {
                    return accumulator.ToString();
                }

                var (operation, argument) = this.ParsedInput[pointer];

                switch (operation) {
                    case "nop":
                        pointer += 1;
                        break;
                    case "acc":
                        accumulator += argument;
                        pointer += 1;
                        break;
                    case "jmp":
                        pointer += argument;
                        break;
                }
            }
        }

        /// <inheritdoc/>
        protected override async Task<string> SolvePartTwo()
        {
            for (int i = 0; i < this.ParsedInput.Count; i++) {
                var instructions = this.ParsedInput.ToList();

                switch (instructions[i].Operation) {
                    case "nop":
                        instructions[i] = instructions[i] with { Operation = "jmp"};
                        break;
                    case "jmp":
                        instructions[i] = instructions[i] with { Operation = "nop" };
                        break;
                    case "acc":
                        continue;
                }

                int? result = RunProgram(instructions);
                if (result is not null) {
                    return result.ToString() ?? "Compiler is stupid and thinks something can be null here";
                }
            }

            return "No valid result found";
        }

        private int? RunProgram(IReadOnlyList<Instruction> instructions)
        {
            int accumulator = 0;
            int pointer = 0;
            HashSet<int> run = new();

            while (pointer < instructions.Count) {
                if (!run.Add(pointer)) {
                    return null;
                }

                var (operation, argument) = instructions[pointer];

                switch (operation) {
                    case "nop":
                        pointer += 1;
                        break;
                    case "acc":
                        accumulator += argument;
                        pointer += 1;
                        break;
                    case "jmp":
                        pointer += argument;
                        break;
                }
            }

            return accumulator;
        }
    }

    public record Instruction(string Operation, int Argument);
}
