﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdventOfCode.Y2020.Solution
{
    using ParsedType = IReadOnlyList<IEnumerable<int>>;

    public class Day22 : APuzzle<ParsedType>
    {
        private int GameCount = 0;

        /// <inheritdoc/>
        public Day22() : base(22, 2020, "Crab Combat")
        {
        }

        /// <inheritdoc/>
        protected override ParsedType ParseInput()
        {
            return this.Input.Split("\n\n")
                .Select(l => l.Split('\n').Skip(1).Select(int.Parse).ToList())
                .ToList();
        }

        /// <inheritdoc/>
        protected override async Task<string> SolvePartOne()
        {
            List<int> player1 = this.ParsedInput[0].ToList();
            List<int> player2 = this.ParsedInput[1].ToList();

            while (player1.Count > 0 && player2.Count > 0) {
                int[] cards =
                {
                    player1[0],
                    player2[0],
                };
                player1.RemoveAt(0);
                player2.RemoveAt(0);

                bool p1Wins = cards[0] > cards[1];
                if (p1Wins) {
                    player1 = player1.Concat(cards).ToList();
                } else {
                    player2 = player2.Concat(cards.Reverse()).ToList();
                }
            }

            var winner = player1.Count > 0 ? player1 : player2;

            int cardNumber = 1;
            winner.Reverse();
            return winner.Sum(card => card * cardNumber++).ToString();
        }

        /// <inheritdoc/>
        protected override async Task<string> SolvePartTwo()
        {
            Queue<int> player1 = new(this.ParsedInput[0]);
            Queue<int> player2 = new(this.ParsedInput[1]);

            var winner = this.PlayGame(player1, player2) ? player1 : player2;

            int cardNumber = 1;
            return winner.Reverse().Sum(card => card * cardNumber++).ToString();
        }

        private bool PlayGame(Queue<int> player1, Queue<int> player2)
        {
            this.GameCount++;
            HashSet<string> p1SeenHands = new();
            HashSet<string> p2SeenHands = new();
            while (player1.Count > 0 && player2.Count > 0) {
                string p1Key = string.Join('.', player1);
                string p2Key = string.Join('.', player2);
                if (p1SeenHands.Contains(p1Key) || p2SeenHands.Contains(p2Key)) {
                    return true;
                }
                p1SeenHands.Add(p1Key);
                p2SeenHands.Add(p2Key);

                int p1Card = player1.Dequeue();
                int p2Card = player2.Dequeue();

                bool p1Wins;
                if (player1.Count >= p1Card && player2.Count >= p2Card) {
                    p1Wins = this.PlayGame(new Queue<int>(player1.Take(p1Card)), new Queue<int>(player2.Take(p2Card)));
                } else {
                    p1Wins = p1Card > p2Card;
                }

                if (p1Wins) {
                    player1.Enqueue(p1Card);
                    player1.Enqueue(p2Card);
                } else {
                    player2.Enqueue(p2Card);
                    player2.Enqueue(p1Card);
                }
            }

            return player1.Count > 0;
        }
    }
}
