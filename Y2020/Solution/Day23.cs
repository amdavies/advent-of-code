﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdventOfCode.Y2020.Solution
{
    using ParsedType = List<int>;
    using Cup = CircularListNode<long>;

    public class Day23 : APuzzle<ParsedType>
    {
        /// <inheritdoc/>
        public Day23() : base(23, 2020, "Crab Cups") { }

        /// <inheritdoc/>
        protected override ParsedType ParseInput() => this.Input.ToCharArray().Select(c => int.Parse(c.ToString())).ToList();

        /// <inheritdoc/>
        protected override async Task<string> SolvePartOne()
        {
            List<Cup> circle = this.ParsedInput.Select(c => new Cup(c)).ToList();
            for (int i = 0; i < circle.Count; i++) {
                circle[i].Next = circle[(i + 1) % circle.Count];
            }
            Cup current = circle[0];

            for (int turn = 1; turn <= 100; turn++) {
                Cup removeStart = current.Next!;
                List<long> removedValues = new()
                {
                    removeStart.Value,
                    removeStart.Next!.Value,
                    removeStart.Next!.Next!.Value,
                };
                current.Next = current.Next!.Next!.Next!.Next!;

                long destinationVal = current.Value == 1? 9 : current.Value - 1;
                while (removedValues.Contains(destinationVal)) {
                    destinationVal = destinationVal == 1 ? 9 : destinationVal - 1;
                }

                Cup insertPoint = circle.Find(c => c.Value == destinationVal)!;
                removeStart.Next!.Next!.Next = insertPoint.Next;
                insertPoint.Next = removeStart;
                current = current.Next;
            }

            string output = "";
            current = circle.Find(c => c.Value == 1)!.Next!;
            while (current.Value != 1) {
                output += current.Value;
                current = current.Next!;
            }
            return output;
        }

        protected override async Task<string> SolvePartTwo()
        {
            long[] pointer = new long[1000001];// Only care about 1million values, but index == 0 is a throwaway
            List<long> circle = this.ParsedInput.Select(c => (long) c).ToList();
            long start = circle.First();
            pointer[1000000] = start; // Millionth record always points to the start
            pointer[circle.Last()] = 10; // Last of the defined cups will always point to 10

            // Fill the defined cups to point to next in the list
            for (int i = 0; i < circle.Count - 1; i++) {
                pointer[circle[i]] = circle[i + 1];
            }
            // Everything else just points to the next value
            for (long i = 10; i < 1000000; i++) {
                pointer[i] = i + 1;
            }

            long current = start;
            for (int turn = 1; turn <= 10000000; turn++) {
                long next = pointer[current];
                long mid = pointer[next];
                long last = pointer[mid];

                long destinationVal = current;
                do {
                    destinationVal = destinationVal == 1 ? 1000000 : destinationVal - 1;
                } while (destinationVal == next || destinationVal == mid || destinationVal == last);

                pointer[current] = pointer[last];
                pointer[last] = pointer[destinationVal];
                pointer[destinationVal] = next;
                current = pointer[current];
            }

            return (pointer[1] * pointer[pointer[1]]).ToString();
        }
    }
}
