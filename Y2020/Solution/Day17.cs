﻿using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Threading.Tasks;

namespace AdventOfCode.Y2020.Solution
{
    using ParsedType = Dictionary<Vector4, bool>;

    public class Day17 : APuzzle<ParsedType>
    {
        public Day17() : base(17, 2020, "Conway Cubes") { }

        /// <summary>
        /// Parse the input string to be more useful
        /// </summary>
        protected override ParsedType ParseInput()
        {
            Dictionary<Vector4, bool> grid = new();
            var lines = this.Input.Split('\n');
            for (int y = 0; y < lines.Length; y++) {
                for (int x = 0; x < lines[y].Length; x++) {
                    grid.Add(new Vector4(x, y, 0, 0), lines[y][x] == '#');
                }
            }

            return grid;
        }

        /// <inheritdoc/>
        protected override async Task<string> SolvePartOne()
        {
            var state = this.ParsedInput;

            for (int cycle = 0; cycle < 6; cycle++) {
                Dictionary<Vector4, bool> nextState = new();
                int maxX = (int) state.Where(c => c.Value).Max(c => c.Key.X);
                int maxY = (int) state.Where(c => c.Value).Max(c => c.Key.Y);
                int maxZ = (int) state.Where(c => c.Value).Max(c => c.Key.Z);
                int minX = (int) state.Where(c => c.Value).Min(c => c.Key.X);
                int minY = (int) state.Where(c => c.Value).Min(c => c.Key.Y);
                int minZ = (int) state.Where(c => c.Value).Min(c => c.Key.Z);

                for (int z = minZ - 1; z <= maxZ + 1; z++) {
                    for (int y = minY - 1; y <= maxY + 1; y++) {
                        for (int x = minX - 1; x <= maxX + 1; x++) {
                            var pos = new Vector4(x, y, z, 0);
                            state.TryGetValue(pos, out var isActive);
                            int neighbours = CountNeighbours(state, pos, false);

                            nextState[pos] = (isActive && neighbours is 2) || neighbours is 3;
                        }
                    }
                }

                // Dump(nextState);
                state = nextState;
            }

            return state.Count(c => c.Value).ToString();
        }

        /// <inheritdoc/>
        protected override async Task<string> SolvePartTwo()
        {
            var state = this.ParsedInput;

            for (int cycle = 0; cycle < 6; cycle++) {
                Dictionary<Vector4, bool> nextState = new();
                int maxX = (int) state.Where(c => c.Value).Max(c => c.Key.X);
                int maxY = (int) state.Where(c => c.Value).Max(c => c.Key.Y);
                int maxZ = (int) state.Where(c => c.Value).Max(c => c.Key.Z);
                int maxW = (int) state.Where(c => c.Value).Max(c => c.Key.W);
                int minX = (int) state.Where(c => c.Value).Min(c => c.Key.X);
                int minY = (int) state.Where(c => c.Value).Min(c => c.Key.Y);
                int minZ = (int) state.Where(c => c.Value).Min(c => c.Key.Z);
                int minW = (int) state.Where(c => c.Value).Min(c => c.Key.W);

                for (int w = minW - 1; w <= maxW + 1; w++) {
                    for (int z = minZ - 1; z <= maxZ + 1; z++) {
                        for (int y = minY - 1; y <= maxY + 1; y++) {
                            for (int x = minX - 1; x <= maxX + 1; x++) {
                                var pos = new Vector4(x, y, z, w);
                                state.TryGetValue(pos, out var isActive);
                                int neighbours = CountNeighbours(state, pos, true);

                                nextState[pos] = (isActive && neighbours is 2) || neighbours is 3;
                            }
                        }
                    }
                }

                state = nextState;
            }

            return state.Count(c => c.Value).ToString();
        }

        private static int CountNeighbours(Dictionary<Vector4, bool> state, Vector4 position, bool useW)
        {
            int count = 0;
            for (int x = -1; x <= 1; x++) {
                for (int y = -1; y <= 1; y++) {
                    for (int z = -1; z <= 1; z++) {
                        for (int w = useW ? -1 : 0; w <= (useW ? 1 : 0); w++) {
                            if (x is 0 && y is 0 && z is 0 && w is 0) {
                                continue;
                            }

                            var offset = new Vector4(x, y, z, w);

                            state.TryGetValue(position + offset, out var isActive);
                            count += isActive ? 1 : 0;
                        }
                    }
                }
            }

            return count;
        }
    }
}
