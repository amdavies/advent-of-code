﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdventOfCode.Y2020.Solution
{
    using ParsedType = List<int>;

    public class Day15 : APuzzle<ParsedType>
    {
        public Day15() : base(15, 2020, "Rambunctious Recitation")
        {
        }

        protected override ParsedType ParseInput()
        {
            return this.Input.Split(',').Select(int.Parse).ToList();
        }

        /// <inheritdoc/>
        protected override async Task<string> SolvePartOne() => this.RunTurns(2020).ToString();

        /// <inheritdoc/>
        protected override async Task<string> SolvePartTwo() => this.RunTurns(30000000).ToString();

        private int RunTurns(int turnCount)
        {
            int spoken = this.ParsedInput[0];
            int[] last = new int[turnCount];
            int turn = 1;
            foreach (int startingNumber in this.ParsedInput.Skip(1)) {
                last[spoken] = turn++;
                spoken = startingNumber;
            }

            for (; turn < turnCount; turn++) {
                int lastVal = last[spoken];
                int nextNumber = lastVal is 0 ? 0 : turn - lastVal;
                last[spoken] = turn;
                spoken = nextNumber;
            }

            return spoken;
        }
    }
}
