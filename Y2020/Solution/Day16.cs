﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdventOfCode.Y2020.Solution
{
    public class Day16 : APuzzle
    {
        /// <summary>
        /// Parsed input stream
        /// </summary>
        private TicketProblem ParsedInput => this.parsedInputLoader.Value;

        private List<List<long>> ValidTickets = new();

        private record Rule(string Name, (int l, int h) Lower, (int l, int h) Higher);

        private record TicketProblem(List<Rule> Rules, List<long> MyTicket, List<List<long>> OtherTickets);

        /// <summary>
        /// Lazy loader for parsed input
        /// </summary>
        private readonly Lazy<TicketProblem> parsedInputLoader;

        public Day16() : base(16, 2020, "Ticket Translation")
        {
            this.parsedInputLoader = new Lazy<TicketProblem>(this.ParseInput);
        }

        /// <summary>
        /// Parse the input string to be more useful
        /// </summary>
        private TicketProblem ParseInput()
        {
            var problemParts = this.Input.Split("\n\n");
            List<Rule> rules = problemParts[0].Split('\n').ToList().Select(l =>
            {
                var labelSplit = l.Split(": ");
                var rangeSplit = labelSplit[1].Split(" or ");
                return new Rule(
                    labelSplit[0],
                    (int.Parse(rangeSplit[0].Split('-')[0]), int.Parse(rangeSplit[0].Split('-')[1])),
                    (int.Parse(rangeSplit[1].Split('-')[0]), int.Parse(rangeSplit[1].Split('-')[1]))
                );
            }).ToList();

            return new(
                rules,
                problemParts[1].Split('\n')[1].Split(',').Select(long.Parse).ToList(),
                problemParts[2].Split('\n')[1..].Select(l => l.Split(',').Select(long.Parse).ToList()).ToList()
            );
        }

        /// <inheritdoc/>
        protected override async Task<string> SolvePartOne()
        {
            long errorRate = 0;

            foreach (var ticket in this.ParsedInput.OtherTickets) {
                this.ValidTickets.Add(ticket);
                foreach (var field in ticket) {
                    bool isValid = this.ParsedInput.Rules.Any(rule => (field >= rule.Lower.l && field <= rule.Lower.h) || (field >= rule.Higher.l && field <= rule.Higher.h));

                    if (!isValid) {
                        errorRate += field;
                        this.ValidTickets.Remove(ticket);
                    }
                }
            }

            return errorRate.ToString();
        }

        /// <inheritdoc/>
        protected override async Task<string> SolvePartTwo()
        {
            if (this.Part1 == "") {
                throw new Exception("Need to run Part 1 first");
            }

            Dictionary<int, Rule> ruleForColumn = new();
            var unknownRules = this.ParsedInput.Rules.ToList();
            var unknownColumns = Enumerable.Range(0, this.ParsedInput.Rules.Count).ToList();
            while (unknownColumns.Any()) {
                List<int> foundCols = new();
                foreach (var col in unknownColumns) {
                    List<Rule> columnRules = unknownRules.Where(rule => this.ValidTickets.All(ticket => ticket[col] >= rule.Lower.l && ticket[col] <= rule.Lower.h || ticket[col] >= rule.Higher.l && ticket[col] <= rule.Higher.h)).ToList();

                    if (columnRules.Count > 1) {
                        continue;
                    }

                    foundCols.Add(col);
                    ruleForColumn[col] = columnRules.First();
                    unknownRules.Remove(ruleForColumn[col]);
                }

                foundCols.ForEach(col => unknownColumns.Remove(col));
            }

            return ruleForColumn.Where(r => r.Value.Name.StartsWith("departure"))
                .Select(r => this.ParsedInput.MyTicket[r.Key])
                .Aggregate(1L, (a, b) => a * b)
                .ToString();
        }
    }
}
