﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdventOfCode.Y2020.Solution
{
    using ParsedType = List<long>;

    public class Day9 : APuzzle
    {
        private int preLength = 25;

        /// <summary>
        /// Parsed input stream
        /// </summary>
        private ParsedType ParsedInput => this.parsedInputLoader.Value;

        /// <summary>
        /// Lazy loader for parsed input
        /// </summary>
        private readonly Lazy<ParsedType> parsedInputLoader;

        public Day9() : base(09, 2020, "Encoding Error")
        {
            this.parsedInputLoader = new Lazy<ParsedType>(this.ParseInput);
        }

        /// <summary>
        /// Parse the input string to be more useful
        /// </summary>
        private ParsedType ParseInput()
        {
            return this.Input.Split('\n').Select(long.Parse).ToList();
        }

        /// <inheritdoc/>
        protected override async Task<string> SolvePartOne()
        {
            long[] data = this.ParsedInput.ToArray();
            for (int i = this.preLength; i < this.ParsedInput.Count; i++) {
                if (!this.isNumberValid(i, data)) {
                    return this.ParsedInput[i].ToString();
                }
            }

            return "Failed: No invalid match found";
        }

        private bool isNumberValid(int index, long[] data)
        {
            int start = index - this.preLength;
            foreach (long j in data[start..index]) {
                foreach (long k in data[start..index]) {
                    if (j != k && j + k == data[index]) {
                        return true;
                    }
                }
            }

            return false;
        }

        /// <inheritdoc/>
        protected override async Task<string> SolvePartTwo()
        {
            long targetNumber = long.Parse(this.Part1);
            long[] data = this.ParsedInput.ToArray();

            long current = 0;
            int back = 0;
            for (var front = 0; front < data.Length; front++) {
                // Extend our range forwards
                current += data[front];

                // If we're too high, shrink the back of the range
                while (current > targetNumber) {
                    current -= data[back++];
                }

                if (current != targetNumber || front <= back) {
                    continue;
                }

                var range = data[back..(front+1)];
                return (range.Min() + range.Max()).ToString();
            }

            return "Failure: Nothing found";
        }
    }
}
