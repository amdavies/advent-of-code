﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdventOfCode.Y2020.Solution
{
    using ParsedType = IEnumerable<PolicyPassword>;

    public class Day2 : APuzzle
    {
        /// <summary>
        /// Parsed input stream
        /// </summary>
        private ParsedType ParsedInput => this.parsedInputLoader.Value;

        /// <summary>
        /// Lazy loader for parsed input
        /// </summary>
        private readonly Lazy<ParsedType> parsedInputLoader;

        public Day2() : base(02, 2020, "Password Philosophy")
        {
            this.parsedInputLoader = new Lazy<ParsedType>(this.ParseInput);
        }

        /// <summary>
        /// Parse the input string to be more useful
        /// </summary>
        private ParsedType ParseInput()
        {
            return this.Input.Split('\n').Select(l => {
                var parts = l.Split(' ');

                return new PolicyPassword(
                    int.Parse(parts[0].Split('-')[0]),
                    int.Parse(parts[0].Split('-')[1]),
                    parts[1][0],
                    parts[2]
                );
            });
        }

        /// <inheritdoc/>
        protected override async Task<string> SolvePartOne()
        {
            return this.ParsedInput.Count(p =>
            {
                var (min, max, letter, password) = p;
                int charCount = password.Count(c => c == letter);

                return charCount >= min && charCount <= max;
            }).ToString();
        }

        /// <inheritdoc/>
        protected override async Task<string> SolvePartTwo()
        {
            return this.ParsedInput.Count(p => p.Password[p.Min - 1] == p.Letter ^ p.Password[p.Max - 1] == p.Letter).ToString();
        }
    }
}
