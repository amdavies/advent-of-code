﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace AdventOfCode.Y2020.Solution
{
    public record Problem19(Dictionary<int, string> Rules, List<string> Messages);

    public class Day19 : APuzzle<Problem19>
    {
        /// <inheritdoc/>
        public Day19() : base(19, 2020, "Monster Messages") { }

        /// <summary>
        /// Parse the input string to be more useful
        /// </summary>
        protected override Problem19 ParseInput()
        {
            var parts = this.Input.Split("\n\n");

            return new Problem19(
                parts[0].Split('\n').ToDictionary(
                    r => int.Parse(r.Split(':')[0]),
                    r => r.Split(": ")[1]
                ),
                parts[1].Split('\n').ToList()
            );
        }

        /// <inheritdoc/>
        protected override async Task<string> SolvePartOne()
        {
            Regex ruleMatcher = new(@"(?:^|\s*|\()(\d+)(?:\s*|$|\))");
            var baseRule = this.ParsedInput.Rules[0];
            while (baseRule.Any(char.IsDigit)) {
                var matches = ruleMatcher.Matches(baseRule);


                foreach (Match match in matches.Reverse()) {
                    var ruleNo = match.Groups[1].Value;
                    var index = match.Groups[1].Index;
                    baseRule = baseRule.Remove(index, ruleNo.Length).Insert(index, "(" + this.ParsedInput.Rules[int.Parse(ruleNo)] + ")");
                }
            }

            Regex messageMatcher = new("^" + baseRule.Replace("\"", "").Replace(" ", "") + "$");

            return this.ParsedInput.Messages.Count(s => messageMatcher.Match(s).Success).ToString();
        }

        /// <inheritdoc/>
        protected override async Task<string> SolvePartTwo()
        {
            Regex ruleMatcher = new(@"(?:^|\s*|\()(\d+)(?:\s*|$|\))");
            var baseRule = this.ParsedInput.Rules[0];
            this.ParsedInput.Rules[8] = "42 | 42 8";
            this.ParsedInput.Rules[11] = "42 31 | 42 11 31";
            int count8 = 0;
            int count11 = 0;
            while (baseRule.Any(char.IsDigit)) {
                var matches = ruleMatcher.Matches(baseRule);


                foreach (Match match in matches.Reverse()) {
                    var ruleNo = match.Groups[1].Value;
                    var index = match.Groups[1].Index;

                    // 8 and 11 now infinitely loop, tune limits until answer is correct quickly
                    switch (ruleNo) {
                        case "8" when count8++ > 4:
                        case "11" when count11++ > 3: {
                            baseRule = baseRule.Remove(index, ruleNo.Length);
                            continue;
                        }
                    }

                    baseRule = baseRule.Remove(index, ruleNo.Length).Insert(index, "(" + this.ParsedInput.Rules[int.Parse(ruleNo)] + ")");
                }
            }

            Regex messageMatcher = new("^" + baseRule.Replace("\"", "").Replace(" ", "") + "$");

            return this.ParsedInput.Messages.Count(s => messageMatcher.Match(s).Success).ToString();
        }
    }
}
