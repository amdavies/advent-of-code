﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;

namespace AdventOfCode.Y2020.Solution
{
    /// <summary>
    /// --- Day 1: Report Repair ---
    /// ...
    /// Before you leave, the Elves in accounting just need you to fix your expense report (your puzzle input); apparently, something isn't quite adding up.
    ///
    /// Specifically, they need you to find the two entries that sum to 2020 and then multiply those two numbers together.
    /// </summary>
    public class Day1 : APuzzle
    {
        /// <summary>
        /// Parsed input stream
        /// </summary>
        private int[] ParsedInput => this.parsedInputLoader.Value;

        /// <summary>
        /// Lazy loader for parsed input
        /// </summary>
        private readonly Lazy<int[]> parsedInputLoader;

        public Day1(string inputString) : this()
        {
            this.TestInput = inputString;
        }

        public Day1() : base(1, 2020, "Report Repair")
        {
            this.parsedInputLoader = new Lazy<int[]>(this.ParseInput);
        }

        /// <summary>
        ///     Parse the input string to be more useful
        /// </summary>
        private int[] ParseInput()
        {
            var values = this.Input.Split('\n').Select(int.Parse).ToArray();
            Array.Sort(values);

            return values;
        }

        /// <inheritdoc/>
        protected override async Task<string> SolvePartOne()
        {
            var vals = this.ParsedInput;
            for (int a = 0; a < vals.Length; a++) {
                if (vals.Contains(2020 - vals[a])) {
                    return (vals[a] * (2020 - vals[a])).ToString();
                }
            }

            return "Failure: Didn't find a pair";
        }

        /// <inheritdoc/>
        protected override async Task<string> SolvePartTwo()
        {
            var vals = this.ParsedInput;
            for (int a = 0; a < vals.Length; a++) {
                // My input solution uses the smallest value in the set, making optimisation a bit more difficult
                for (int b = vals.Length - 1; b > a; b--) {
                    int t = vals[a] + vals[b];
                    if (t < 2020 && vals.Contains(2020 - t)) {
                        return (vals[a] * vals[b] * (2020 - t)).ToString();
                    }
                }
            }

            return "Failure: Didn't find a set";
        }
    }
}
