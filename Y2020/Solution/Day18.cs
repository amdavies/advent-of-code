﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace AdventOfCode.Y2020.Solution
{
    using ParsedType = IEnumerable<string>;

    public class Day18 : APuzzle<ParsedType>
    {
        private readonly Regex sumRegex = new(@"(\d+)\s*([\+\*])\s*(\d+)");
        private readonly Regex plusRegex = new(@"(\d+)\s*([\+])\s*(\d+)");
        private readonly Regex timesRegex = new(@"(\d+)\s*([\*])\s*(\d+)");

        /// <inheritdoc/>
        public Day18() : base(18, 2020, "Operation Order") { }

        /// <inheritdoc/>
        protected override ParsedType ParseInput() => this.Input.Split('\n');

        /// <inheritdoc/>
        protected override async Task<string> SolvePartOne() => this.ParsedInput.Sum(formula => long.Parse(this.Calculate(formula))).ToString();

        /// <inheritdoc/>
        protected override async Task<string> SolvePartTwo() => this.ParsedInput.Sum(formula => long.Parse(this.Calculate(formula, true))).ToString();

        private string Calculate(string formula, bool precedence = false)
        {
            // Console.WriteLine($"Processing: {formula}");
            formula = ProcessParenthesis(formula, precedence);
            // Console.WriteLine($"Calculating: {formula}");

            while (formula.Contains('+') || formula.Contains('*')) {
                Regex reg;
                Match sums;

                if (precedence) {
                    sums = this.plusRegex.Match(formula);
                    reg = this.plusRegex;
                    if (!sums.Success) {
                        sums = this.timesRegex.Match(formula);
                        reg = this.timesRegex;
                    }
                } else {
                    sums = this.sumRegex.Match(formula);
                    reg = this.sumRegex;
                }

                long sum = sums.Groups[2].Value switch
                {
                    "+" => long.Parse(sums.Groups[1].Value) + long.Parse(sums.Groups[3].Value),
                    "*" => long.Parse(sums.Groups[1].Value) * long.Parse(sums.Groups[3].Value),
                    _ => throw new Exception("What is this sum?"),
                };

                formula = reg.Replace(formula, sum.ToString(), 1);
            }

            return formula;
        }

        private string ProcessParenthesis(string formula, bool precedence)
        {
            while (formula.Contains('(')) {
                int pStart = formula.IndexOf('(');
                int length = 0;
                int depth = 1;
                for (int i = pStart+1; i < formula.Length; i++) {
                    if (formula[i] is '(') depth++;
                    if (formula[i] is ')') depth--;
                    if (depth != 0) continue;
                    length = i - pStart + 1;
                    break;
                }

                if (length is 0) throw new Exception("Where did the end go?");

                string calculated = this.Calculate(formula.Substring(pStart + 1, length - 2), precedence);
                formula = formula.Replace(formula.Substring(pStart, length), calculated);
            }

            return formula;
        }
    }
}
