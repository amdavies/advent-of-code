﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdventOfCode.Y2020.Solution
{
    using ParsedType = IReadOnlyList<uint>;

    public class Day10 : APuzzle
    {
        /// <summary>
        /// Parsed input stream
        /// </summary>
        private ParsedType ParsedInput => this.parsedInputLoader.Value;

        /// <summary>
        /// Lazy loader for parsed input
        /// </summary>
        private readonly Lazy<ParsedType> parsedInputLoader;

        public Day10() : base(10, 2020, "Adapter Array")
        {
            this.parsedInputLoader = new Lazy<ParsedType>(this.ParseInput);
        }

        /// <summary>
        /// Parse the input string to be more useful
        /// </summary>
        private ParsedType ParseInput()
        {
            var adapters = this.Input.Split('\n').Select(uint.Parse).ToList();
            adapters.Add(0);
            adapters.Add(adapters.Max() + 3);
            adapters.Sort();

            return adapters;
        }

        /// <inheritdoc/>
        protected override async Task<string> SolvePartOne()
        {
            var one = 0;
            var three = 0;
            for (int i = 0; i < this.ParsedInput.Count - 1; i++) {
                var diff = this.ParsedInput[i + 1] - this.ParsedInput[i];
                if (diff == 1) one++;
                if (diff == 3) three++;
            }
            return (one * three).ToString();
        }

        /// <inheritdoc/>
        protected override async Task<string> SolvePartTwo()
        {
            Dictionary<int, ulong> connections = new(){{this.ParsedInput.Count - 1, 1}};

            for (int i = this.ParsedInput.Count - 2; i >= 0; i--) {
                ulong validConnections = 0;
                uint currentJoltage = this.ParsedInput[i];
                for (int connection = i + 1; connection < this.ParsedInput.Count && this.ParsedInput[connection] - currentJoltage <= 3; connection++) {
                    validConnections += connections[connection];
                }

                connections[i] = validConnections;
            }

            return connections[0].ToString();
        }
    }
}
