﻿namespace AdventOfCode.Y2020
{
    public record PolicyPassword(int Min, int Max, char Letter, string Password);
}
