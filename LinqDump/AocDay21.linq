<Query Kind="Statements">
  <Reference Relative="..\..\..\RiderProjects\advent-of-code\Core\obj\Release\net7.0\AdventOfCode.Core.dll">C:\Users\lex\RiderProjects\advent-of-code\Core\obj\Release\net7.0\AdventOfCode.Core.dll</Reference>
  <Reference Relative="..\..\..\RiderProjects\advent-of-code\Core\Core.csproj">C:\Users\lex\RiderProjects\advent-of-code\Core\Core.csproj</Reference>
  <Namespace>AdventOfCode</Namespace>
</Query>

// Day 21

var input = Puzzle.GetInput(sample: false)
	.Split("\n")
    .Select(l => l.Split(": "))
    .ToDictionary(l => l[0], l => l[1]);

long GetNumber(string instruction) {
    if (long.TryParse(instruction, out var val)) {
        return val;
    }
    
    if (instruction.Contains(" + ")) {
        var parts = instruction.Split(" + ");
        return GetNumber(input[parts[0]]) + GetNumber(input[parts[1]]);
    }

    if (instruction.Contains(" - "))
    {
        var parts = instruction.Split(" - ");
        return GetNumber(input[parts[0]]) - GetNumber(input[parts[1]]);
    }

    if (instruction.Contains(" * "))
    {
        var parts = instruction.Split(" * ");
        return GetNumber(input[parts[0]]) * GetNumber(input[parts[1]]);
    }

    if (instruction.Contains(" / "))
    {
        var parts = instruction.Split(" / ");
        return GetNumber(input[parts[0]]) / GetNumber(input[parts[1]]);
    }
    
    throw new Exception($"Wtf is this input? {instruction}");
};

GetNumber(input["root"]).Dump("Part 1");

input["root"] = input["root"].Replace(" + ", " - ");

bool increasing = true;
bool quickSearch = true;
long stepSize = 100;
long last = 1;

while (GetNumber(input["root"]) != 0) {
    var difference = GetNumber(input["root"]);
    
    if (Math.Sign(last) != Math.Sign(difference)) {
        "Flipped".Dump();
        stepSize /= 2;
        increasing = !increasing;
        quickSearch = false;
    }
    
    if (increasing) input["humn"] = (long.Parse(input["humn"]) + stepSize).ToString();
    else input["humn"] = (long.Parse(input["humn"]) - stepSize).ToString();
    
    difference.Dump();
    
    if (quickSearch) stepSize *= 2;
    
    last = difference;
}
input["humn"].Dump("Part 2");
