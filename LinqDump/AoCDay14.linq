<Query Kind="Statements">
  <Reference Relative="..\..\..\RiderProjects\advent-of-code\Core\obj\Release\net7.0\AdventOfCode.Core.dll">C:\Users\lex\RiderProjects\advent-of-code\Core\obj\Release\net7.0\AdventOfCode.Core.dll</Reference>
  <Reference Relative="..\..\..\RiderProjects\advent-of-code\Core\Core.csproj">C:\Users\lex\RiderProjects\advent-of-code\Core\Core.csproj</Reference>
  <Namespace>AdventOfCode</Namespace>
  <Namespace>AdventOfCode.Grid</Namespace>
  <Namespace>AdventOfCode.Enumeration</Namespace>
</Query>

// Day 14

var input = Puzzle.GetInput(sample: false)
	.Split("\n");

// Build Map
Dictionary<Position, bool> map = new();
foreach (var line in input) {
	var points = line.Split(" -> ").Select(pt => pt.Split(",").ToPosition());
	
	foreach ((int index, Position[] pair) in points.RollingSlice(2, 1)) {
		(int, int) delta;
		if (pair[0].X != pair[1].X) {
			delta = pair[0].X > pair[1].X ? (-1, 0) : (1, 0);
		}else{
			delta = pair[0].Y > pair[1].Y ? (0, -1) : (0, 1);
		}
		
		Position pos = pair[0];
		do {
			map[pos] = true;
			pos = pos + delta;
			map[pos] = true;
		} while (pos != pair[1]);
	}
}

// Drop sand until we fall off
var options = new[]{
	(0, 1),
	(-1, 1),
	(1, 1)
};
var maxY = map.Select(s => s.Key.Y).Max(s => s);
maxY.Dump();
var sandCount = 0;
while (true)
{
	Position sand = new(500, 0);

	while (sand.Y <= maxY) { 
		if (!map.ContainsKey(sand + (0, 1))) {
			sand = sand + (0, 1);
			continue;
		}
		if (!map.ContainsKey(sand + (-1, 1)))
		{
			sand = sand + (-1, 1);
			continue;
		}
		if (!map.ContainsKey(sand + (1, 1)))
		{
			sand = sand + (1, 1);
			continue;
		}
		
		// Didn't moved, add to map and start a new piece
		map[sand] = true;
		break;
	}
	
	if (sand.Y > maxY) {
		break;
	}
	
	sandCount++;
}

sandCount.Dump("Part 1");


while (true)
{
	Position sand = new(500, 0);

	while (sand.Y <= maxY + 1)
	{
		var newPos = sand + (0, 1);
		if (!map.ContainsKey(newPos) && newPos.Y <= maxY+1)
		{
			sand = newPos;
			continue;
		}
		
		newPos = sand + (-1, 1);
		if (!map.ContainsKey(newPos) && newPos.Y <= maxY+1)
		{
			sand = newPos;
			continue;
		}
		
		newPos = sand + (1, 1);
		if (!map.ContainsKey(newPos) && newPos.Y <= maxY+1)
		{
			sand = newPos;
			continue;
		}

		// Didn't move, add to map and start a new piece
		map[sand] = true;
		break;
	}

	sandCount++;
	if (sand == new Position(500,0))
	{
		sand.Dump();
		break;
	}
}

sandCount.Dump("Part 1");

