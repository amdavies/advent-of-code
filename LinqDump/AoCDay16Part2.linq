<Query Kind="Statements">
  <Reference Relative="..\..\..\RiderProjects\advent-of-code\Core\obj\Release\net7.0\AdventOfCode.Core.dll">C:\Users\lex\RiderProjects\advent-of-code\Core\obj\Release\net7.0\AdventOfCode.Core.dll</Reference>
  <Reference Relative="..\..\..\RiderProjects\advent-of-code\Core\Core.csproj">C:\Users\lex\RiderProjects\advent-of-code\Core\Core.csproj</Reference>
  <Namespace>AdventOfCode</Namespace>
</Query>

// Day 16

var input = Puzzle.GetInput(sample: false)
	.Split("\n");
	
var valves = new Dictionary<string, (int flow, List<string> doors)>();

foreach (var line in input) {
	var parts = line.Replace("Valve ", "").Replace(" has flow rate=", ":").Replace("; tunnel leads to valve ", ":").Replace("; tunnels lead to valves ", ":").Split(":");
	
	valves[parts[0]] = (int.Parse(parts[1]), parts[2].Split(", ").ToList());
}

int valveCount = valves.Count(r => r.Value.flow > 0);
valveCount.Dump();

var paths = new Queue<(int eventualFlow, string opened, string current, string last)>();
paths.Enqueue((0, "", "AA", "AA"));

long steps = 0;
Dictionary<string, int> bestWithValves = new();
for (int minute = 4; minute < 30; minute++) {
	var optionCount = paths.Count;
	for (int i = 0; i < optionCount; i++) {
		var path = paths.Dequeue();
        
        steps++;

        bool moved = false;
		foreach (var option in valves[path.current].doors)
		{
            if (path.last == option) {
                // We just came from here, and did nothing!
                continue;
            }
            moved = true;
			paths.Enqueue(path with { current = option, last = path.current });
		}
		if (!path.opened.Contains(path.current)) {
			// Check to open a valve, if not move
			var room = valves[path.current];
            var currentOpened = path.opened.Split(":").Where(s => !string.IsNullOrEmpty(s)).ToHashSet();
			if (room.flow > 0 && !currentOpened.Contains(path.current)) {
				var opened = currentOpened.Append(path.current).Order().Implode(":");
				var newPath = path with {
					opened = opened,
					eventualFlow = path.eventualFlow + room.flow * (29 - minute),
                    last = path.current
				};
                moved = true;
				paths.Enqueue(newPath);
			}
		}
        
        if (!moved || path.opened.Split(":").Count() == valveCount) {
            // We've got nothing to do but wait
            bestWithValves[path.opened] = int.Max(bestWithValves.GetValueOrDefault(path.opened, 0), path.eventualFlow);
        }
	}
	
	paths = new (paths.Distinct());
}

steps.Dump();

foreach (var path in paths) {
    bestWithValves[path.opened] = int.Max(bestWithValves.GetValueOrDefault(path.opened, 0), path.eventualFlow);
}

int bestPairing = 0;
foreach (var path1 in bestWithValves)
{
    foreach (var path2 in bestWithValves)
    {
        if (path1.Key.Split(":").Intersect(path2.Key.Split(":")).Any()) {
            continue;
        }

        bestPairing = int.Max(bestPairing, path1.Value + path2.Value);
    }
}
bestPairing.Dump();
