<Query Kind="Statements">
  <Reference Relative="..\..\..\RiderProjects\advent-of-code\Core\obj\Release\net7.0\AdventOfCode.Core.dll">C:\Users\lex\RiderProjects\advent-of-code\Core\obj\Release\net7.0\AdventOfCode.Core.dll</Reference>
  <Reference Relative="..\..\..\RiderProjects\advent-of-code\Core\Core.csproj">C:\Users\lex\RiderProjects\advent-of-code\Core\Core.csproj</Reference>
  <Namespace>AdventOfCode</Namespace>
</Query>

// Day 25

var input = Puzzle.GetInput(sample: false)
	.Split("\n");
	
long sum = 0;
foreach (var snafu in input) {
    long num = 0;
    int sig = 0;
    foreach (char digit in snafu.Reverse()) {
        long digitBase = (long)Math.Pow(5, sig++);
        
        long value = digit switch {
            '=' => digitBase * -2,
            '-' => digitBase * -1,
            '0' => 0,
            '1' => digitBase * 1,
            '2' => digitBase * 2,
        };
        num += value;
    }
    
    sum += num;
}

sum.Dump();

// Convert to SNAFU
string sumSnafu = "";
while (sum != 0) {
    long remain = sum % 5;
    sum /= 5;
    
    // Outside of writeable range, add one back and setup for a negative value instead
    if (remain > 2) {
        sum++;
        remain -= 5;
    }

    sumSnafu += remain switch
    {
        -2 => '=',
        -1 => '-',
        0 => '0',
        1 => '1',
        2 => '2',
    };
}
sumSnafu.Reverse().Implode().Dump("Part 2");
