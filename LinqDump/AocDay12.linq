<Query Kind="Statements">
  <Reference Relative="..\..\..\RiderProjects\advent-of-code\Core\obj\Release\net7.0\AdventOfCode.Core.dll">C:\Users\lex\RiderProjects\advent-of-code\Core\obj\Release\net7.0\AdventOfCode.Core.dll</Reference>
  <Reference Relative="..\..\..\RiderProjects\advent-of-code\Core\Core.csproj">C:\Users\lex\RiderProjects\advent-of-code\Core\Core.csproj</Reference>
  <Namespace>AdventOfCode</Namespace>
  <Namespace>AdventOfCode.Grid</Namespace>
</Query>

// Day 12

var moves = new[] {
	new Position(1, 0),
	new Position(-1, 0),
	new Position(0, 1),
	new Position(0, -1),
};


var input = Puzzle.GetInput(sample: false)
	.Split("\n");

Dictionary<Position, int> map = new();
Position Start = new();
Position End = new();

var y = 0;
foreach (var line in input) {
	var x = 0;
	
	foreach (var node in line.ToCharArray()) {
		int height = node - 'a';
		if (node == 'S') {
			height = 0;
			Start = new(x, y);
		}
		if (node == 'E') {
			height = 25;
			End = new(x, y);
		}
		map[new(x,y)] = height;
		x++;
	}
	
	y++;
}

map[End + (1, 0)].Dump();

var maxHeightReached = 0;
var seen = new HashSet<Position> { Start };
var options = new Heap<(Position pos, int steps)>((point, point1) => point.steps - point1.steps);
options.Insert((Start, 0));
while (options.Count > 0)
{
	var option = options.Pop();
	var point = option.pos;

	// Finish if we're at the end
	if (point == End)
	{
		option.steps.Dump("Part 1");
		break;
	};

	// Add available steps to the heap
	foreach (var move in moves)
	{
		var next = point + move;

		if (!seen.Contains(next) && map.ContainsKey(next) && (map[next] - map[point]) <= 1)
		{
			options.Insert((next, option.steps + 1));
			seen.Add(next);
		}
	}
}

seen = new HashSet<Position> { End };
options.Clear();
options.Insert((End, 0));
while (options.Count > 0)
{
	var option = options.Pop();
	var point = option.pos;

	// Finish if we're at the end
	if (map[point] == 0)
	{
		option.steps.Dump("Part 2");
		break;
	};

	// Add available steps to the heap
	foreach (var move in moves)
	{
		var next = point + move;

		if (!seen.Contains(next) && map.ContainsKey(next) && (map[point] - map[next]) <= 1)
		{
			options.Insert((next, option.steps + 1));
			seen.Add(next);
		}
	}
}

// Debug time
string debug = "";
y = 0;
foreach (var line in input)
{
	var x = 0;

	foreach (var node in line.ToCharArray())
	{
		debug += seen.Contains(new(x,y)) ? (char) (map[new(x,y)] + 'a') : (char) (map[new(x,y)] + 'A');
		
		x++;
	}
	debug += "\n";
	y++;
}

debug.Dump("Seen Map");
seen.Dump();