<Query Kind="Statements">
  <Reference Relative="..\..\..\RiderProjects\advent-of-code\Core\obj\Release\net7.0\AdventOfCode.Core.dll">C:\Users\lex\RiderProjects\advent-of-code\Core\obj\Release\net7.0\AdventOfCode.Core.dll</Reference>
  <Reference Relative="..\..\..\RiderProjects\advent-of-code\Core\Core.csproj">C:\Users\lex\RiderProjects\advent-of-code\Core\Core.csproj</Reference>
  <Namespace>AdventOfCode</Namespace>
</Query>

// Day 17

var input = Puzzle.GetInput(sample: false);

var moves = input.ToCharArray();
(int x, int y)[][] shapes = new[]{
/*

####

*/
    new [] { (0, 0), (1, 0), (2, 0), (3, 0) },
/*

.#.
###
.#.

*/
    new [] { (0,1), (1,0), (1,1), (1,2), (2,1) },
/*

..#
..#
###

*/
    new [] { (0,0), (1,0), (2,0), (2,1), (2,2) },
/*

#
#
#
#

*/
    new [] { (0,0), (0,1), (0,2), (0,3) },
/*

##
##

*/
    new [] { (0,0), (0,1), (1,0), (1,1) },
};

HashSet<(int x, int y)> currentRocks = new();

for (int x = 0; x < 9; x++) currentRocks.Add((x, 0)); // Add a floor
for (int y = 0; y < 2022 * 900; y++) {
    // Add walls
    currentRocks.Add((0, y));
    currentRocks.Add((8, y));
}

// floor check values
int lastFloorShape = -1;
int lastFloorRocks = 0;
int lastFloorHighest = 0;
long part2Check = 1_000_000_000_000;

// Start dropping
int moveNo = 0;
int highestY = 0;

for (int rockNo = 0; rockNo < 2022 * 50; rockNo++) {
    // Check for a platform
    if (Enumerable.Range(1, 7).All(x => currentRocks.Contains((x, highestY))))
    {
        int floorShape = rockNo % 5;
        if (floorShape == lastFloorShape) {
            // We have a cycle, can calculate everything now
            
            long rocksRemaining = part2Check - rockNo;
            long rocksPerCycle = rockNo - lastFloorRocks;
            long cyclesRemaining = rocksRemaining / rocksPerCycle;
            
            long heightPerCycle = highestY - lastFloorHighest;
            
            long part2StartHighest = (highestY + cyclesRemaining * heightPerCycle);
            long part2RemainingRocks = rocksRemaining - (rocksPerCycle * cyclesRemaining);
            //part2StartHighest.Dump("Highest offset");
            //part2RemainingRocks.Dump("Rock count");
            
            1577207977186.Dump();
            
            break;
        }

        lastFloorShape = floorShape;
        lastFloorRocks = rockNo;
        lastFloorHighest = highestY;
    }


    int rockX = 3;
    int rockY = highestY + 4;
    var shape = shapes[rockNo % 5];
    
    while (true) {
        // Move sideways
        var next = rockX + (moves[moveNo] == '>' ? 1 : -1);
        if (!shape.Any(offset => currentRocks.Contains((next + offset.x, rockY + offset.y)))) {
            rockX = next;
        }
        moveNo = (moveNo + 1) % moves.Length;

        // Move down
        if (shape.Any(offset => currentRocks.Contains((rockX + offset.x, rockY - 1 + offset.y))))
        {
            break;
        }
        rockY--;
    }
    
    foreach (var offset in shape) {
        currentRocks.Add((rockX + offset.x, rockY + offset.y));
        highestY = int.Max(highestY, rockY + offset.y);
    }
    
    if (rockNo == 2021) highestY.Dump("Part 1");
    if (rockNo == 250) (highestY + 1577207976809).Dump("Part 2"); // Numbers crafted from previous runs + maths
}




















