<Query Kind="Statements">
  <Reference Relative="..\..\..\RiderProjects\advent-of-code\Core\obj\Release\net7.0\AdventOfCode.Core.dll">C:\Users\lex\RiderProjects\advent-of-code\Core\obj\Release\net7.0\AdventOfCode.Core.dll</Reference>
  <Reference Relative="..\..\..\RiderProjects\advent-of-code\Core\Core.csproj">C:\Users\lex\RiderProjects\advent-of-code\Core\Core.csproj</Reference>
  <Namespace>AdventOfCode</Namespace>
</Query>

// Day 16, more fun

var input = Puzzle.GetInput(sample: false)
	.Split("\n");

var rooms = new Dictionary<string, (string key, int flow, List<string> doors)>();

foreach (var line in input)
{
    var parts = line.Replace("Valve ", "").Replace(" has flow rate=", ":").Replace("; tunnel leads to valve ", ":").Replace("; tunnels lead to valves ", ":").Split(":");
    rooms[parts[0]] = (parts[0], int.Parse(parts[1]), parts[2].Split(", ").ToList());
}

// Build network of valves -> valves, we only care about shortest path between them
var ValveList = rooms.Values.Select(a => a.key).Order().ToList();

var dists = new int[rooms.Count, rooms.Count];
for (int i = 0; i < ValveList.Count; i++) //Fill in the default values
{
    for (int j = i; j < ValveList.Count; j++)
    {
        if (i == j) dists[i, j] = 0;
        else if (rooms[ValveList[i]].doors.Contains(ValveList[j]))
        {
            dists[i, j] = dists[j, i] = 1;
        } else
        {
            dists[i, j] = dists[j, i] = 9999; //Don't use int.MaxValue here because we need to do some additions.
        }
    }
}

for (int k = 0; k < ValveList.Count; k++)
{
    for (int i = 0; i < ValveList.Count; i++)
    {
        for (int j = i + 1; j < ValveList.Count; j++)
        {
            if (dists[i, k] + dists[k, j] < dists[i, j])
                dists[i, j] = dists[j, i] = dists[i, k] + dists[k, j];
        }
    }
}

//Only care about AA and the ones that generate flow.

var impValves = ValveList.Where(a => a == "AA" || rooms[a].flow != 0).ToList();
List<int> indices = new();

for (int i = 0; i < ValveList.Count; i++) if (rooms[ValveList[i]].flow != 0 || ValveList[i] == "AA") indices.Add(i);

int[,] impDists = new int[indices.Count(), indices.Count()];
int a = 0;
foreach (var i in indices)
{
    int b = 0;
    foreach (var j in indices)
    {
        impDists[a, b] = dists[i, j];
        impDists[b, a] = dists[j, i];
        b++;
    }
    a++;
}

var valveMasks = new int[impDists.GetLength(0)];
for (int i = 0; i < valveMasks.Length; i++) valveMasks[i] = 1 << i;

Dictionary<int, int> bestForState = new();
Queue<(int valve, int timeTaken, int valvesOpen, int eventualFlow)> toProcess = new();
toProcess.Enqueue((0, 30, 0, 0));

long stepCount = 0;
while (toProcess.Any()) {
    var step = toProcess.Dequeue();
    stepCount++;
    
    bestForState[step.valvesOpen] = int.Max(bestForState.GetValueOrDefault(step.valvesOpen, 0), step.eventualFlow);
    for (int i = 0; i < impValves.Count; i++) {
        var remainingTime = step.timeTaken - (impDists[step.valve, i]) - 1;
        if ((valveMasks[i] & step.valvesOpen) != 0 || remainingTime < 0) continue;
        toProcess.Enqueue((
            i,
            remainingTime,
            step.valvesOpen | valveMasks[i],
            step.eventualFlow + (remainingTime * rooms[impValves[i]].flow)
        ));
    }
}

bestForState.Max(s => s.Value).Dump();
stepCount.Dump();

