<Query Kind="Statements">
  <Reference Relative="..\..\..\RiderProjects\advent-of-code\Core\obj\Release\net7.0\AdventOfCode.Core.dll">C:\Users\lex\RiderProjects\advent-of-code\Core\obj\Release\net7.0\AdventOfCode.Core.dll</Reference>
  <Reference Relative="..\..\..\RiderProjects\advent-of-code\Core\Core.csproj">C:\Users\lex\RiderProjects\advent-of-code\Core\Core.csproj</Reference>
  <Namespace>AdventOfCode</Namespace>
  <Namespace>AdventOfCode.Grid</Namespace>
</Query>

// Day 22

var input = Puzzle.GetInput(sample: false)
	.Split("\n\n");
	

const int FLOOR = -1;
const int AIR = 0;
const int WALL = 1;

var matches = Regex.Matches(input[1], @"(\d+)([LR]?)");
var instructions = matches.Select(m => (dist: int.Parse(m.Groups[1].Value), rotate: m.Groups[2].Value));
Dictionary<Position, int> map = new();

int y = 1;
foreach (var row in input[0].Split("\n")) {
    int x = 1;
    foreach (var point in row) {
        map[new(x, y)] = point == '#' ? WALL : point == '.' ? FLOOR : AIR;
        x++;
    }
    y++;
}

// Fill missing points to make things easier (maybe)
int mapWidth = map.Max(kvp => kvp.Key.X);
int mapHeight = map.Max(kvp => kvp.Key.Y);
for (int x = 1; x <= mapWidth; x++) {
    for (y = 1; y <= mapHeight; y++) {
        if (!map.ContainsKey(new(x, y))) map[new(x, y)] = AIR;
    }
}

// Loop through instructions and hope I can do something sensible
(int, int)[] moves = new[]{
    (1, 0),
    (0, 1),
    (-1, 0),
    (0, -1),
};
Position pos = new(1,1);
int heading = 0;

foreach (var instruction in instructions) {
    for (int i = 0; i < instruction.dist; i++) {
        var next = pos + moves[heading];
        
        // Check if we've hit a wall or fallen off
        if (!map.ContainsKey(next) || map[next] == AIR) {
            //next = heading switch
            //{
            //    0 => next with { X = 1},
            //    1 => next with { Y = 1},
            //    2 => new(1, y),
            //    3 => new(1, y),
            //};
            if (next.X > mapWidth) next = next with {X = 1};
            if (next.Y > mapHeight) next = next with { Y = 1 };
            if (next.X < 1) next = next with { X = mapWidth };
            if (next.Y < 1) next = next with { Y = mapHeight };

            while(map[next] == AIR) {
                next = next + moves[heading];

                if (next.X > mapWidth) next = next with { X = 1 };
                if (next.Y > mapHeight) next = next with { Y = 1 };
                if (next.X < 1) next = next with { X = mapWidth };
                if (next.Y < 1) next = next with { Y = mapHeight };
            }
        }
        
        if (map[next] != FLOOR) break;
        
        pos = next;
    }
    
    if (instruction.rotate == "R") heading = (heading + 1) % 4;
    if (instruction.rotate == "L") heading = (heading + 3) % 4;
}

(1000 * pos.Y + 4 * pos.X + (int) heading).Dump("Part 1");




// Space is now a cube, edge case management time
pos = new(1, 1);
heading = 0;

/*
    +---+---+
    | 1 | 2 |
    +---+---+
    | 3 |
+---+---+
| 5 | 4 |
+---+---+
| 6 |
+---+

*/

foreach (var instruction in instructions)
{
    for (int i = 0; i < instruction.dist; i++)
    {
        var nextHeading = heading;
        var next = pos + moves[heading];

        // Check if we've hit a wall or fallen off
        if (!map.ContainsKey(next) || map[next] == AIR)
        {
            if (heading == 0) {
                // Travelling "right"
                if (next.Y is >= 0 and < 50) {
                    // Moving off 2, onto 4
                    nextHeading = 2; // Heading map left
                    next = pos with {
                        Y = 100 + (50 - next.Y)
                    };
                } else if (next.Y is >= 50 and < 100) {
                    // Moving off 3, onto 2
                    nextHeading = 3; // Heading map up
                    next = pos with
                    {
                        Y = 49,
                        X = 100 + (next.Y - 50),
                    };
                } else if (next.Y is >= 100 and < 150) {
                    // Moving off 4, onto 2
                    nextHeading = 2; // Heading map left
                    next = pos with
                    {
                        Y = 49, // NOT DONE
                        X = 100 + (next.Y - 50), // NOT DONE
                    };
                } else if (next.Y is >= 150) {
                    // Moving off 6, onto 3
                    nextHeading = 3; // Heading map up
                    next = pos with
                    {
                        Y = 149,
                        X = 50 + (next.Y - 50),
                    };
                }
            } else if (heading == 1) {
                
            } else if (heading == 2) {
                
            } else if (heading == 3) {
                
            }
        }

        if (map[next] != FLOOR) break;

        pos = next;
        heading = nextHeading;
    }

    if (instruction.rotate == "R") heading = (heading + 1) % 4;
    if (instruction.rotate == "L") heading = (heading + 3) % 4;
}