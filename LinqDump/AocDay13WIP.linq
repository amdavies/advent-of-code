<Query Kind="Statements">
  <Reference Relative="..\..\..\RiderProjects\advent-of-code\Core\obj\Release\net7.0\AdventOfCode.Core.dll">C:\Users\lex\RiderProjects\advent-of-code\Core\obj\Release\net7.0\AdventOfCode.Core.dll</Reference>
  <Reference Relative="..\..\..\RiderProjects\advent-of-code\Core\Core.csproj">C:\Users\lex\RiderProjects\advent-of-code\Core\Core.csproj</Reference>
  <Namespace>AdventOfCode</Namespace>
  <Namespace>System.Text.Json</Namespace>
</Query>

// Day 13

var input = Puzzle.GetInput(sample: false);
var inputA = input
	.Split("\n\n")
	.Select(p => p.Split("\n").ToTuple2());
	
int pairNo = 1;
int pairSum = 0;
foreach ((string left, string right) pair in inputA)
{
	var leftData = JsonSerializer.Deserialize<JsonElement>(pair.left);
	var rightData = JsonSerializer.Deserialize<JsonElement>(pair.right);

	var res = Comparator.compareValues(leftData, rightData);
	if (res == 1) {
		pairSum += pairNo;
	}
	pairNo++;
}
pairSum.Dump("Part 1");

var inputB = input.Split("\n").Where(l => !string.IsNullOrEmpty(l)).Append("[[2]]").Append("[[6]]").ToList();

inputB.Sort((right, left) =>
{
	var leftData = JsonSerializer.Deserialize<JsonElement>(left);
	var rightData = JsonSerializer.Deserialize<JsonElement>(right);
	
	return Comparator.compareValues(leftData, rightData);
});

((inputB.IndexOf("[[2]]") + 1) * (inputB.IndexOf("[[6]]") + 1)).Dump();



static class Comparator
{
	const int FAIL = -1;
	const int UNKNOWN = 0;
	const int CORRECT = 1;

	public static int compareValues(JsonElement left, JsonElement right)
	{
		return (left.ValueKind, right.ValueKind) switch
		{
		(JsonValueKind.Number, JsonValueKind.Number)
			=> compareNumbers(left.GetInt32(), right.GetInt32()),
		(JsonValueKind.Number, JsonValueKind.Array)
			=> compareArrays(new[]{left}, right.EnumerateArray().ToArray()),
			(JsonValueKind.Array, JsonValueKind.Number)
				=> compareArrays(left.EnumerateArray().ToArray(), new []{right}),
			(JsonValueKind.Array, JsonValueKind.Array)
				=> compareArrays(left.EnumerateArray().ToArray(), right.EnumerateArray().ToArray()),
			(JsonValueKind.Undefined, JsonValueKind.Undefined) => UNKNOWN,
			(JsonValueKind.Undefined, _) => CORRECT,
			(_, JsonValueKind.Undefined) => FAIL,
			_ => throw new Exception($"It gone cray cray: {left.ValueKind} -- {right.ValueKind}")
		};
	}

	public static int compareArrays(JsonElement[] left, JsonElement[] right)
	{
		for (int i = 0; i < left.Length; i++)
		{
			if (i >= right.Length)
			{
				return FAIL;
			}
			
			var res = compareValues(left[i], right[i]);

			if (res != UNKNOWN)
			{
				//(left[i], right[i]).Dump("Found Result");
				return res;
			}
		}
		

		return left.Length == right.Length ? UNKNOWN : left.Length < right.Length ? CORRECT : FAIL;
	}
	
	public static int compareNumbers(int left, int right)
	{
		if (left < right) {
			//(left,right).Dump("Correct");
			return CORRECT;
		}
		if (right < left) {
			//(left,right).Dump("Fail");
			return FAIL;
		}
		//(left,right).Dump("Unknown");
		return UNKNOWN;

	}
}