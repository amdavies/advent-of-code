<Query Kind="Statements">
  <Reference Relative="..\..\..\RiderProjects\advent-of-code\Core\obj\Release\net7.0\AdventOfCode.Core.dll">C:\Users\lex\RiderProjects\advent-of-code\Core\obj\Release\net7.0\AdventOfCode.Core.dll</Reference>
  <Reference Relative="..\..\..\RiderProjects\advent-of-code\Core\Core.csproj">C:\Users\lex\RiderProjects\advent-of-code\Core\Core.csproj</Reference>
  <Namespace>AdventOfCode</Namespace>
</Query>

// Day 19

var input = Puzzle.GetInput(sample: true)
	.Split("\n");
	
var blueprints = input.Select(l => {
    var parts = l.Split(" ");
    return (new[] { parts[6], parts[12], parts[18], parts[21], parts[27], parts[30]}).Select(int.Parse).ToArray();
}).Take(3).ToList();
	
HashSet<State> options = new();
int bId = 0;
foreach (var _ in blueprints) options.Add(new(bId++));


for (int minute = 0; minute < 32; minute++) {
    HashSet<State> nextOptions = new();
    foreach (var state in options) {
        var blueprint = blueprints[state.blueprint];
        
        var nextOre = state.ore + state.oreRobots;
        var nextClay = state.clay + state.clayRobots;
        var nextObsidian = state.obsidian + state.obsidianRobots;
        var nextGeode = state.geode + state.geodeRobots;
        
        // Can we afford geode robots?
        if (state.ore >= blueprint[4] && state.obsidian >= blueprint[5]) {
            nextOptions.Add(state with {
                ore = nextOre - blueprint[4],
                clay = nextClay,
                obsidian = nextObsidian - blueprint[5],
                geode = nextGeode,
                geodeRobots = state.geodeRobots + 1
            });
        }

        // Can we afford obsidian robots?
        if (state.ore >= blueprint[2] && state.clay >= blueprint[3] && state.obsidianRobots < 20)
        {
            nextOptions.Add(state with
            {
                ore = Math.Min(nextOre - blueprint[2], 20),
                clay = Math.Min(nextClay - blueprint[3], 20),
                obsidian = Math.Min(nextObsidian, 20),
                geode = Math.Min(nextGeode, 20),
                obsidianRobots = state.obsidianRobots + 1
            });
        }

        // Can we afford clay robots?
        if (state.ore >= blueprint[1] && state.clayRobots < 20)
        {
            nextOptions.Add(state with
            {
                ore = Math.Min(nextOre - blueprint[1], 20),
                clay = Math.Min(nextClay, 20),
                obsidian = Math.Min(nextObsidian, 20),
                geode = Math.Min(nextGeode, 20),
                clayRobots = state.clayRobots + 1
            });
        }

        // Can we afford ore robots?
        if (state.ore >= blueprint[0] && state.oreRobots < 5)
        {
            nextOptions.Add(state with
            {
                ore = Math.Min(nextOre - blueprint[0], 20),
                clay = Math.Min(nextClay, 20),
                obsidian = Math.Min(nextObsidian, 20),
                geode = Math.Min(nextGeode, 20),
                oreRobots = state.oreRobots + 1
            });
        }
        
        // Or we do nothing and wait?
        nextOptions.Add(state with {
            ore = Math.Min(nextOre, 20),
            clay = Math.Min(nextClay, 20),
            obsidian = Math.Min(nextObsidian, 20),
            geode = nextGeode,
        });
    }
    options = nextOptions;
}

int totalQuality = 0;
int c = 1;

var bestPer = options.GroupBy(o => o.blueprint).Select(g => g.Max(o => o.geode)).ToArray();
foreach (var best in bestPer) {
    totalQuality += best * c++;
}
totalQuality.Dump();
bestPer.Aggregate(1, (a, b) => a * b).Dump();


record State(
    int blueprint,
    int oreRobots = 1,
    int clayRobots = 0,
    int obsidianRobots = 0,
    int geodeRobots = 0,
    int ore = 0,
    int clay = 0,
    int obsidian = 0,
    int geode = 0
);
