<Query Kind="Statements">
  <Reference Relative="..\..\..\RiderProjects\advent-of-code\Core\obj\Release\net7.0\AdventOfCode.Core.dll">C:\Users\lex\RiderProjects\advent-of-code\Core\obj\Release\net7.0\AdventOfCode.Core.dll</Reference>
  <Reference Relative="..\..\..\RiderProjects\advent-of-code\Core\Core.csproj">C:\Users\lex\RiderProjects\advent-of-code\Core\Core.csproj</Reference>
  <Namespace>AdventOfCode</Namespace>
</Query>

// Day 20

var input = Puzzle.GetInput(sample: false)
	.Split("\n").Select(int.Parse);


// Create list
long key = 811589153;
List<Node> nodes = new();
bool first = true;
foreach (var value in input) {
    var node = new Node { Value = value * key };
    if (!first) {
        node.Previous = nodes.Last();
        nodes.Last().Next = node;
    }
    first = false;
    nodes.Add(node);
}
nodes.Last().Next = nodes.First();
nodes.First().Previous = nodes.Last();


// Shuffle
int shuffleCount = 10;
for (int _ = 0; _ < shuffleCount; _++)
{
    foreach (var node in nodes)
    {
        long stepCount = Math.Abs(node.Value) % (nodes.Count - 1);
        for (long i = 0; i < stepCount; i++)
        {
            if (node.Value > 0) node.MoveRight();
            else node.Previous.MoveRight();
        }
    }
}

// Find start, and fetch all required
var points = nodes.Find(node => node.Value == 0).Take(3001).ToArray();
(points[1000] + points[2000] + points[3000]).Dump();



class Node : IEnumerable<long>
{
    public long Value;
    public Node Previous;
    public Node Next;

    public IEnumerator<long> GetEnumerator()
    {
        var current = this;
        while (true)
        {
            yield return current.Value;
            current = current.Next;
        }
    }

    IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    
    public void MoveRight()
    {
        var prev = this.Previous;
        var cur = this;
        var next = this.Next;
        var after = this.Next.Next;
        
        prev.Next = next;
        next.Previous = prev;
        next.Next = cur;
        cur.Previous = next;
        cur.Next = after;
        after.Previous = cur;
    }
}