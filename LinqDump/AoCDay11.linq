<Query Kind="Statements">
  <Reference Relative="..\..\..\RiderProjects\advent-of-code\Core\obj\Release\net7.0\AdventOfCode.Core.dll">C:\Users\lex\RiderProjects\advent-of-code\Core\obj\Release\net7.0\AdventOfCode.Core.dll</Reference>
  <Reference Relative="..\..\..\RiderProjects\advent-of-code\Core\Core.csproj">C:\Users\lex\RiderProjects\advent-of-code\Core\Core.csproj</Reference>
  <Namespace>AdventOfCode</Namespace>
</Query>

// Day 11

bool useSample = true;

List<Func<long, long>> Operations;
if (useSample)
{
	Operations = new(){
		i => i * 19,
		i => i + 6,
		i => i * i,
		i => i + 3,
	};
} else {
	Operations = new(){
		i => i * 5,
		i => i * i,
		i => i * 7,
		i => i + 1,
		i => i + 3,
		i => i + 5,
		i => i + 8,
		i => i + 2,
	};
}

int i = 0;
var input = Puzzle.GetInput(useSample)
	.Split("\n\n")
	.Select(m =>
	{
		var data = m.Split("\n")[1..];
		
		return new Monkey{
			Items = data[0].Split(": ")[1].Split(", ").Select(long.Parse).ToList(),
			Operation = Operations[i++],
			TestDivision = int.Parse(data[2].Split("by ")[1]),
			TargetTrue = int.Parse(data[3].Split("monkey ")[1]),
			TargetFalse = int.Parse(data[4].Split("monkey ")[1]),
		};
	}).ToList();

var lcm = (long) input.Select(m => (ulong) m.TestDivision).LeastCommonMultiple();
lcm.Dump();
input.Aggregate(1L, (acc, monkey) => acc * monkey.TestDivision).Dump();

for (int round = 0; round < 10_000; round++)
{
	foreach (var monkey in input) {
		foreach (var item in monkey.Items) {
			long worry = monkey.Operation(item) % lcm;
			int target = (worry % monkey.TestDivision == 0) ? monkey.TargetTrue : monkey.TargetFalse;
			input[target].Items.Add(worry);
		}
		
		monkey.Activity += monkey.Items.Count();
		monkey.Items.Clear();
	}
}

var a = input.Select(m => m.Activity).Order().Reverse().Take(2).ToList();
(a[0] * a[1]).Dump("Part 1");

class Monkey
{
	public List<long> Items;
	public Func<long, long> Operation;
	public int TestDivision;
	public int TargetTrue;
	public int TargetFalse;
	
	public long Activity;
}