<Query Kind="Statements">
  <Reference Relative="..\..\..\RiderProjects\advent-of-code\Core\obj\Release\net7.0\AdventOfCode.Core.dll">C:\Users\lex\RiderProjects\advent-of-code\Core\obj\Release\net7.0\AdventOfCode.Core.dll</Reference>
  <Reference Relative="..\..\..\RiderProjects\advent-of-code\Core\Core.csproj">C:\Users\lex\RiderProjects\advent-of-code\Core\Core.csproj</Reference>
  <Namespace>AdventOfCode</Namespace>
</Query>

// Day 18

var input = Puzzle.GetInput(sample: false)
	.Split("\n")
	.Select(l => l.Split(",").Select(int.Parse).ToTuple3());
    
HashSet<(int x, int y, int z)> cubes = new();
foreach (var cube in input) {
    cubes.Add(cube);
}


// Starting surface area
int surfaceArea = input.Count() * 6;

foreach (var cube in cubes) {
    // Check each face, remove from area
    if (cubes.Contains((cube.x + 1, cube.y, cube.z))) surfaceArea--;
    if (cubes.Contains((cube.x - 1, cube.y, cube.z))) surfaceArea--;
    if (cubes.Contains((cube.x, cube.y + 1, cube.z))) surfaceArea--;
    if (cubes.Contains((cube.x, cube.y - 1, cube.z))) surfaceArea--;
    if (cubes.Contains((cube.x, cube.y, cube.z + 1))) surfaceArea--;
    if (cubes.Contains((cube.x, cube.y, cube.z - 1))) surfaceArea--;
}
surfaceArea.Dump("Part 1");


// Floor fill the exterior and count touch points
int externalArea = 0;
HashSet<(int x, int y, int z)> edgesFound = new();
HashSet<(int x, int y, int z)> seen = new();
Queue<(int x, int y, int z)> options = new();
options.Enqueue((0,0,0));

(int x, int y, int z)[] edges = new[]{
    ( 1,  0,  0),
    (-1,  0,  0),
    ( 0,  1,  0),
    ( 0, -1,  0),
    ( 0,  0,  1),
    ( 0,  0, -1),
};

int maxRange = int.Max(cubes.Max(c => c.x), int.Max(cubes.Max(c => c.y), cubes.Max(c => c.z))) + 5;
int minRange = int.Min(cubes.Min(c => c.x), int.Min(cubes.Max(c => c.y), cubes.Min(c => c.z))) - 5;

while (options.Any()) {
    var option = options.Dequeue();
    
    if (seen.Contains(option)) {
        // Lazy de-dupe
        continue;
    }
    seen.Add(option);

    // Check for touching cubes
    foreach (var edge in edges) {
        var edgeCube = (option.x + edge.x, option.y + edge.y, option.z + edge.z);
        if (cubes.Contains(edgeCube)) {
            externalArea += 1;
            edgesFound.Add(edgeCube);
            continue;
        }
        
        if (!seen.Contains(edgeCube) &&
            option.x <= maxRange && option.y <= maxRange && option.z <= maxRange &&
            option.x >= minRange && option.y >= minRange && option.z >= minRange
        ) {
            options.Enqueue(edgeCube);
        }
    }
}

externalArea.Dump("Part 2");