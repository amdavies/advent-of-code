<Query Kind="Statements">
  <Reference Relative="..\..\..\RiderProjects\advent-of-code\Core\obj\Release\net7.0\AdventOfCode.Core.dll">C:\Users\lex\RiderProjects\advent-of-code\Core\obj\Release\net7.0\AdventOfCode.Core.dll</Reference>
  <Reference Relative="..\..\..\RiderProjects\advent-of-code\Core\Core.csproj">C:\Users\lex\RiderProjects\advent-of-code\Core\Core.csproj</Reference>
  <Namespace>AdventOfCode</Namespace>
  <Namespace>AdventOfCode.Grid</Namespace>
</Query>

// Day 24

var input = Puzzle.GetInput(sample: false)
	.Split("\n");

var walls = new HashSet<Position>();
var occupied = new List<HashSet<Position>>()
{
    walls.ToHashSet(),
};
var blizzards = new List<Blizzard>();

int gridY = 0;
foreach (var line in input) {
    int gridX = 0;
    foreach(char a in line) {
        if (a is '#') {
            walls.Add(new (gridX, gridY));
            occupied[0].Add(new (gridX, gridY));
        }
        if (a is not '.' and not '#') {
            Direction dir = a switch {
                'v' => Direction.Down,
                '>' => Direction.Right,
                '<' => Direction.Left,
                '^' => Direction.Up,
                _ => throw new Exception($"Input is broken, see a '{a}'"),
            };
            Position pos = new(gridX, gridY);
            occupied[0].Add(pos);
            blizzards.Add(new(pos, dir));
        }
        gridX++;
    }
    gridY++;
}

int maxY = walls.Max(w => w.Y);
int maxX = walls.Max(w => w.X);

for (int time = 1; time < 2500; time++) {
    occupied.Add(walls.ToHashSet());
    var movedBlizzards = new List<Blizzard>();
    foreach (var blizzard in blizzards)
    {
        var nextPos = blizzard.direction switch {
            Direction.Up => blizzard.pos with { Y = blizzard.pos.Y - 1 },
            Direction.Down => blizzard.pos with { Y = blizzard.pos.Y + 1 },
            Direction.Left => blizzard.pos with { X = blizzard.pos.X - 1 },
            Direction.Right => blizzard.pos with { X = blizzard.pos.X + 1 },
            _ => throw new Exception("Input is broken"),
        };
        if (walls.Contains(nextPos)) {
            // Hit an edge, wrap
            nextPos = blizzard.direction switch
            {
                Direction.Up => nextPos with {Y = maxY - 1},
                Direction.Down => nextPos with {Y = 1},
                Direction.Left => nextPos with { X = maxX - 1},
                Direction.Right => nextPos with { X = 1 },
                _ => throw new Exception("Input is broken"),
            };
        }
        movedBlizzards.Add(blizzard with {pos = nextPos});
        occupied[time].Add(nextPos);
    }
    blizzards = movedBlizzards;
}

var availableMoves = new[]{
    (0, 0),
    (0, 1),
    (1, 0),
    (-1, 0),
    (0, -1),
};

Position start = new(1, 0);
Position end = new(maxX - 1, maxY);

HashSet<(Position pos, int steps)> visited = new();
int part2StepCount = 0;
var options = new Heap<(Position pos, int steps, List<Position> positions)>((point, point1) => point.steps - point1.steps);
options.Insert((start, 0, new() { start}));
while (options.Count > 0) {
    var option = options.Pop();
    if (option.pos == end) {
        option.steps.Dump("Part 1");
        part2StepCount += option.steps;
        break;
    }
    if (visited.Contains((option.pos, option.steps))) {
        continue;
    }
    visited.Add((option.pos, option.steps));
    
    var occupiedAtTime = occupied[option.steps + 1];
    foreach (var move in availableMoves)
    {
        var nextPos = option.pos + move;
        if (!occupiedAtTime.Contains(nextPos) && nextPos.X > 0 && nextPos.Y > 0)
        {
            options.Insert((option.pos + move, option.steps + 1, option.positions.Append(option.pos + move).ToList()));
        }
    }
}

// Grab Snacks!
visited = new();
options.Clear();
options.Insert((end, part2StepCount, new() { end }));
while (options.Count > 0)
{
    var option = options.Pop();
    if (option.pos == start)
    {
        part2StepCount = option.steps;
        break;
    }
    if (visited.Contains((option.pos, option.steps)))
    {
        continue;
    }
    visited.Add((option.pos, option.steps));
    
    if (option.steps == 25 && option.pos != end) {
        for (int time = part2StepCount; time <= option.steps; time++) {
            RenderState(time, option.positions[time - part2StepCount]);
        }
        
        return;
    }

    var occupiedAtTime = occupied[option.steps + 1];
    foreach (var move in availableMoves)
    {
        var nextPos = option.pos + move;
        if (!occupiedAtTime.Contains(nextPos) && nextPos.X >= 0 && nextPos.Y >= 0 && nextPos.X <= maxX && nextPos.Y <= maxY)
        {
            options.Insert((option.pos + move, option.steps + 1, option.positions.Append(option.pos + move).ToList()));
        }
    }
}
// Return with Snacks!
visited = new();
options.Clear();
options.Insert((start, part2StepCount, new() { start }));
while (options.Count > 0)
{
    var option = options.Pop();
    if (option.pos == end)
    {
        option.steps.Dump("Part 2");
        part2StepCount = option.steps;
        break;
    }
    if (visited.Contains((option.pos, option.steps)))
    {
        continue;
    }
    visited.Add((option.pos, option.steps));

    var occupiedAtTime = occupied[option.steps + 1];
    foreach (var move in availableMoves)
    {
        var nextPos = option.pos + move;
        if (!occupiedAtTime.Contains(nextPos) && nextPos.X >= 0 && nextPos.Y >= 0 && nextPos.X <= maxX && nextPos.Y <= maxY)
        {
            options.Insert((option.pos + move, option.steps + 1, option.positions.Append(option.pos + move).ToList()));
        }
    }
}


void RenderState(int time, Position elf) {
    string output = "";
    
    for (int y = 0; y <= maxY; y++) {
        for (int x = 0; x <= maxX; x++) {
            if (elf == new Position(x, y)) {
                output += "E";
                continue;
            }
            output += walls.Contains(new(x, y)) ? "#" : occupied[time].Contains(new(x, y)) ? "x" : ".";
        }
        output += "\n";
    }
    
    output.Dump(time.ToString());
}


record Blizzard(Position pos, Direction direction);
