<Query Kind="Statements">
  <Reference Relative="..\..\..\RiderProjects\advent-of-code\Core\obj\Release\net7.0\AdventOfCode.Core.dll">C:\Users\lex\RiderProjects\advent-of-code\Core\obj\Release\net7.0\AdventOfCode.Core.dll</Reference>
  <Reference Relative="..\..\..\RiderProjects\advent-of-code\Core\Core.csproj">C:\Users\lex\RiderProjects\advent-of-code\Core\Core.csproj</Reference>
  <Namespace>AdventOfCode</Namespace>
  <Namespace>AdventOfCode.Grid</Namespace>
</Query>

// Day 15

var input = Puzzle.GetInput(sample: false)
	.Split("\n");
	
List<(Position pos, int d)> sensors = new();
HashSet<Position> beacons = new();

foreach (var line in input) {
	var parts = line
		.Replace("x=", "")
		.Replace("y=","")
		.Replace("Sensor at ", "")
		.Replace(" closest beacon is at ", "")
		.Split(":").Select(p => Position.FromString(p)).ToArray();
		
	sensors.Add((parts[0], (int)parts[0].DistanceTo(parts[1])));
	beacons.Add(parts[1]);
}

int impossible = 0;
var minX = sensors.Min(s => s.pos.X - s.d);
var maxX = sensors.Max(s => s.pos.X + s.d);
for (int c = minX; c <= maxX; c++) {
	Position pos = new(c, 2000000);
	//Position pos = new(c, 10);
	if (beacons.Contains(pos)) continue;
	foreach (var s in sensors) {
		var dist = pos.DistanceTo(s.pos);
		if (dist <= s.d) {
			impossible++;
			break;
		}
	}
}
impossible.Dump("Part 1");

// 4332078 -- low?
// 5073496 -- correct!
const int maxCoord = 4_000_000;

for (var y = 0  ; y <= maxCoord  ; y++)
{
	List<(int min, int max)> bounds = sensors.Select(s =>
	{
		var yDelta = (s.pos.Y - y).Abs();
		var minX = Math.Max((s.pos.X - s.d + yDelta), 0);
		var maxX = Math.Min((s.pos.X + s.d - yDelta), maxCoord);
		return (minX, maxX);
	}).Where(e => e.Item1 <= e.Item2).OrderBy(e => e.Item1).ToList();

	bounds.Sort((a, b) => a.min.CompareTo(b.min));
	
	var merge = bounds.First();
	var toCheck = new Queue<(int min, int max)>(bounds.Skip(1).ToList());

	var isMerged = true;
	while (isMerged && toCheck.Count >= 1)
	{
		var next = toCheck.Dequeue();
		
		if (next.min > merge.max) {
			(merge, next).Dump();
			break;
		}

		if (merge.min <= next.min && merge.max >= next.min)
		{
			merge = (merge.min, Math.Max(merge.max, next.max));
		}
	}

	if (!isMerged || merge.min != 0 || merge.max != maxCoord)
	{
		toCheck.Dump();
		isMerged.Dump();
		merge.Dump();
		(((merge.max + 1)) * 4000000L + y).Dump("Part 2");
		break;
	}
}

// 10382630753392 -- Riens Answer
// 16256522651511 -- Too high