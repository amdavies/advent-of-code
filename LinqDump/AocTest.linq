<Query Kind="Statements">
  <Reference Relative="..\..\..\RiderProjects\advent-of-code\Core\obj\Release\net7.0\AdventOfCode.Core.dll">C:\Users\lex\RiderProjects\advent-of-code\Core\obj\Release\net7.0\AdventOfCode.Core.dll</Reference>
  <Reference Relative="..\..\..\RiderProjects\advent-of-code\Core\Core.csproj">C:\Users\lex\RiderProjects\advent-of-code\Core\Core.csproj</Reference>
  <Namespace>AdventOfCode</Namespace>
</Query>

// Day 10

var input = Puzzle.GetInput(sample: false)
	.Split("\n");
	
var display = "";
var total = 0;
var i = 1;
var x = 1;
foreach (var line in input) {
	var parts = line.Split(" ");
	Render();RunCycle();
	if (parts[0] == "addx") {
		Render();RunCycle();
		x = x + int.Parse(parts[1]);
	}
}

void RunCycle()
{
	if (i is 20 or 60 or 100 or 140 or 180 or 220)
	{
		total += i * x;
	}
	
	i++;
}

void Render()
{
	var p = i-1;
	if (p % 40 == 0 && p != 0)
	{
		display += "\n";
	}
	if (Math.Abs(x - (p % 40)) <= 1)
	{
		display += "#";
	}
	else
	{
		display += " ";
	}
}

total.Dump("Part 1");
display.Dump("Part 2");
	
