<Query Kind="Statements">
  <Reference Relative="..\..\..\RiderProjects\advent-of-code\Core\obj\Release\net7.0\AdventOfCode.Core.dll">C:\Users\lex\RiderProjects\advent-of-code\Core\obj\Release\net7.0\AdventOfCode.Core.dll</Reference>
  <Reference Relative="..\..\..\RiderProjects\advent-of-code\Core\Core.csproj">C:\Users\lex\RiderProjects\advent-of-code\Core\Core.csproj</Reference>
  <Namespace>AdventOfCode</Namespace>
  <Namespace>AdventOfCode.Grid</Namespace>
</Query>

// Day 23

var input = Puzzle.GetInput(sample: false)
	.Split("\n").Select(line => line.ToList()).ToList();



var grid = new HashSet<Position>();
for (var i = 0; i < input.Count; i++)
{
    for (var j = 0; j < input[i].Count; j++)
    {
        if (input[i][j] == '#')
        {
            grid.Add(new(j, i));
        }
    }
}

var movements = new Position[] {
    new(0, -1),
    new(0, 1),
    new(-1, 0),
    new(1, 0)
};
var diagonals = new Position[] {
    new(-1, -1),
    new(1, -1),
    new(-1, 1),
    new(1, 1),
    new(-1, -1),
    new(-1, 1),
    new(1, -1),
    new(1, 1)
};
var allDirections = movements.Concat(diagonals).Distinct().ToList();

var round = 1;
while (true)
{
    var possibleMoves = new List<(Position From, Position To)>();
    foreach (var elf in grid.Where(elf => allDirections.Any(move => grid.Contains(elf + move))))
    {
        for (var i = 0; i < 4; i++)
        {
            if (!grid.Contains(elf + movements[i]) && !grid.Contains(elf + diagonals[i * 2]) && !grid.Contains(elf + diagonals[(i * 2) + 1]))
            {
                possibleMoves.Add((elf, elf + movements[i]));
                break;
            }
        }
    }

    var moved = false;
    foreach (var move in possibleMoves.GroupBy(move => move.To).Where(group => group.Count() == 1).Select(group => group.Single()))
    {
        grid.Remove(move.From);
        grid.Add(move.To);
        moved = true;
    }

    if (round == 10)
    {
        int maxX = grid.Max(elf => elf.X);
        int minX = grid.Min(elf => elf.X);
        int maxY = grid.Max(elf => elf.Y);
        int minY = grid.Min(elf => elf.Y);
        
        (((maxX - minX + 1) * (maxY - minY + 1)) - grid.Count).Dump("Part 1");
    }

    if (!moved)
    {
        round.Dump("Part 2");
        return;
    }

    movements = movements.Skip(1).Concat(movements.Take(1)).ToArray();
    diagonals = diagonals.Skip(2).Concat(diagonals.Take(2)).ToArray();
    round++;
}