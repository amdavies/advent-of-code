﻿using System.Reflection;

namespace AdventOfCode.Y2022;

public static class Loader
{
    public static Dictionary<int, APuzzle> GetProblems()
    {
        var yearSolutions = new Dictionary<int, APuzzle>();

        Type[] types = Assembly.GetAssembly(typeof(Loader))!.GetTypes();
        foreach (Type type in types) {
            if (!typeof(APuzzle).IsAssignableFrom(type)) {
                continue;
            }

            if (type.IsAbstract) {
                continue;
            }

            APuzzle? problem = (APuzzle?) Activator.CreateInstance(type);
            if (problem == null) {
                continue;
            }

            yearSolutions.Add(problem.Day, problem);
        }

        return yearSolutions;
    }
}