namespace AdventOfCode.Y2022.Solution;

using ParsedInputType = Dictionary<string, Day7.Directory>;

public class Day7 : APuzzle<ParsedInputType>
{
    public Day7() : base(7, 2022, "No Space Left On Device")
    {
    }

    protected override ParsedInputType ParseInput()
    {
        var input = this.Input.Split("\n");
        Stack<string> path = new();
        Directory currentDirectory = new();
        path.Push("/");
        Dictionary<string, Directory> directories = new()
        {
            {"/", currentDirectory}
        };

        foreach (var commandLine in input)
        {
            if (commandLine == "$ cd /") continue;

            if (commandLine[0] == '$') {
                // Process command
                var cmd = commandLine.Split(' ');
                switch (cmd[1]) {
                    case "cd":
                        if (cmd[2] == "..") path.Pop();
                        else path.Push(cmd[2] + "/");
                        break;
                    case "ls":
                        string pathName = path.Reverse().Implode();
                        if (!directories.ContainsKey(pathName)) {
                            directories[pathName] = new();
                        }

                        currentDirectory = directories[pathName];
                        break;
                }
            } else {
                // Process data
                var result = commandLine.Split(' ');
                switch (result[0]) {
                    case "dir":
                        // Sub-directory
                        var pathName = path.Reverse().Implode() + result[1] + "/";

                        directories[pathName] = new();
                        currentDirectory.SubDirectories.Add(directories[pathName]);
                        break;
                    default:
                        // File
                        currentDirectory.ContainedFileSize += int.Parse(result[0]);
                        break;
                }
            }
        }

        return directories;
    }

    protected override async Task<string> SolvePartOne()
    {
        return this.ParsedInput
            .Select(s => s.Value.GetSize())
            .Where(s => s <= 100000)
            .Sum().ToString();
    }

    protected override async Task<string> SolvePartTwo()
    {
        int diskTotal = 70000000;
        int required = 30000000;
        int currentFree = diskTotal - this.ParsedInput["/"].GetSize();
        int toDelete = required - currentFree;

        return this.ParsedInput
            .Select(s => s.Value.GetSize())
            .Order()
            .First(s => s > toDelete).ToString();
    }

    public class Directory
    {
        public int ContainedFileSize;
        public readonly List<Directory> SubDirectories;

        public Directory() {
            this.SubDirectories = new List<Directory>();
        }

        public int GetSize() => ContainedFileSize + SubDirectories.Sum(d => d.GetSize());
    }
}
