namespace AdventOfCode.Y2022.Solution;

using ParsedInputType = IEnumerable<string>;

public class Day11 : APuzzleSolver<ParsedInputType> {
    public Day11() : base(11, 2022, "Untitled")
    {
    }

    protected override ParsedInputType ParseInput() => this.Input.SplitBlock();

    protected override async Task RunSolver()
    {

        List<Func<long, long>> Operations = new(){
            i => i * 5,
            i => i * i,
            i => i * 7,
            i => i + 1,
            i => i + 3,
            i => i + 5,
            i => i + 8,
            i => i + 2,
        };
        int i = 0;

        var input = this.ParsedInput.Select(m =>
        {
            var data = m.Split("\n")[1..];

            return new Monkey{
                Items = data[0].Split(": ")[1].Split(", ").Select(long.Parse).ToList(),
                Operation = Operations[i++],
                TestDivision = int.Parse(data[2].Split("by ")[1]),
                TargetTrue = int.Parse(data[3].Split("monkey ")[1]),
                TargetFalse = int.Parse(data[4].Split("monkey ")[1]),
            };
        }).ToList();

        var lcm = (long) input.Select(m => (ulong) m.TestDivision).LeastCommonMultiple();
        // lcm.Dump();
        // input.Aggregate(1L, (acc, monkey) => acc * monkey.TestDivision).Dump();

        for (int round = 0; round < 10_000; round++)
        {
            foreach (var monkey in input) {
                foreach (var item in monkey.Items) {
                    long worry = monkey.Operation(item) % lcm;
                    int target = (worry % monkey.TestDivision == 0) ? monkey.TargetTrue : monkey.TargetFalse;
                    input[target].Items.Add(worry);
                }

                monkey.Activity += monkey.Items.Count();
                monkey.Items.Clear();
            }

            if (round is 19)
            {
                var a = input.Select(m => m.Activity).Order().Reverse().Take(2).ToList();
                this.PartOneResult = (a[0] * a[1]).ToString();
            }
        }

        var b = input.Select(m => m.Activity).Order().Reverse().Take(2).ToList();

        this.PartTwoResult = (b[0] * b[1]).ToString();
    }

    class Monkey
    {
        public required List<long> Items;
        public required Func<long, long> Operation;
        public int TestDivision;
        public int TargetTrue;
        public int TargetFalse;

        public long Activity;
    }
}