namespace AdventOfCode.Y2022.Solution;

using ParsedInputType = IEnumerable<(int Start, int End)[]>;

public class Day4 : APuzzle<ParsedInputType>
{
    public Day4() : base(4, 2022, "Camp Cleanup")
    {
    }

    protected override ParsedInputType ParseInput() =>
        this.Input.Split("\n")
            .Select(s => s.Split(',')
                                .Select(e => e.Split('-').Select(int.Parse).ToTuple2())
                                .ToArray());

    protected override async Task<string> SolvePartOne() =>
        this.ParsedInput.Count(a => a[0].Contains(a[1]) || a[1].Contains(a[0])).ToString();

    protected override async Task<string> SolvePartTwo() =>
        this.ParsedInput.Count(
            a => a[0].Overlaps(a[1])
        ).ToString();
}