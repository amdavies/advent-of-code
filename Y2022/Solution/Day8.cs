using AdventOfCode.Enumeration;
using MoreLinq;

namespace AdventOfCode.Y2022.Solution;

using ParsedInputType = IEnumerable<int[]>;

public class Day8 : APuzzle<ParsedInputType>
{
    public Day8() : base(8, 2022, "Treetop Tree House")
    {
    }

    private int maxX;
    private int maxY;

    protected override ParsedInputType ParseInput()
    {
        var parsed = this.Input.Split("\n")
            .Select(r => r.ToIntArray()).ToArray();

        maxX = parsed[0].Length - 1;
        maxY = parsed.Length - 1;

        return parsed;
    }

    protected override async Task<string> SolvePartOne()
    {
        var input = this.ParsedInput.ToArray();

        return (..maxX,..maxY).Enumerate().Count(pos =>
            pos.a == 0 || pos.a == maxX || pos.b == 0 || pos.b == maxY || // Outside
            input[pos.b][..pos.a].Max() < input[pos.b][pos.a] || // From left
            input[pos.b][(pos.a + 1)..].Max() < input[pos.b][pos.a] || // From right
            input.Where((_, index) => index < pos.b)
                .Select(row => row[pos.a]).Max() < input[pos.b][pos.a] || // From above
            input.Where((_, index) => index > pos.b)
                .Select(row => row[pos.a]).Max() < input[pos.b][pos.a] // From below
         ).ToString();
    }

    protected override async Task<string> SolvePartTwo()
    {
        var input = this.ParsedInput.ToArray();

        return (..maxX, ..maxY).Enumerate().Max(pos =>
                // Looking left
                input[pos.b][..pos.a]
                    .Reverse()
                    .CountUntil(tree => tree < input[pos.b][pos.a])
                *
                // Looking right
                input[pos.b][(pos.a + 1)..]
                    .CountUntil(tree => tree < input[pos.b][pos.a])
                *
                // Looking Up
                input
                    .Where((_, index) => index < pos.b)
                    .Select(row => row[pos.a])
                    .Reverse()
                    .CountUntil(tree => tree < input[pos.b][pos.a])
                *
                // Looking Down
                input
                    .Where((_, index) => index > pos.b)
                    .Select(row => row[pos.a])
                    .CountUntil(tree => tree < input[pos.b][pos.a])
        ).ToString();
    }
}