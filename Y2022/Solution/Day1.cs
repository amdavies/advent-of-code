namespace AdventOfCode.Y2022.Solution;

using ParsedInputType = IEnumerable<int>;

public class Day1 : APuzzle<ParsedInputType>
{
    public Day1() : base(1, 2022, "Calorie Counting")
    {}

    protected override ParsedInputType ParseInput() =>
        this.Input.SplitBlock()
            .Select(e => e.ToIntArray("\n").Sum())
            .ToList();

    protected override async Task<string> SolvePartOne() => this.ParsedInput.Max().ToString();

    protected override async Task<string> SolvePartTwo() =>
        this.ParsedInput
            .OrderDescending()
            .Take(3)
            .Sum().ToString();
}