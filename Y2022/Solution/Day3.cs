namespace AdventOfCode.Y2022.Solution;

using ParsedInputType = IEnumerable<char[]>;

public class Day3 : APuzzle<ParsedInputType>
{
    public Day3() : base(3, 2022, "Rucksack Reorganization")
    {
    }

    protected override ParsedInputType ParseInput() => this.Input.Split("\n").Select(r => r.ToCharArray());

    protected override async Task<string> SolvePartOne()
    {
        return this.ParsedInput.Sum(r => {
            var c = r.Chunk(r.Length/2).ToArray();
            var inter = c[0].Intersect(c[1]).First();

            return char.IsUpper(inter) ? inter - 38 : inter - 96;
        }).ToString();
    }

    protected override async Task<string> SolvePartTwo()
    {
        return this.ParsedInput.Chunk(3).Sum(e => {
            var inter = e[0].Intersect(e[1]).Intersect(e[2]).First();
            return char.IsUpper(inter) ? inter - 38 : inter - 96;
        }).ToString();
    }
}