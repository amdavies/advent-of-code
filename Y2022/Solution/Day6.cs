using AdventOfCode.Enumeration;

namespace AdventOfCode.Y2022.Solution;

public class Day6 : APuzzle<char[]>
{
    public Day6() : base(6, 2022, "Tuning Trouble")
    {
    }

    protected override char[] ParseInput() => this.Input.ToCharArray();

    protected override async Task<string> SolvePartOne()
    {
        return (this.ParsedInput
            .RollingSlice(4)
            .First(s => s.value.Distinct().Count() == 4).index + 4)
            .ToString();
    }

    protected override async Task<string> SolvePartTwo()
    {
        return (this.ParsedInput
                .RollingSlice(14)
                .First(s => s.value.Distinct().Count() == 14).index + 14)
            .ToString();
    }
}