namespace AdventOfCode.Y2022.Solution;

using ParsedInputType = IEnumerable<string>;

public class Day10 : APuzzle<ParsedInputType>
{
    private int total;
    private string? display;

    public Day10() : base(10, 2022, "Cathode-Ray Tube")
    {
    }

    protected override ParsedInputType ParseInput() => this.Input.Split("\n");

    protected override async Task<string> SolvePartOne()
    {
        this.RunSolver();

        return total.ToString();
    }

    protected override async Task<string> SolvePartTwo()
    {
        if (string.IsNullOrEmpty(display)) this.RunSolver();

        return "\n" + display;
    }

    private void RunSolver()
    {
        var i = 1;
        var x = 1;
        foreach (var line in this.ParsedInput) {
            var parts = line.Split(" ");
            Render();RunCycle();
            if (parts[0] == "addx") {
                Render();RunCycle();
                x = x + int.Parse(parts[1]);
            }
        }

        void RunCycle()
        {
            if (i is 20 or 60 or 100 or 140 or 180 or 220)
            {
                total += i * x;
            }

            i++;
        }

        void Render()
        {
            var p = i-1;
            if (p % 40 == 0 && p != 0)
            {
                display += "\n";
            }
            if (Math.Abs(x - (p % 40)) <= 1)
            {
                display += "#";
            }
            else
            {
                display += " ";
            }
        }
    }
}