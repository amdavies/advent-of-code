namespace AdventOfCode.Y2022.Solution;

using ParsedInputType = IEnumerable<(string, string)>;

public class Day2 : APuzzle<ParsedInputType>
{
    public Day2() : base(2, 2022, "Rock Paper Scissors")
    {
    }

    private const int WIN = 6;
    private const int DRAW = 3;
    private const int LOSE = 0;
    private const int ROCK = 1;
    private const int PAPR = 2;
    private const int SISC = 3;

    private Dictionary<(string, string), (int r1, int r2)> roundScores = new()
    {
        {("A","X"), (DRAW + ROCK, LOSE + SISC)},
        {("A","Y"), (WIN  + PAPR, DRAW + ROCK)},
        {("A","Z"), (LOSE + SISC, WIN  + PAPR)},

        {("B","X"), (LOSE + ROCK, LOSE + ROCK)},
        {("B","Y"), (DRAW + PAPR, DRAW + PAPR)},
        {("B","Z"), (WIN  + SISC, WIN  + SISC)},

        {("C","X"), (WIN  + ROCK, LOSE + PAPR)},
        {("C","Y"), (LOSE + PAPR, DRAW + SISC)},
        {("C","Z"), (DRAW + SISC, WIN  + ROCK)},
    };

    protected override ParsedInputType ParseInput() => this.Input.Split("\n").Select(e => e.Split(" ").ToTuple2()).ToList();

    protected override async Task<string> SolvePartOne() => this.ParsedInput
        .Sum(r => this.roundScores[r].r1)
        .ToString();

    protected override async Task<string> SolvePartTwo() => this.ParsedInput
        .Sum(r => this.roundScores[r].r2)
        .ToString();
}