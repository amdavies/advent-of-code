using AdventOfCode.Grid;

namespace AdventOfCode.Y2022.Solution;

using ParsedInputType = IEnumerable<(char dir, int steps)>;

public class Day9 : APuzzle<ParsedInputType>
{
    public Day9() : base(9, 2022, "Rope Bridge")
    {
    }

    protected override ParsedInputType ParseInput() => this.Input
        .Split("\n")
        .Select(l => l.Split(' '))
        .Select(l => (l[0][0], int.Parse(l[1])));

    protected override async Task<string> SolvePartOne() => GetVisited().ToString();

    protected override async Task<string> SolvePartTwo() => GetVisited(10).ToString();

    private int GetVisited(int knotCount = 2)
    {
        HashSet<Position> visited = new() { new(0, 0) };
        var knots = new Position[knotCount];
        foreach (var line in this.ParsedInput)
        {
            for (int i = 0; i < line.steps; i++)
            {
                knots[0] = knots[0].AfterMove(line.dir);

                for (var k = 1; k < knots.Length; ++k)
                {
                    int xDiff = knots[k-1].X - knots[k].X;
                    int yDiff = knots[k-1].Y - knots[k].Y;
                    if (xDiff.Abs() <= 1 && yDiff.Abs() <= 1) continue;

                    (int x, int y) delta = (0, 0);
                    if (yDiff != 0) delta.y = yDiff.Sign();
                    if (xDiff != 0) delta.x = xDiff.Sign();
                    knots[k] += delta;
                }

                visited.Add(knots[^1]);
            }
        }

        return visited.Count;
    }
}