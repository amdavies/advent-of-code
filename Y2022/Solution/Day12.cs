using AdventOfCode.Grid;

namespace AdventOfCode.Y2022.Solution;

using ParsedInputType = IEnumerable<string>;

public class Day12 : APuzzleSolver<ParsedInputType>
{
    public Day12() : base(12, 2022, "Hill Climbing Algorithm")
    {
    }

    protected override ParsedInputType ParseInput() => this.Input.Split("\n");

    protected override async Task RunSolver()
    {
        var moves = new[] {
            new Position(1, 0),
            new Position(-1, 0),
            new Position(0, 1),
            new Position(0, -1),
        };

        Dictionary<Position, int> map = new();
        Position Start = new();
        Position End = new();

        var y = 0;
        foreach (var line in this.ParsedInput) {
            var x = 0;

            foreach (var node in line.ToCharArray()) {
                int height = node - 'a';
                if (node == 'S') {
                    height = 0;
                    Start = new(x, y);
                }
                if (node == 'E') {
                    height = 25;
                    End = new(x, y);
                }
                map[new(x,y)] = height;
                x++;
            }

            y++;
        }

        // map[End + (1, 0)].Dump();

        var seen = new HashSet<Position> { Start };
        var options = new Heap<(Position pos, int steps)>((point, point1) => point.steps - point1.steps);
        options.Insert((Start, 0));
        while (options.Count > 0)
        {
            var option = options.Pop();
            var point = option.pos;

            // Finish if we're at the end
            if (point == End)
            {
                this.PartOneResult = option.steps.ToString();
                break;
            };

            // Add available steps to the heap
            foreach (var move in moves)
            {
                var next = point + move;

                if (!seen.Contains(next) && map.ContainsKey(next) && (map[next] - map[point]) <= 1)
                {
                    options.Insert((next, option.steps + 1));
                    seen.Add(next);
                }
            }
        }

        seen = new HashSet<Position> { End };
        options.Clear();
        options.Insert((End, 0));
        while (options.Count > 0)
        {
            var option = options.Pop();
            var point = option.pos;

            // Finish if we're at the end
            if (map[point] == 0)
            {
                this.PartTwoResult = option.steps.ToString();
                break;
            };

            // Add available steps to the heap
            foreach (var move in moves)
            {
                var next = point + move;

                if (!seen.Contains(next) && map.ContainsKey(next) && (map[point] - map[next]) <= 1)
                {
                    options.Insert((next, option.steps + 1));
                    seen.Add(next);
                }
            }
        }
    }
}