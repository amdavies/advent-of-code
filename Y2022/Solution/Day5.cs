using System.Text.RegularExpressions;

namespace AdventOfCode.Y2022.Solution;

using ParsedInputType = IEnumerable<string>;

public class Day5 : APuzzle<ParsedInputType>
{
    public Day5() : base(5, 2022, "Supply Stacks")
    {
    }

    protected override ParsedInputType ParseInput() => this.Input.Split("\n\n");

    protected override async Task<string> SolvePartOne()
    {
        var input = this.ParsedInput.ToArray();
        var initial = input[0];
        var stacks = new Stack<char>[9].Select(s => new Stack<char>()).ToArray();
        foreach (var line in initial.Split("\n").Select(l => l.ToCharArray()).Where(l => l[1] != '1').Reverse()) {
            for (int i = 1, j = 0; j < 9; j++, i += 4) {
                char c = line[i];
                if (c == ' ') continue;
                stacks[j].Push(line[i]);
            }
        }

        var moveLine = new Regex(@"move (\d+) from (\d+) to (\d+)");
        IEnumerable<(int count, int from, int to)> moves = input[1].Split("\n").Select(l => moveLine.Matches(l)[0].Groups.Cast<Group>().Select(m => m.ToString()).Skip(1).Select(int.Parse).ToTuple3());

        foreach(var move in moves) {
            for (int i = 0; i < move.count; i++) {
                var t = stacks[move.from - 1].Pop();
                stacks[move.to - 1].Push(t);
            }
        }

        return stacks.Select(s => s.Peek()).Implode().ToString();
    }

    protected override async Task<string> SolvePartTwo()
    {
        var input = this.ParsedInput.ToArray();
        var initial = input[0];
        var stacks = new Stack<char>[9].Select(s => new Stack<char>()).ToArray();
        foreach (var line in initial.Split("\n").Select(l => l.ToCharArray()).Where(l => l[1] != '1').Reverse()) {
            for (int i = 1, j = 0; j < 9; j++, i += 4) {
                char c = line[i];
                if (c == ' ') continue;
                stacks[j].Push(line[i]);
            }
        }

        var moveLine = new Regex(@"move (\d+) from (\d+) to (\d+)");
        IEnumerable<(int count, int from, int to)> moves = input[1].Split("\n").Select(l => moveLine.Matches(l)[0].Groups.Cast<Group>().Select(m => m.ToString()).Skip(1).Select(int.Parse).ToTuple3());

        foreach(var move in moves) {
            var t = new Stack<char>();
            for (int i = 0; i < move.count; i++) {
                t.Push(stacks[move.from - 1].Pop());
            }

            while (t.Any()) {
                stacks[move.to - 1].Push(t.Pop());
            }
        }

        return stacks.Select(s => s.Peek()).Implode().ToString();
    }
}