using System.Drawing;
using AdventOfCode.Grid;

namespace AdventOfCode.Y2021.Solution;

public class Day17 : APuzzle<IEnumerable<int>>
{
    public Day17() : base(17, 2021, "Trick Shot")
    {}

    protected override async Task<string> SolvePartOne() => this.ParsedInput.Max().ToString();

    protected override async Task<string> SolvePartTwo() => this.ParsedInput.Count().ToString();

    private static IEnumerable<int> FireShots(((int min, int max) x, (int min, int max) y) target)
    {
        ((int Min, int Max) tX, (int Min, int Max) tY) = target;
        Rectangle t = new (tX.Min, tY.Min, (tX.Max-tX.Min).Abs() + 1, (tY.Max-tY.Min).Abs() + 1);

        for (int vX = (int) Math.Sqrt(2*tX.Min); vX <= tX.Max; vX++) {
            for (int vY = tY.Min; vY <= -tY.Min; vY++) {
                Position pos = new(0, 0);
                (int X, int Y) v = (vX, vY);
                int maxY = 0;

                while (pos.X <= tX.Max && pos.Y >= tY.Min) {
                    pos += v;
                    v.Y -= 1;
                    v.X = (0, v.X - 1).Max();

                    maxY = (maxY, pos.Y).Max();
                    if (!t.Contains(pos.X, pos.Y)) continue;

                    yield return maxY;
                    break;
                }
            }
        }
    }

    protected override IEnumerable<int> ParseInput() => FireShots(this.Input.Split("x=").Last().Split(", y=").Select(r => r.Split("..").Select(int.Parse).ToTuple2()).ToTuple2());
}
