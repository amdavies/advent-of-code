namespace AdventOfCode.Y2021.Solution;

public class Day21 : APuzzle<(int p1Start, int p2Start)>
{
    public Day21() : base(21, 2021, "Dirac Dice")
    {}

    protected override async Task<string> SolvePartOne()
    {
        int rollCount = 0;

        int p1Score = 0;
        int p1Pos = this.ParsedInput.p1Start;

        int p2Score = 0;
        int p2Pos = this.ParsedInput.p2Start;

        int dieStep = 0;

        while (p1Score < 1000 && p2Score < 1000) {
            int roll = RollDie(ref dieStep) + RollDie(ref dieStep) + RollDie(ref dieStep);
            rollCount += 3;
            p1Pos += roll;
            while (p1Pos > 10) p1Pos -= 10;
            p1Score += p1Pos;

            if (p1Score >= 1000) break;

            roll = RollDie(ref dieStep) + RollDie(ref dieStep) + RollDie(ref dieStep);
            rollCount += 3;
            p2Pos += roll;
            while (p2Pos > 10) p2Pos -= 10;
            p2Score += p2Pos;
        }

        return ((p1Score, p2Score).Min() * rollCount).ToString();
    }

    protected override async Task<string> SolvePartTwo()
    {
        ulong p1Wins = 0;
        ulong p2Wins = 0;

        Dictionary<(int p1p, int p1s, int p2p, int p2s), ulong> states = new();
        states.Add((this.ParsedInput.p1Start, 0, this.ParsedInput.p2Start, 0), 1);

        while (states.Any()) {
            Dictionary<(int p1p, int p1s, int p2p, int p2s), ulong> newStates = new();

            foreach (var (state, count) in states) {
                foreach (var (p1State, isP1Win) in QuantumRolls(state.p1p, state.p1s)) {
                    if (isP1Win) {
                        p1Wins += count;
                        continue;
                    }

                    foreach (var (p2State, isP2Win) in QuantumRolls(state.p2p, state.p2s)) {
                        if (isP2Win) {
                            p2Wins += count;
                            continue;
                        }

                        newStates.Increment((p1State.pos, p1State.score, p2State.pos, p2State.score), count);
                    }
                }
            }

            states = newStates;
        }

        return Math.Max(p1Wins, p2Wins).ToString();
    }

    private int RollDie(ref int dieStep)
    {
        dieStep++;

        if (dieStep > 100) dieStep -= 100;

        return dieStep;
    }

    private IEnumerable<((int pos, int score) state, bool isWin)> QuantumRolls(int pos, int score)
    {
        for (int r1 = 1; r1 <= 3; r1++) {
            for (int r2 = 1; r2 <= 3; r2++) {
                for (int r3 = 1; r3 <= 3; r3++) {
                    int newPos = pos + r1 + r2 + r3;
                    if (newPos > 10) newPos -= 10;

                    int newScore = score + newPos;

                    yield return ((newPos, newScore), newScore >= 21);
                }
            }
        }
    }

    protected override (int p1Start, int p2Start) ParseInput()
    {
        var parts = this.Input.Split('\n');

        return (int.Parse(new(parts[0].Skip(28).ToArray())), int.Parse(new(parts[1].Skip(28).ToArray())));
    }
}
