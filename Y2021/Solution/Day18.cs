using System.Text.Json;
using System.Text.RegularExpressions;

namespace AdventOfCode.Y2021.Solution;

using ParsedInputType = IEnumerable<string>;

public class Day18 : APuzzle<ParsedInputType>
{
    private static Regex lastDigitReg = new(@"(\d+)(\D+)$");
    private static Regex firstDigitReg = new(@"(\d+)");
    private static Regex doubleDigitReg = new(@"(\d\d+)");

    public Day18() : base(18, 2021, "Snailfish")
    {}

    protected override async Task<string> SolvePartOne()
    {
        return this.ParsedInput.Aggregate("",
            (agg, line) => agg == "" ? line : this.ReduceLine($"[{agg},{line}]"),
            NodeMagnitude).ToString();
    }

    protected override async Task<string> SolvePartTwo()
    {
        return this.ParsedInput
            .Subsets(2)
            .Where(s => s.Distinct().Count() == 2)
            .Max(a => (NodeMagnitude(this.ReduceLine($"[{a.First()},{a.Skip(1).First()}]")),NodeMagnitude(this.ReduceLine($"[{a.Skip(1).First()},{a.First()}]"))).Max())
            .ToString();
    }

    private string ReduceLine(string line)
    {
        while (true) {
            string initial = line;

            line = TryExplode(line);
            if (line != initial) continue;

            line = TrySplit(line);
            if (line != initial) continue;

            return line;
        }
    }

    private static string TryExplode(string line)
    {
        int depth = 0;
        int beforeCount = 0;

        for (int pos = 0; pos < line.Length; pos++) {
            switch (line[pos]) {
                case '[':
                    depth++;
                    continue;
                case ']':
                    depth--;
                    continue;
                case ',': break;
                default:
                    beforeCount++;
                    break;
            }

            if (depth <= 4) continue;

            // Should be at the first digit at this depth
            // Need to find an optional digit before, and an optional digit after
            string valString = line.Skip(pos).TakeWhile(cc => cc != ']').Implode();
            int[] vals = valString.Split(',').Select(int.Parse).ToArray();

            string before = line.Slice(0, pos - 1).Implode();

            if (beforeCount > 1) {
                before = lastDigitReg.Replace(before,
                    match =>
                    {
                        int newVal = int.Parse(match.Groups[1].Value) + vals[0];
                        return $"{newVal}{match.Groups[2].Value}";
                    });
            }

            string after = new (line.Skip(pos + valString.Length + 1).ToArray());
            after = firstDigitReg.Replace(after, match => (int.Parse(match.Groups[1].Value) + vals[1]).ToString(), 1);

            return $"{before}0{after}";
        }
        return line;
    }

    private static string TrySplit(string line)
    {
        var newLine = doubleDigitReg.Replace(line, match => $"[{Math.Floor(float.Parse(match.Value) / 2f)},{Math.Ceiling(float.Parse(match.Value) / 2f)}]", 1);

        return newLine;
    }

    private static int NodeMagnitude(string node)
    {
        return NodeMagnitude(JsonSerializer.Deserialize<JsonElement>(node));
    }

    private static int NodeMagnitude(JsonElement node)
    {
        if (node.ValueKind is JsonValueKind.Number) return node.GetInt32();
        return 3 * NodeMagnitude(node[0]) + 2 * NodeMagnitude(node[1]);
    }

    protected override ParsedInputType ParseInput()
    {
        return this.Input.Split('\n');
    }
}
