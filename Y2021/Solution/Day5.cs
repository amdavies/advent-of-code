using AdventOfCode.Grid;

namespace AdventOfCode.Y2021.Solution;

using ParsedInputType = IEnumerable<(Position start, Position end)>;

public class Day5 : APuzzle<ParsedInputType>
{
    public Day5() : base(5, 2021, "Hydrothermal Venture")
    {}

    protected override async Task<string> SolvePartOne() => this.countOverlaps(this.ParsedInput.Where(v => v.start.X == v.end.X || v.start.Y == v.end.Y)).ToString();

    protected override async Task<string> SolvePartTwo() => this.countOverlaps(this.ParsedInput).ToString();

    private int countOverlaps(ParsedInputType vents)
    {
        Dictionary<Position, int> filled = new();

        vents.ForEach(vent =>
        {
            Position curPos = vent.start;

            filled[vent.end] = filled.GetValueOrDefault(vent.end, 0) + 1;

            while (curPos != vent.end) {
                filled[curPos] = filled.GetValueOrDefault(curPos, 0) + 1;

                if (curPos.X < vent.end.X) curPos = curPos with {X = curPos.X + 1};
                if (curPos.X > vent.end.X) curPos = curPos with {X = curPos.X - 1};
                if (curPos.Y < vent.end.Y) curPos = curPos with {Y = curPos.Y + 1};
                if (curPos.Y > vent.end.Y) curPos = curPos with {Y = curPos.Y - 1};
            }
        });

        return filled.Count(kvp => kvp.Value > 1);
    }

    protected override ParsedInputType ParseInput() => this.Input.Split('\n')
        .Select(l =>l.Split(" -> ")
            .Select(p => p.ToPosition())
            .ToTuple2())
        .ToArray();
}