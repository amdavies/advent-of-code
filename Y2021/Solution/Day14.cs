namespace AdventOfCode.Y2021.Solution;

public class Day14 : APuzzle<(string start, Dictionary<string, string> inserts)>
{
    public Day14() : base(14, 2021, "Extended Polymerization")
    {}

    protected override async Task<string> SolvePartOne()
    {
        Dictionary<char, long> counts = this.CreatePolymer(10);
        return (counts.Values.Max() - counts.Values.Min()).ToString();
    }

    protected override async Task<string> SolvePartTwo()
    {
        Dictionary<char, long> counts = this.CreatePolymer(40);
        return (counts.Values.Max() - counts.Values.Min()).ToString();
    }

    private Dictionary<char, long> CreatePolymer(int steps)
    {
        // Count starting pairs
        var state = this.ParsedInput.start;
        var pairs = new Dictionary<string, long>();

        var altPairs = state.Zip(state.Skip(1)).Select((parts) => "" + parts.First + parts.Second).GroupBy(s => s).ToDictionary(s => s.Key, s => s.Count());

        for (int i = 0; i < state.Length - 1; i++) {
            var pair = new string(state.Skip(i).Take(2).ToArray());

            if (!pairs.ContainsKey(pair)) pairs[pair] = 0;
            pairs[pair]++;
        }

        // Increment pairs
        (1..steps).Each(step =>
        {
            var newPairs = pairs.Clone();

            pairs.ForEach((bigram, count) =>
            {
                var insert = this.ParsedInput.inserts[bigram];

                newPairs.Increment(bigram, -count);
                newPairs.Increment(bigram[0] + insert, count);
                newPairs.Increment(insert + bigram[1], count);
            });

            pairs = newPairs;
        });

        var counts = new Dictionary<char, long>() {
            { state.Last(), 1 }
        };

        pairs.ForEach((bigram, count) =>
        {
            if (!counts.ContainsKey(bigram[0])) counts[bigram[0]] = 0;
            counts[bigram[0]] += count;
        });
        return counts;
    }

    protected override (string, Dictionary<string, string>) ParseInput()
    {
        var parts = this.Input.Split("\n\n");

        return (parts[0], parts[1].Split('\n').Select(l => l.Split(" -> ").ToTuple2()).ToDictionary(s => s.Item1, s => s.Item2));
    }
}
