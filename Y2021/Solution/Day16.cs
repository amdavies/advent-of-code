using MoreLinq.Experimental;

namespace AdventOfCode.Y2021.Solution;

using ParsedInputType = IEnumerable<string>;

public class Day16 : APuzzle<string>
{
    public long SumOfVersions { get; set; }

    public Day16() : base(16, 2021, "Packet Decoder")
    {}

    protected override async Task<string> SolvePartOne()
    {
        ParseData(new(this.ParsedInput));

        return this.SumOfVersions.ToString();
    }

    protected override async Task<string> SolvePartTwo()
    {
        (_, long value) = ParseData(new(this.ParsedInput));

        return value.ToString();
    }

    private (Queue<char>, long) ParseData(Queue<char> data)
    {
        long version = data.Dequeue(3).LongFromBin();
        this.SumOfVersions += version;

        long type = data.Dequeue(3).LongFromBin();

        if (type == 4) {
            // Literal
            string val = "";

            bool last;
            do {
                last = data.Dequeue() == '0';
                val += data.Dequeue(4).Implode();
            } while (!last);

            return (data, val.LongFromBin());
        }

        // Operator
        int lengthType = int.Parse(data.Dequeue(1).Implode());
        List<long> subValues = new();

        if (lengthType == 0) {
            int length = (int) data.Dequeue(15).LongFromBin();

            Queue<char> subPackets = new(data.Take(length));
            while (subPackets.Any()) {
                (subPackets, long subValue) = this.ParseData(subPackets);
                subValues.Add(subValue);
            }
            data = new(data.Skip(length));
        } else {
            int packetCount = (int) data.Dequeue(11).LongFromBin();
            (1..packetCount).Each(_ =>
            {
                (data, long subValue) = this.ParseData(data);
                subValues.Add(subValue);
            });
        }

        return type switch {
            0 => (data, subValues.Sum(i => i)),
            1 => (data, subValues.Aggregate(1L, (agg, val) => agg * val)),
            2 => (data, subValues.Min()),
            3 => (data, subValues.Max()),
            5 => (data, subValues[0] > subValues[1] ? 1 : 0),
            6 => (data, subValues[0] < subValues[1] ? 1 : 0),
            7 => (data, subValues[0] == subValues[1] ? 1 : 0),
            _ => (data, 0L)
        };
    }

    protected override string ParseInput()
    {
        return this.Input
            .Replace("0","0000")
            .Replace("1","0001")
            .Replace("2","0010")
            .Replace("3","0011")
            .Replace("4","0100")
            .Replace("5","0101")
            .Replace("6","0110")
            .Replace("7","0111")
            .Replace("8","1000")
            .Replace("9","1001")
            .Replace("A","1010")
            .Replace("B","1011")
            .Replace("C","1100")
            .Replace("D","1101")
            .Replace("E","1110")
            .Replace("F","1111");
    }
}
