using System.Collections.Immutable;
using System.Numerics;

namespace AdventOfCode.Y2021.Solution;

using ParsedInputType = IEnumerable<IEnumerable<Vector3>>;

public class Day19 : APuzzle<ParsedInputType>
{
    public Day19() : base(19, 2021, "Beacon Scanner")
    {}

    protected override async Task<string> SolvePartOne()
    {
        (List<Vector3> _, ImmutableHashSet<Vector3> knownBeacons) = this.DecodeScanners();

        return knownBeacons.Count.ToString();
    }

    protected override async Task<string> SolvePartTwo()
    {
        (List<Vector3> scanners, ImmutableHashSet<Vector3> _) = this.DecodeScanners();

        return scanners.Max(s => scanners.Where(ss => ss != s).Max(ss => ((int) (s.X - ss.X)).Abs() + ((int) (s.Y - ss.Y)).Abs() + ((int) (s.Z - ss.Z)).Abs())).ToString();
    }

    private IEnumerable<Vector3> WithOrientations(Vector3 source)
    {
        return WithHeadings(source).SelectMany(b => WithRotations(b));
    }

    private static IEnumerable<Vector3> WithHeadings(Vector3 source)
    {
        var current = source;
        for (int i = 0; i < 3; i++) {
            yield return current;
            yield return new(-current.X, -current.Y, current.Z);

            current = new(current.Y, current.Z, current.X);
        }
    }

    private static IEnumerable<Vector3> WithRotations(Vector3 source)
    {
        var current = source;

        for (int i = 0; i < 4; i++) {
            yield return current;
            current = new(current.X, -current.Z, current.Y);
        }
    }

    private (List<Vector3> scanners, ImmutableHashSet<Vector3> knownBeacons) DecodeScanners()
    {
        // Treat scanner 1 as our source of truth
        var s1 = this.ParsedInput.First();
        List<Vector3> scanners = new() { new(0, 0, 0) };

        ImmutableHashSet<Vector3> knownBeacons = s1.ToImmutableHashSet();

        var rotatedQueue = new Queue<(int id, List<List<Vector3>> set)>();

        this.ParsedInput.Skip(1).Select(sb =>
                sb.SelectMany(b => this.WithOrientations(b).Select((rotated, i) => (index: i, value: rotated)))
                    .GroupBy(b => b.index, b => b.value)
                    .Select(group => group.ToList()))
            .ForEach((s, i) => rotatedQueue.Enqueue((i + 1, s.ToList())));

        while (rotatedQueue.Any()) {
            var rotationSet = rotatedQueue.Dequeue();

            bool found = false;

            foreach (var beacons in rotationSet.set) {
                var offset = Enumerable.MaxBy(beacons.SelectMany(source => knownBeacons.Select(target => target - source))
                        .GroupBy(offset => offset)
                        .Select(group => (Offset: @group.Key, Count: @group.Count())),
                    i => i.Count);
                if (offset.Count < 12) continue;

                found = true;
                knownBeacons = knownBeacons.Concat(beacons.Select(b => b + offset.Offset)).ToImmutableHashSet();
                scanners.Add(offset.Offset);
                break;
            }

            if (!found) {
                if (!rotatedQueue.Any()) throw new Exception("Unable to match up final set");
                rotatedQueue.Enqueue(rotationSet);
            }

            ;
        }

        return (scanners, knownBeacons);
    }

    protected override ParsedInputType ParseInput()
    {
        return this.Input.Split("\n\n").Select(s => s.Split('\n').Skip(1).Select(l =>
        {
            var parts = l.Split(',').Select(int.Parse).ToArray();
            return new Vector3(parts[0], parts[1], parts[2]);
        }));
    }
}
