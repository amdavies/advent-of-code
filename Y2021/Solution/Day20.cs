using AdventOfCode.Grid;

namespace AdventOfCode.Y2021.Solution;

public class Day20 : APuzzle<(string enhancement, Dictionary<Position, bool> input)>
{
    public Day20() : base(20, 2021, "Trench Map")
    {}

    protected override async Task<string> SolvePartOne()
    {
        string enhancementLookup = this.ParsedInput.enhancement;
        Dictionary<Position, bool> image = this.ParsedInput.input.Clone();

        OutputGrid(image, enhancementLookup[0] == '#');

        for (int i = 1; i <= 2; i++) {
            image = this.Enhance(image, enhancementLookup);

            OutputGrid(image, enhancementLookup[0] == '#');
        }

        return image.Count(kvp => kvp.Value).ToString();
    }

    private void OutputGrid(Dictionary<Position, bool> image, bool doesFlip)
    {
        if (!this.Debug) return;
        Console.WriteLine($"\n\n");

        int minX = image.Min(kvp => kvp.Key.X) - 1;
        int minY = image.Min(kvp => kvp.Key.Y) - 1;
        int maxX = image.Max(kvp => kvp.Key.X) + 1;
        int maxY = image.Max(kvp => kvp.Key.Y) + 1;

        bool defaultPixel = (maxX) % 2 == 0 && doesFlip;

        for (int y = minY; y <= maxY; y++) {
            for (int x = minX; x <= maxX; x++) {
                Console.Write(image.GetValueOrDefault(new(x, y), defaultPixel) ? "#" : ".");
            }
            Console.WriteLine("");
        }
    }

    public bool Debug { get; set; } = false;

    protected override async Task<string> SolvePartTwo()
    {
        string enhancementLookup = this.ParsedInput.enhancement;
        Dictionary<Position, bool> image = this.ParsedInput.input.Clone();

        for (int i = 1; i <= 50; i++) {
            image = this.Enhance(image, enhancementLookup);
        }

        return image.Count(kvp => kvp.Value).ToString();
    }

    private Dictionary<Position, bool> Enhance(Dictionary<Position, bool> image, string enhancementLookup)
    {
        Dictionary<Position, bool> enhancedImage = new();

        int minX = image.Min(kvp => kvp.Key.X) - 1;
        int minY = image.Min(kvp => kvp.Key.Y) - 1;
        int maxX = image.Max(kvp => kvp.Key.X) + 1;
        int maxY = image.Max(kvp => kvp.Key.Y) + 1;

        bool defaultPixel = (maxX + 1) % 2 == 0 && enhancementLookup[0] == '#';

        for (int y = minY; y <= maxY; y++) {
            for (int x = minX; x <= maxX; x++) {
                Position pos = new(x, y);
                enhancedImage[pos] = this.PixelAtPosition(pos, image, enhancementLookup, defaultPixel);
            }
        }

        image = enhancedImage;
        return image;
    }

    private bool PixelAtPosition(Position pos, Dictionary<Position, bool> image, string enhancementLookup, bool defaultPixel)
    {
        string binary = "";

        for (int y = -1; y <= 1; y++) {
            for (int x = -1; x <= 1; x++) {
                binary += image.GetValueOrDefault(pos + new Position(x, y), defaultPixel) ? '1' : '0';
            }
        }

        return enhancementLookup[binary.IntFromBin()] == '#';
    }

    protected override (string enhancement, Dictionary<Position, bool> input) ParseInput()
    {
        var parts = this.Input.Split("\n\n");
        var grid = new Dictionary<Position, bool>();

        parts[1].Split('\n').ForEach((l, y) => l.ForEach((c, x) => grid[new(x,y)] = c == '#'));

        return (parts[0], grid);
    }
}
