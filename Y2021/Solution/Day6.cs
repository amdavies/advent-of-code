namespace AdventOfCode.Y2021.Solution;

public class Day6 : APuzzle<long []>
{
    public Day6() : base(6, 2021, "Lanternfish")
    {}

    protected override async Task<string> SolvePartOne() => GrowFish(this.ParsedInput, 80);

    protected override async Task<string> SolvePartTwo() => GrowFish(this.ParsedInput, 256);

    private static string GrowFish(long [] dayCount, int days) => (1..days).Aggregate(
        dayCount,
        (lastCount, day) => new []
        {
            lastCount[1],
            lastCount[2],
            lastCount[3],
            lastCount[4],
            lastCount[5],
            lastCount[6],
            lastCount[7] + lastCount[0],
            lastCount[8],
            lastCount[0],
        },
        finalCount => finalCount.Sum().ToString()
    );

    protected override long [] ParseInput() => (..8).Select(i => this.Input.LongCount(c => c == (char) (i + '0'))).ToArray();
}