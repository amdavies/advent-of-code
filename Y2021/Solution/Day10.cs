namespace AdventOfCode.Y2021.Solution;

using ParsedInputType = IEnumerable<string>;

public class Day10 : APuzzle<ParsedInputType>
{
    public Day10() : base(10, 2021, "Syntax Scoring")
    {}

    private readonly Dictionary<char, char> bracketPairs = new(){
        { '(', ')' },
        { '[', ']' },
        { '{', '}' },
        { '<', '>' },
    };

    protected override async Task<string> SolvePartOne()
    {
        Dictionary<char, int> scoreLookup = new() {
            { ')', 3 },
            { ']', 57 },
            { '}', 1197 },
            { '>', 25137 },
        };

        int score = 0;
        foreach (string line in this.ParsedInput) {
            Stack<char> open = new();
            foreach (char c in line) {
                if (this.bracketPairs.ContainsKey(c)) {
                    open.Push(this.bracketPairs[c]);
                    continue;
                }

                if (open.Peek() != c) {
                    score += scoreLookup[c];
                    break;
                }

                open.Pop();
            }
        }

        return score.ToString();
    }

    protected override async Task<string> SolvePartTwo()
    {
        Dictionary<char, long> scoreLookup = new() {
            { ')', 1 },
            { ']', 2 },
            { '}', 3 },
            { '>', 4 },
        };

        List<long> scores = new();
        foreach (string line in this.ParsedInput) {
            Stack<char> open = new();
            foreach (char c in line) {
                if (this.bracketPairs.ContainsKey(c)) {
                    open.Push(this.bracketPairs[c]);
                    continue;
                }

                // Invalid line, skip
                if (open.Peek() != c) {
                    open.Clear();
                    break;
                }

                open.Pop();
            }

            // Score is based on remaining open chunks
            if (open.Count > 0) {
                scores.Add(open.Aggregate(0L, (lineScore, symbol) => lineScore * 5 + scoreLookup[symbol]));
            }
        }

        // Conveniently always an odd number of scores, so we can skip "int half" of them and take the middle
        return scores.OrderByDescending(s => s).Skip(scores.Count / 2).First().ToString();
    }

    protected override ParsedInputType ParseInput()
    {
        return this.Input.Split('\n');
    }
}