namespace AdventOfCode.Y2021.Solution;

using ParsedInputType = IEnumerable<int>;

public class Day7 : APuzzle<ParsedInputType>
{
    public Day7() : base(7, 2021, "The Treachery of Whales")
    {}

    protected override async Task<string> SolvePartOne()
    {
        var target = this.ParsedInput.ToList().Median();

        return this.ParsedInput.Sum(c => (c - target).Abs()).ToString();
    }

    protected override async Task<string> SolvePartTwo()
    {
        // True answer is the mean, rounding puts it either side
        var mean = this.ParsedInput.Average();

        return new[] {
            (int) Math.Floor(mean), (int) Math.Ceiling(mean)
        }.Min(target => this.ParsedInput.Sum(c => (c - target).Abs().Triangle())).ToString();
    }

    protected override ParsedInputType ParseInput() => this.Input.ToIntArray(",");
}