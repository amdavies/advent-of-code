using AdventOfCode.Grid;

namespace AdventOfCode.Y2021.Solution;

using ParsedInputType = Dictionary<Position, int>;

public class Day11 : APuzzle<ParsedInputType>
{
    public Day11() : base(11, 2021, "Dumbo Octopus")
    {}

    protected override async Task<string> SolvePartOne()
    {
        int flashCount = 0;

        // Clone grid to avoid pollution
        var grid = this.ParsedInput.ToDictionary(kvp => kvp.Key, kvp => kvp.Value);

        (1..100).Each(step =>
        {
            // Increase all
            grid.ForEach(s => grid[s.Key]++);

            // Increase based on "flashes"
            HashSet<Position> flashed = new();

            while (grid.Any(s => !flashed.Contains(s.Key) && s.Value > 9)) {
                grid.Where(s => !flashed.Contains(s.Key) && s.Value > 9).ForEach(s =>
                {
                    flashed.Add(s.Key);

                    GetNeighbours(s.Key).ForEach(n => grid[n]++);
                });
            }

            // Count flashes and set to 0
            flashCount += grid.Count(s => s.Value > 9);
            grid.Where(s => s.Value > 9).ForEach(s => grid[s.Key] = 0);
        });

        return flashCount.ToString();
    }

    protected override async Task<string> SolvePartTwo()
    {
        // Clone grid to avoid pollution
        var grid = this.ParsedInput.ToDictionary(kvp => kvp.Key, kvp => kvp.Value);

        var step = 1;
        while (true) {
            // Increase all
            grid.ForEach(s => grid[s.Key]++);

            // Increase based on "flashes"
            HashSet<Position> flashed = new();

            while (grid.Any(s => !flashed.Contains(s.Key) && s.Value > 9)) {
                grid.Where(s => !flashed.Contains(s.Key) && s.Value > 9).ForEach(s =>
                {
                    flashed.Add(s.Key);

                    GetNeighbours(s.Key).ForEach(n => grid[n]++);
                });
            }

            // Count flashes and set to 0
            if (grid.Count(s => s.Value > 9) == grid.Count) break;

            grid.Where(s => s.Value > 9).ForEach(s => grid[s.Key] = 0);
            step++;
        };

        return step.ToString();
    }

    private static IEnumerable<Position> GetNeighbours(Position source)
    {
        for (var dX = -1; dX <= 1; dX++) {
            for (var dY = -1; dY <= 1; dY++) {
                var target = source + (dX, dY);
                if (target.X is >= 0 and < 10 && target.Y is >= 0 and < 10) yield return target;
            }
        }
    }

    protected override ParsedInputType ParseInput()
    {
        Dictionary<Position, int> grid = new();

        this.Input.Split('\n').ForEach((row, y) =>
        {
            row.ToIntArray().ForEach((energy, x) => grid[new(x, y)] = energy);
        });

        return grid;
    }
}
