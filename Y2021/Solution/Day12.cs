using System.Collections;

namespace AdventOfCode.Y2021.Solution;

using ParsedInputType = Dictionary<string, string[]>;

public class Day12 : APuzzle<ParsedInputType>
{
    public Day12() : base(12, 2021, "Passage Pathing")
    {}

    protected override async Task<string> SolvePartOne()
    {
        // Traversal time!
        List<string> completePaths = new();
        Stack<(string node, List<string> path)> remaining = new();
        remaining.Push(("start", new(){"start"}));
        while (remaining.Any()) {
            var currentPath = remaining.Pop();

            foreach (var connection in this.ParsedInput[currentPath.node]) {
                // Can only visit small caves (lowercase identifiers) once
                if (connection.ToLower() == connection && currentPath.path.Contains(connection)) continue;

                var newPath = currentPath.path.ToList();
                newPath.Add(connection);

                if (connection == "end") {
                    completePaths.Add(newPath.Implode(","));
                    continue;
                }

                remaining.Push((connection, newPath));
            }
        }

        return completePaths.Count.ToString();
    }

    protected override async Task<string> SolvePartTwo()
    {
        List<string> completePaths = new();
        Stack<(string node, List<string> path, bool hasDoubled)> remaining = new();
        remaining.Push(("start", new(){"start"}, false));
        while (remaining.Any()) {
            var currentPath = remaining.Pop();

            foreach (var connection in this.ParsedInput[currentPath.node]) {
                // Can only visit small caves (lowercase identifiers) once. Or one of them twice
                if (connection == "start") continue;

                var hasDoubled = currentPath.hasDoubled;
                if (connection.ToLower() == connection && currentPath.path.Contains(connection)) {
                    if (hasDoubled) continue;
                    hasDoubled = true;
                }

                var newPath = MoreEnumerable.Append(currentPath.path, connection).ToList();

                if (connection == "end") {
                    completePaths.Add(newPath.Implode(","));
                    continue;
                }

                remaining.Push((connection, newPath, hasDoubled));
            }
        }

        return completePaths.Count.ToString();
    }

    protected override ParsedInputType ParseInput()
    {
        var connections = this.Input.Split('\n').Select(l => l.Split('-').ToTuple2()).ToArray();

        return connections.SelectMany(c => new[] { c.Item1, c.Item2 }).Distinct().ToDictionary(n => n,
            n =>
            {
                return connections.Where(c => c.Item1 == n)
                    .Select(c => c.Item2).Union(connections.Where(c => c.Item2 == n)
                        .Select(c => c.Item1)).ToArray();
            });
    }
}
