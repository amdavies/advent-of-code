namespace AdventOfCode.Y2021.Solution;

using ParsedInputType = IEnumerable<(Direction direction, int size)>;

public class Day2 : APuzzle<ParsedInputType>
{
    public Day2() : base(2, 2021, "Dive!")
    {}

    protected override async Task<string> SolvePartOne() => this.ParsedInput.Aggregate((h: 0, depth: 0),
        (pos, command) => command.direction switch {
            Direction.Right => (pos.h + command.size, pos.depth),
            Direction.Up => (pos.h, pos.depth - command.size),
            Direction.Down => (pos.h, pos.depth + command.size),
            _ => pos,
        },
        pos => pos.depth * pos.h).ToString();

    protected override async Task<string> SolvePartTwo() => this.ParsedInput.Aggregate((h: 0, depth: 0, aim: 0),
        (pos, command) => command.direction switch {
            Direction.Right => (pos.h + command.size, pos.depth + (command.size * pos.aim), pos.aim),
            Direction.Up => (pos.h, pos.depth, pos.aim - command.size),
            Direction.Down => (pos.h, pos.depth, pos.aim + command.size),
            _ => pos,
        },
        pos => pos.depth * pos.h).ToString();

    protected override ParsedInputType ParseInput() => this.Input.ToCommandList(c => c switch
        {
            "up" => Direction.Up,
            "down" => Direction.Down,
            "forward" => Direction.Right,
            _ => throw new InvalidDataException()
        },
        int.Parse);
}