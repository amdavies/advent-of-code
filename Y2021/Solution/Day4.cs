using AdventOfCode.Grid;

namespace AdventOfCode.Y2021.Solution;

public record Day4InputData(IEnumerable<int> Draw, IEnumerable<BingoBoard> Boards);

public class Day4 : APuzzle<Day4InputData>
{
    public Day4() : base(4, 2021, "Giant Squid")
    {}

    protected override async Task<string> SolvePartOne()
    {
        foreach (var drawnNumbers in this.ParsedInput.Draw.EnumerateSlice()) {
            foreach (BingoBoard board in this.ParsedInput.Boards.Where(b => b.IsWinner(drawnNumbers))) {
                return (drawnNumbers.Last() * board.Unmarked(drawnNumbers)).ToString();
            }
        }

        return "Nothing";
    }

    protected override async Task<string> SolvePartTwo()
    {
        int boardCount = this.ParsedInput.Boards.Count();
        List<int> winners = new ();
        foreach (var drawnNumbers in this.ParsedInput.Draw.EnumerateSlice()) {
            foreach (BingoBoard board in this.ParsedInput.Boards.Where(b => !winners.Contains(b.Id) && b.IsWinner(drawnNumbers))) {
                winners.Add(board.Id);

                if (winners.Count == boardCount)
                    return (drawnNumbers.Last() * board.Unmarked(drawnNumbers)).ToString();
            }
        }

        return "Nothing";
    }

    protected override Day4InputData ParseInput()
    {
        string[] parts = this.Input.Split("\n\n");

        return new(
            parts[0].Split(',').Select(int.Parse).ToArray(),
            parts.Skip(1).Select((b, j) => new BingoBoard(
                    j,
                    b.Split('\n')
                        .SelectMany(
                            (l, y) => l.Replace("  ", " ")
                                .Trim()
                                .Split(' ')
                                .Select((n, x) => (Position: new Position(x, y), Number: int.Parse(n)))
                        )
                        .ToDictionary(c => c.Position, c => c.Number)
                )
            )
        );
    }
}

public record BingoBoard(int Id, Dictionary<Position, int> Numbers)
{
    public bool IsWinner(IEnumerable<int> drawnNumbers) => (..4).Seq().Any(i =>
        {
            int xCount = this.Numbers.Count(kvp => kvp.Key.X == i && drawnNumbers.Contains(kvp.Value));
            int yCount = this.Numbers.Count(kvp => kvp.Key.Y == i && drawnNumbers.Contains(kvp.Value));

            return xCount is 5 || yCount is 5;
        }
    );

    public int Unmarked(IEnumerable<int> drawnNumbers) => this.Numbers.Where(kvp => !drawnNumbers.Contains(kvp.Value)).Sum(kvp => kvp.Value);
}