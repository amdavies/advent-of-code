namespace AdventOfCode.Y2021.Solution;

using ParsedInputType = IEnumerable<(string[] inputs, string[] outputs)>;

public class Day8 : APuzzle<ParsedInputType>
{
    public Day8() : base(8, 2021, "Seven Segment Search")
    {}

    protected override async Task<string> SolvePartOne() => this.ParsedInput.Sum(
        c => c.outputs.Count(o => o.Length is 2 or 3 or 4 or 7)
    ).ToString();

    protected override async Task<string> SolvePartTwo() => this.ParsedInput.Sum(c => {
            // Calculate digits
            Dictionary<int, string> digits = new();

            // Unique digits
            digits[1] = c.inputs.First(i => i.Length == 2);
            digits[4] = c.inputs.First(i => i.Length == 4);
            digits[7] = c.inputs.First(i => i.Length == 3);
            digits[8] = c.inputs.First(i => i.Length == 7);

            // Segment A is the additional segment between 1 and 7
            char sA = digits[7].Except(digits[1]).First();

            // 6 is the only 6-segment digit that doesn't contain all of 7
            digits[6] = c.inputs.First(o => o.Length == 6 && !digits[7].All(o.Contains));

            // Segment C is all that's missing from 6
            char sC = digits[8].Except(digits[6]).First();

            // Segment F is the other segment in 1
            char sF = digits[1].Except(new[] { sC }).First();

            // 5 digits can be 2, 3, or 5
            // 2: Contains C
            // 3: contains C, F, A
            // 5: Contains F
            var fives = c.inputs.Where(i => i.Length == 5).ToArray();
            digits[3] = fives.First(i => i.Contains(sA) && i.Contains(sC) && i.Contains(sF));
            digits[2] = fives.First(i => i.Contains(sC) && !i.Contains(sF) && !digits.ContainsValue(i));
            digits[5] = fives.First(i => i.Contains(sF) && !i.Contains(sC) && !digits.ContainsValue(i));

            // 6 digits can be 0, 6, or 9
            // 6: Already calculated for C/F
            // 0: Missing D -- Seems common
            // 9: Missing E -- Segment difference between 5 & 6
            char sE = digits[6].Except(digits[5]).First();

            digits[0] = c.inputs.Where(i => i.Length == 6)
                .First(i => i.Contains(sE) && !digits.ContainsValue(i));

            // 9 is all we have left!!!
            digits[9] = c.inputs.First(i => !digits.ContainsValue(i));

            var lookup = digits.Flip();

            return c.outputs.Aggregate("", (str, o) => str + lookup[o], int.Parse);
        }
    ).ToString();

    protected override ParsedInputType ParseInput() => this.Input.Split('\n')
        .Select(l => l.Split(" | ")
            .Select(p => p.Split(' ')
                .Select(s => new string(s.OrderBy(c => c).ToArray()))
                .ToArray()
            ).ToTuple2());
}