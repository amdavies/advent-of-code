using AdventOfCode.Grid;

namespace AdventOfCode.Y2021.Solution;

using ParsedInputType = IDictionary<Position, int>;

public class Day15 : APuzzle<ParsedInputType>
{
    public Day15() : base(15, 2021, "Chiton")
    {}

    protected override async Task<string> SolvePartOne()
    {
        var grid = this.ParsedInput;
        Position start = new(0,0);
        Position end = new(this.MaxX,this.MaxY);

        var seen = new HashSet<Position>
        {
            start,
        };

        var options = new Heap<(Position position, int steps)>((point, point1) => point.steps - point1.steps);
        options.Insert((start, 0));

        while (options.Count > 0) {
            var (pos, riskSum) = options.Pop();

            if (pos == end) return riskSum.ToString();

            foreach (var choice in GetNeighbours(pos)) {
                if (seen.Contains(choice)) continue;

                options.Insert((choice, riskSum+this.ParsedInput[choice]));
                seen.Add(choice);
            }
        }

        return "0";
    }

    private IEnumerable<Position> GetNeighbours(Position source)
    {
        if (source.X > 0) yield return source - (1, 0);
        if (source.Y > 0) yield return source - (0, 1);
        if (source.X < this.MaxX) yield return source + (1, 0);
        if (source.Y < this.MaxY) yield return source + (0, 1);
    }

    private IEnumerable<Position> GetXNeighbours(Position source)
    {
        if (source.X > 0) yield return source - (1, 0);
        if (source.Y > 0) yield return source - (0, 1);
        if (source.X < this.Max5X) yield return source + (1, 0);
        if (source.Y < this.Max5Y) yield return source + (0, 1);
    }

    protected override async Task<string> SolvePartTwo()
    {
        var grid = this.ParsedInput;
        Position start = new(0,0);
        Position end = new(this.Max5X,this.Max5Y);

        var seen = new HashSet<Position>
        {
            start,
        };

        var options = new Heap<(Position position, int steps)>((point, point1) => point.steps - point1.steps);
        options.Insert((start, 0));

        while (options.Count > 0) {
            var (pos, riskSum) = options.Pop();

            if (pos == end) return riskSum.ToString();

            foreach (var choice in GetXNeighbours(pos)) {
                if (seen.Contains(choice)) continue;

                options.Insert((choice, riskSum+this.ParsedInput[choice]));
                seen.Add(choice);
            }
        }

        return "0";
    }

    protected override ParsedInputType ParseInput()
    {
        Dictionary<Position, int> grid = new();

        var lines = this.Input.Split('\n');

        this.MaxY = lines.Length - 1;
        this.MaxX = lines[0].Length - 1;
        this.Max5Y = (lines.Length * 5) - 1;
        this.Max5X = (lines[0].Length * 5) - 1;

        lines.ForEach((row, y) =>
        {
            row.ToIntArray().ForEach(((risk, x) =>
            {
                Position pos = new(x, y);
                (..4,..4).Each((incX, incY) =>
                {
                    Position delta = new((this.MaxX + 1) * incX, (this.MaxY + 1) * incY);
                    var newRisk = risk + (incX + incY);
                    while (newRisk > 9) newRisk -= 9;
                    grid[pos + delta] = newRisk;
                });
            }));
        });

        return grid;
    }

    public int Max5X { get; set; }

    public int Max5Y { get; set; }

    public int MaxX { get; set; }

    public int MaxY { get; set; }
}
