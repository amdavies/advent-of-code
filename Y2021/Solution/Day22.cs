using System.Numerics;

namespace AdventOfCode.Y2021.Solution;

using ParsedInputType = IEnumerable<((int start, int end) x, (int start, int end) y, (int start, int end) z, string instruction)>;

public class Day22 : APuzzle<ParsedInputType>
{
    public Day22() : base(22, 2021, "No Title")
    {}

    protected override async Task<string> SolvePartOne()
    {
        List<Cube> state = new();

        foreach (var instruction in this.ParsedInput) {
            List<Cube> newState = new();
            Cube cube = new(instruction.x.start, instruction.x.end, instruction.y.start, instruction.y.end, instruction.z.start, instruction.z.end);

            if (cube.x1 < -50 || cube.x2 > 50 || cube.y1 < -50 || cube.y2 > 50 || cube.z1 < -50 || cube.z2 > 50) continue;

            foreach (Cube c in state) {
                newState.AddRange(c.Subtract(cube));
            }
            if (instruction.instruction == "on") newState.Add(cube);

            state = newState;
        }

        return state.Sum(c => c.GetVolume()).ToString();
    }

    protected override async Task<string> SolvePartTwo()
    {
        List<Cube> state = new();

        foreach (var instruction in this.ParsedInput) {
            List<Cube> newState = new();
            Cube cube = new(instruction.x.start, instruction.x.end, instruction.y.start, instruction.y.end, instruction.z.start, instruction.z.end);

            foreach (Cube c in state) {
                newState.AddRange(c.Subtract(cube));
            }
            if (instruction.instruction == "on") newState.Add(cube);

            state = newState;
        }

        return state.Sum(c => c.GetVolume()).ToString();
    }

    protected override ParsedInputType ParseInput()
    {
        return this.Input.Split('\n').Select(l =>
        {
            var parts = l.Split(' ');
            string instruction = parts[0];

            parts = parts[1].Split(',');

            var ranges = parts.Select(p =>
            {
                var range = p[2..].Split("..");
                return (int.Parse(range[0]), int.Parse(range[1]));
            }).ToArray();


            return (ranges[0], ranges[1], ranges[2], instruction);
        });
    }

    private record struct Cube(int x1, int x2, int y1, int y2, int z1, int z2)
    {
        public bool Intersects(Cube b)
        {
            return (x1, b.x1).Max() <= (x2, b.x2).Min() &&
                   (y1, b.y1).Max() <= (y2, b.y2).Min() &&
                   (z1, b.z1).Max() <= (z2, b.z2).Min();
        }

        public Cube GetIntersect(Cube b)
        {
            return new((x1, b.x1).Max(),
                (x2, b.x2).Min(),
                (y1, b.y1).Max(),
                (y2, b.y2).Min(),
                (z1, b.z1).Max(),
                (z2, b.z2).Min());
        }

        public IEnumerable<Cube> Subtract(Cube b)
        {
            if (!this.Intersects(b)) {
                yield return this with {};
                yield break;
            }

            var intersect = this.GetIntersect(b);
            if (intersect.x1 > this.x1) yield return this with { x2 = intersect.x1 - 1 };
            if (intersect.x2 < this.x2) yield return this with { x1 = intersect.x2 + 1 };
            if (intersect.y1 > this.y1) yield return this with { x1 = intersect.x1, x2 = intersect.x2, y2 = intersect.y1 - 1 };
            if (intersect.y2 < this.y2) yield return this with { x1 = intersect.x1, x2 = intersect.x2, y1 = intersect.y2 + 1 };
            if (intersect.z1 > this.z1) yield return intersect with { z1 = this.z1, z2 = intersect.z1 - 1 };
            if (intersect.z2 < this.z2) yield return intersect with { z1 = intersect.z2 + 1, z2 = this.z2 };
        }

        public long GetVolume()
        {
            return (this.x2 - this.x1 + 1L) * (this.y2 - this.y1 + 1L) * (this.z2 - this.z1 + 1L);
        }
    }
}
