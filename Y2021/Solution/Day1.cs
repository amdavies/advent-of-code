namespace AdventOfCode.Y2021.Solution;

using ParsedInputType = IEnumerable<int>;

public class Day1 : APuzzle<ParsedInputType>
{
    public Day1() : base(1, 2021, "Sonar Sweep")
    {}

    protected override async Task<string> SolvePartOne() => this.ParsedInput.Zip(this.ParsedInput.Skip(1)).Count(d => d.First < d.Second).ToString();

    protected override async Task<string> SolvePartTwo() => this.ParsedInput.Zip(this.ParsedInput.Skip(3)).Count(d => d.First < d.Second).ToString();

    protected override ParsedInputType ParseInput() => this.Input.ToIntArray("\n");
}