namespace AdventOfCode.Y2021.Solution;

using ParsedInputType = IEnumerable<string>;

public class Day3 : APuzzle<ParsedInputType>
{
    public Day3() : base(3, 2021, "Binary Diagnostic")
    {
    }

    protected override async Task<string> SolvePartOne()
    {
        var length = this.ParsedInput.First().Length;
        var max = 2.Power(length) - 1;
        int[] counter = new int[length];

        foreach (var line in this.ParsedInput) {
            var digits = line.ToCharArray();

            for (int i = 0; i < digits.Length; i++) {
                counter[i] += digits[i] - '0';
            }
        }

        var target = this.ParsedInput.Count() / 2;
        var gamma = Convert.ToInt32(counter.Select(c => c >= target ? '1' : '0').Implode(""), 2);


        return (gamma * (max - gamma)).ToString();
    }

    protected override async Task<string> SolvePartTwo()
    {
        var allValues = this.ParsedInput.Select(i => i.ToCharArray()).ToArray();

        var remainingValues = allValues.ToArray();
        var pos = 0;

        while (remainingValues.Length > 1) {
            var target = remainingValues.Length / 2f;
            var mostCommon = remainingValues.Count(c => c[pos] == '1') >= target ? '1' : '0';

            remainingValues = remainingValues.Where(c => c[pos] == mostCommon).ToArray();
            pos++;
        }

        var oxGen = Convert.ToInt32(remainingValues[0].Implode(), 2);

        remainingValues = allValues.ToArray();
        pos = 0;

        while (remainingValues.Length > 1) {
            var target = remainingValues.Length / 2f;
            var leastCommon = remainingValues.Count(c => c[pos] == '1') >= target ? '0' : '1';

            remainingValues = remainingValues.Where(c => c[pos] == leastCommon).ToArray();
            pos++;
        }

        var scrub = Convert.ToInt32(remainingValues[0].Implode(), 2);

        return (oxGen * scrub).ToString();
    }

    protected override ParsedInputType ParseInput() => this.Input.Split('\n').ToArray();
}