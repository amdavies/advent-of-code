namespace AdventOfCode.Y2021.Solution;

using ParsedInputType = IEnumerable<string>;

public class Day24 : APuzzle<ParsedInputType>
{
    public Day24() : base(24, 2021, "Arithmetic Logic Unit")
    {}

    protected override async Task<string> SolvePartOne()
    {
        /**
inp w1       // 1..9
mul x 0      NOOP
add x z      NOOP
mod x 26     NOOP
div z 1      NOOP
add x 15     x = 15
eql x w      x = 0
eql x 0      x = 1
mul y 0      NOOP
add y 25     y = 25
mul y x      y = 25
add y 1      y = 26
mul z y      NOOP
mul y 0      y = 0
add y w      y = w1
add y 13     y = w1 + 13
mul y x      y = w1 + 13
add z y      z = w1 + 13



inp w2      // 1..9
mul x 0     x = 0
add x z     x = w1 + 13
mod x 26    x = (w1 + 13) mod 26                // 0..25
div z 1     z = w1 + 13
add x 10    x = 10 + ((w1 + 13) mod 26)         // 10..35
eql x w2    x = 0                               // 10 > w2
eql x 0     x = 1
mul y 0     y = 0
add y 25    y = 25
mul y x     y = 25
add y 1     y = 26
mul z y     z = 26(w1 + 13)
mul y 0     y = 0
add y w2    y = w2
add y 16    y = w2 + 16
mul y x     y = w2 + 16
add z y     z = 26(w1 + 13) + w2 + 16



inp w3      // 1..9
mul x 0     x = 0
add x z     x = 26(w1 + 13) + w2 + 16
mod x 26    x = w2 + 16                         // 0..25
div z 1     NOOP
add x 12    x = 12..37
eql x w3    x = 0                                 // x > 1..9
eql x 0     x = 1
mul y 0     y = 0
add y 25    y = 25
mul y x     y = 25
add y 1     y = 26
mul z y     z = 26(26(w1 + 13) + w2 + 16)
mul y 0     y = 0
add y w3    y = w3
add y 2     y = w3 + 2
mul y x     y = w3 + 2
add z y     z = 26(26(w1 + 13) + w2 + 16) + w3 + 2




inp w4
mul x 0     x = 0
add x z     x = 26(26(w1 + 13) + w2 + 16) + w3 + 2
mod x 26    x = w3 + 2
div z 1     noop
add x 10    x = 10..35
eql x w4    x = 0
eql x 0     x = 1
mul y 0     y = 0
add y 25    y = 25
mul y x
add y 1     y = 26
mul z y     z = 26(26(26(w1 + 13) + w2 + 16) + w3 + 2)
mul y 0     y = 0
add y w4    y = w4
add y 8     y = w4 + 8
mul y x
add z y     z = 26(26(26(w1 + 13) + w2 + 16) + w3 + 2) + w4 + 8




inp w5
mul x 0     x = 0
add x z     x = 26(26(26(w1 + 13) + w2 + 16) + w3 + 2) + w4 + 8
mod x 26    x = 0..25
div z 1
add x 14    x = 14..39
eql x w5    x = 0
eql x 0     x = 1
mul y 0     y = 0
add y 25    y = 25
mul y x
add y 1     y = 26
mul z y     z = 26(26(26(26(w1 + 13) + w2 + 16) + w3 + 2) + w4 + 8)
mul y 0     y = 0
add y w5    y = w5
add y 11    y = w5 + 11
mul y x
add z y     z = 26(26(26(26(w1 + 13) + w2 + 16) + w3 + 2) + w4 + 8) + w5 + 11




inp w6
mul x 0     x = 0
add x z     x = 26(26(26(26(w1 + 13) + w2 + 16) + w3 + 2) + w4 + 8) + w5 + 11
mod x 26    x = w5 + 11
div z 26    z = 26(26(26(w1 + 13) + w2 + 16) + w3 + 2) + w4 + 8       // w5 / 26 < 1 == 0
add x -11   x = 0
eql x w6    x = 1                    // w6 == w5
eql x 0     x = 0
mul y 0     y = 0
add y 25    y = 25
mul y x     y = 0
add y 1     y = 1
mul z y     z = 26(26(26(w1 + 13) + w2 + 16) + w3 + 2) + w4 + 8
mul y 0     y = 0
add y w6    y = w6
add y 6     y = w6 + 6
mul y x     y = 0
add z y     z = 26(26(26(w1 + 13) + w2 + 16) + w3 + 2) + w4 + 8




inp w7
mul x 0     x = 0
add x z     x = 26(26(26(w1 + 13) + w2 + 16) + w3 + 2) + w4 + 8
mod x 26    x = 0..25
div z 1
add x 10    x = 10..35
eql x w7    x = 0
eql x 0     x = 1
mul y 0     y = 0
add y 25    y = 25
mul y x
add y 1     y = 26
mul z y     z = 26(26(26(26(w1 + 13) + w2 + 16) + w3 + 2) + w4 + 8)
mul y 0     y = 0
add y w7    y = w7
add y 12    y = w7 + 12
mul y x     y = w7 + 12
add z y     z = 26(26(26(26(w1 + 13) + w2 + 16) + w3 + 2) + w4 + 8) + w7 + 12




inp w8
mul x 0     x = 0
add x z     x = 26(26(26(26(w1 + 13) + w2 + 16) + w3 + 2) + w4 + 8) + w7 + 12
mod x 26    x = w7 + 12
div z 26    z = 26(26(26(w1 + 13) + w2 + 16) + w3 + 2) + w4 + 8
add x -16   x = w7 - 4
eql x w8    x = 1                           // w8 = w7 - 4
eql x 0     x = 0
mul y 0     y = 0
add y 25    y = 25
mul y x     y = 0
add y 1     y = 1
mul z y     z = 26(26(26(w1 + 13) + w2 + 16) + w3 + 2) + w4 + 8
mul y 0     y = 0
add y w8    y = w8
add y 2     y = w8 + 2
mul y x     y = 0
add z y     z = 26(26(26(w1 + 13) + w2 + 16) + w3 + 2) + w4 + 8




inp w9
mul x 0     x = 0
add x z     x = 26(26(26(w1 + 13) + w2 + 16) + w3 + 2) + w4 + 8
mod x 26    x = w4 + 8
div z 26    z = 26(26(w1 + 13) + w2 + 16) + w3 + 2
add x -9    x = w4 -1
eql x w9    x = 1                           // w9 = w4 -1
eql x 0     x = 0
mul y 0     y = 0
add y 25    y = 25
mul y x     y = 0
add y 1     y = 1
mul z y     z = 26(26(w1 + 13) + w2 + 16) + w3 + 2
mul y 0     y = 0
add y w9    y = w9
add y 2     y = w9 + 2
mul y x     y = 0
add z y     z = 26(26(w1 + 13) + w2 + 16) + w3 + 2




inp w10
mul x 0     x = 0
add x z     x = 26(26(w1 + 13) + w2 + 16) + w3 + 2
mod x 26    x = (w3 + 2) m 26
div z 1
add x 11    x = 11..36
eql x w10   x = 0
eql x 0     x = 1
mul y 0     y = 0
add y 25    y = 25
mul y x
add y 1     y = 26
mul z y     z = 26(26(26(w1 + 13) + w2 + 16) + w3 + 2)
mul y 0     y = 0
add y w10   y = w10
add y 15    y = w10 + 15
mul y x     y = w10 + 15
add z y     z = 26(26(26(w1 + 13) + w2 + 16) + w3 + 2) + w10 + 15




inp w11
mul x 0     x = 0
add x z     x = 26(26(26(w1 + 13) + w2 + 16) + w3 + 2) + w10 + 15
mod x 26    x = w10 + 15
div z 26    z = 26(26(w1 + 13) + w2 + 16) + w3 + 2
add x -8    x = w10 + 7
eql x w11   x = 1                   // w11 == w10 + 7
eql x 0     x = 0
mul y 0     y = 0
add y 25    y = 25
mul y x     y = 0
add y 1     y = 1
mul z y     z = 26(26(w1 + 13) + w2 + 16) + w3 + 2
mul y 0     y = 0
add y w11   y = w11
add y 1     y = w11 + 1
mul y x     y = 0
add z y     z = 26(26(w1 + 13) + w2 + 16) + w3 + 2




inp w12
mul x 0     x = 0
add x z     x = 26(26(w1 + 13) + w2 + 16) + w3 + 2
mod x 26    x = w3 + 2
div z 26    z = 26(w1 + 13) + w2 + 16
add x -8    x = w3 - 6
eql x w12   x = 1                       // w12 == w3 - 6
eql x 0     x = 0
mul y 0     y = 0
add y 25    y = 25
mul y x     y = 0
add y 1     y = 1
mul z y     z = 26(w1 + 13) + w2 + 16
mul y 0     y = 0
add y w12   y = w12
add y 10    y = w12 + 10
mul y x     y = 0
add z y     z = 26(w1 + 13) + w2 + 16




inp w13
mul x 0     x = 0
add x z     x = 26(w1 + 13) + w2 + 16
mod x 26    x = w2 + 16
div z 26    z = w1 + 13
add x -10   x = w2 + 6
eql x w13   x = 1               // w13 == w2 + 6
eql x 0     x = 0
mul y 0     y = 0
add y 25    y = 25
mul y x     y = 0
add y 1     y = 1
mul z y     z = w1 + 13
mul y 0     y = 0
add y w13   y = w13
add y 14    y = w13 + 14
mul y x     y = 0
add z y     z = w1 + 13




inp w14
mul x 0     x = 0
add x z     x = w1 + 13
mod x 26    x = w1 + 13
div z 26    z = 0
add x -9    x = w1 + 4
eql x w14   x = 1               // w14 == w1 + 4
eql x 0     x = 0
mul y 0     y = 0
add y 25    y = 26
mul y x     y = 0
add y 1     y = 1
mul z y     z = 0
mul y 0     y = 0
add y w14   y = w14
add y 10    y = w14 + 10
mul y x     y = 0
add z y     z = 0!!


Constraints:


w1 = w14 - 4            // Max = 5      Min = 1
w2 = w13 - 6            // Max = 3      Min = 1
w3 = w12 + 6            // Max = 9      Min = 7
w4 = w9 + 1             // Max = 9      Min = 2
w5 = w6                 // Max = 9      Min = 1
w6 = w5                 // Max = 9      Min = 1
w7 = w8 + 4             // Max = 9      Min = 5
w8 = w7 - 4             // Max = 5      Min = 1
w9 = w4 - 1             // Max = 8      Min = 1
w10 = w11 - 7           // Max = 2      Min = 1
w11 = w10 + 7           // Max = 9      Min = 8
w12 = w3 - 6            // Max = 3      Min = 1
w13 = w2 + 6            // Max = 9      Min = 7
w14 = w1 + 4            // Max = 9      Min = 5
         */

        return "53999995829399";
    }

    protected override async Task<string> SolvePartTwo()
    {
        return "11721151118175";
    }

    protected override ParsedInputType ParseInput()
    {
        return this.Input.Split('\n');
    }
}
