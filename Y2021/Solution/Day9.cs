using AdventOfCode.Grid;

namespace AdventOfCode.Y2021.Solution;

using ParsedInputType = Dictionary<Position, int>;

public class Day9 : APuzzle<ParsedInputType>
{
    public Day9() : base(9, 2021, "Smoke Basin")
    {}

    protected override async Task<string> SolvePartOne()
    {
        int risk = 0;
        var directions = new[] { Direction.Up, Direction.Down, Direction.Left, Direction.Right };


        this.ParsedInput.ForEach(kvp =>
        {
            var (pos, height) = kvp;

            var neighbours = getNeighbours(pos).Select(p => this.ParsedInput[p]);
            if (neighbours.Any(n => n <= height)) return;

            risk += height + 1;
        });

        return risk.ToString();
    }

    protected override async Task<string> SolvePartTwo()
    {
        List<int> basins = new();
        HashSet<Position> validPositions = Enumerable.ToHashSet(this.ParsedInput.Where(kvp => kvp.Value is not 9).Select(kvp => kvp.Key));
        HashSet<Position> seen = new();

        List<Position> lows = new();
        this.ParsedInput.ForEach(kvp =>
        {
            var (pos, height) = kvp;

            var neighbours = getNeighbours(pos).Select(p => this.ParsedInput[p]);
            if (neighbours.Any(n => n <= height)) return;

            lows.Add(pos);
        });

        int countBasin(Position pos)
        {
            if (seen.Contains(pos)) return 0;

            seen.Add(pos);
            int size = 1;

            foreach (var n in this.Directions.Select(d => pos.AfterMove(d, 1)).Where(p => validPositions.Contains(p))) {
                size += countBasin(n);
            }

            return size;
        }

        lows.ForEach(pos =>
        {
            if (seen.Contains(pos)) return;

            basins.Add(countBasin(pos));
        });

        return basins.OrderByDescending(b => b).Take(3).Aggregate(1, (sum, basin) => sum * basin).ToString();
    }

    private IEnumerable<Direction> Directions = new[] { Direction.Up, Direction.Down, Direction.Left, Direction.Right };

    private IEnumerable<Position> getNeighbours(Position pos) => this.Directions.Select(d => pos.AfterMove(d, 1)).Where(p => this.ParsedInput.ContainsKey(p));

    protected override ParsedInputType ParseInput()
    {
        Dictionary<Position, int> grid = new();

        this.Input.Split('\n').ForEach((row, y) =>
        {
            row.ToIntArray().ForEach(((height, x) => grid[new(x, y)] = height));
        });

        return grid;
    }
}