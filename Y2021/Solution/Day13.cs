using System.Drawing;
using AdventOfCode.Grid;
using Pastel;

namespace AdventOfCode.Y2021.Solution;

public class Day13 : APuzzle<(HashSet<Position> grid, IEnumerable<string> instructions)>
{
    public Day13() : base(13, 2021, "Transparent Origami")
    {}

    protected override async Task<string> SolvePartOne()
    {
        HashSet<Position> grid = MoreEnumerable.ToHashSet(this.ParsedInput.grid);

        var instruction = this.ParsedInput.instructions.First();

        HashSet<Position> output = this.foldGrid(grid, instruction);

        return output.Count().ToString();
    }

    protected override async Task<string> SolvePartTwo()
    {
        HashSet<Position> grid = MoreEnumerable.ToHashSet(this.ParsedInput.grid);

        foreach (var instruction in this.ParsedInput.instructions) {
            grid = foldGrid(grid, instruction);
        }

        string output = "";

        for (int y = 0; y <= grid.Max(p => p.Y); y++) {
            string line = "\n";

            for (int x = 0; x <= grid.Max(p => p.X); x++) {
                line += grid.Contains(new(x, y)) ? " ".PastelBg(Color.Green).Pastel(Color.Green) : ' ';
            }

            output += line;
        }

        return output;
    }

    private HashSet<Position> foldGrid(HashSet<Position> source, string instruction)
    {
        (int X, int Y) mirrorLine;

        if (instruction.Contains("x=")) {
            mirrorLine = (int.Parse(instruction.Split("x=")[1]), int.MaxValue);
        }
        else {
            mirrorLine = (int.MaxValue, int.Parse(instruction.Split("y=")[1]));
        }

        HashSet<Position> nextGrid = new();
        foreach (var position in source) {
            if (position.X < mirrorLine.X && position.Y < mirrorLine.Y) {
                nextGrid.Add(position);
                continue;
            }

            if (position.X > mirrorLine.X) {
                nextGrid.Add(new(position.X - (position.X - mirrorLine.X) * 2, position.Y));
            }

            if (position.Y > mirrorLine.Y) {
                nextGrid.Add(new(position.X, position.Y - (position.Y - mirrorLine.Y) * 2));
            }
        }

        return nextGrid;
    }

    protected override (HashSet<Position>, IEnumerable<string>) ParseInput()
    {
        var puzzleParts = this.Input.Split("\n\n");

        var grid = MoreEnumerable.ToHashSet(puzzleParts[0].Split('\n').Select(s => Position.FromString(s)));

        return (grid, puzzleParts[1].Split('\n').ToArray());
    }
}