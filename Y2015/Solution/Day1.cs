﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdventOfCode.Y2015.Solution
{
    using ParsedType = IEnumerable<char>;

    public class Day1 : APuzzle
    {
        /// <summary>
        /// Parsed input stream
        /// </summary>
        private ParsedType ParsedInput => this.parsedInputLoader.Value;

        /// <summary>
        /// Lazy loader for parsed input
        /// </summary>
        private readonly Lazy<ParsedType> parsedInputLoader;

        public Day1() : base(1, 2015, "Not Quite Lisp")
        {
            this.parsedInputLoader = new Lazy<ParsedType>(this.ParseInput);
        }

        /// <summary>
        /// Parse the input string to be more useful
        /// </summary>
        private ParsedType ParseInput()
        {
            return this.Input.ToCharArray();
        }

        /// <inheritdoc/>
        protected override async Task<string> SolvePartOne()
        {
            return this.ParsedInput.Sum(c => c == '(' ? 1 : -1).ToString();
        }

        /// <inheritdoc/>
        protected override async Task<string> SolvePartTwo()
        {
            int count = 1;
            int floor = 0;
            foreach (char c in this.ParsedInput) {
                floor += c == '(' ? 1 : -1;

                if (floor < 0) {
                    return count.ToString();
                }

                count++;
            }

            return "Failure: We didn't reach the basement";
        }
    }
}
