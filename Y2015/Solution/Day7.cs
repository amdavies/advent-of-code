namespace AdventOfCode.Y2015.Solution;

using ParsedInputType = IEnumerable<(string command, string output)>;

public class Day7 : APuzzle<ParsedInputType>
{
    public Day7() : base(7, 2015, "Some Assembly Required")
    {}

    private static string[] operations = new[] {"AND", "OR", "LSHIFT", "RSHIFT", "NOT"};

    protected override async Task<string> SolvePartOne()
    {
        Dictionary<string, ushort> wires = new();
        Stack<(string command, string output)> wiresToProcess = new();

        wiresToProcess.Push(this.ParsedInput.Single(instruction => instruction.output == "a"));

        while (wiresToProcess.Count > 0) {
            var instruction = wiresToProcess.Pop();

            // Numbers can be set to wires
            if (ushort.TryParse(instruction.command, out var asNum)) {
                wires[instruction.output] = asNum;
                continue;
            }

            // Work out what wires we need to process
            var requiredWires = instruction.command.Split(' ').Where(s => !operations.Contains(s) && !wires.ContainsKey(s)).ToArray();

            // If we don't need any new wires, process the instruction
            if (!requiredWires.Any()) {
                this.ProcessWire(instruction, wires);
                continue;
            }

            wiresToProcess.Push(instruction);
            foreach (var wire in requiredWires) {
                // Handle raw values
                if (ushort.TryParse(wire, out var number)) {
                    wires[wire] = number;
                    continue;
                }
                wiresToProcess.Push(this.ParsedInput.Single(i => i.output == wire));
            }
        }


        return wires["a"].ToString();
    }

    protected void ProcessWire((string command, string output) instruction, Dictionary<string, ushort> wires)
    {
        var (command, output) = instruction;

        if (command.Contains(" AND ")) {
            var parts = command.Split(" AND ");
            wires[output] = (ushort) (wires.GetValueOrDefault(parts[0]) & wires.GetValueOrDefault(parts[1]));
        } else if (command.Contains(" OR ")) {
            var parts = command.Split(" OR ");
            wires[output] = (ushort) (wires.GetValueOrDefault(parts[0]) | wires.GetValueOrDefault(parts[1]));
        } else if (command.Contains(" LSHIFT ")) {
            var parts = command.Split(" LSHIFT ");
            wires[output] = (ushort) (wires.GetValueOrDefault(parts[0]) << int.Parse(parts[1]));
        } else if (command.Contains(" RSHIFT ")) {
            var parts = command.Split(" RSHIFT ");
            wires[output] = (ushort) (wires.GetValueOrDefault(parts[0]) >> int.Parse(parts[1]));
        } else if (command.Contains("NOT ")) {
            wires[output] = (ushort) ~wires.GetValueOrDefault(command.Replace("NOT ", ""));
        } else {
            // Huge assumption this is just a number
            if (ushort.TryParse(command, out var asNum)) {
                wires[output] = asNum;
            }else {
                wires[output] = wires.GetValueOrDefault(command);
            }
        }
    }

    protected override async Task<string> SolvePartTwo()
    {
        Dictionary<string, ushort> wires = new() {
            {"b", ushort.Parse(this.Part1)}
        };
        Stack<(string command, string output)> wiresToProcess = new();

        wiresToProcess.Push(this.ParsedInput.Single(instruction => instruction.output == "a"));

        while (wiresToProcess.Count > 0) {
            var instruction = wiresToProcess.Pop();

            // Wire b is forcefully set to Part1
            if (instruction.output == "b") continue;

            // Numbers can be set to wires
            if (ushort.TryParse(instruction.command, out var asNum)) {
                wires[instruction.output] = asNum;
                continue;
            }

            // Work out what wires we need to process
            var requiredWires = instruction.command.Split(' ').Where(s => !operations.Contains(s) && !wires.ContainsKey(s)).ToArray();

            // If we don't need any new wires, process the instruction
            if (!requiredWires.Any()) {
                this.ProcessWire(instruction, wires);
                continue;
            }

            wiresToProcess.Push(instruction);
            foreach (var wire in requiredWires) {
                // Handle raw values
                if (ushort.TryParse(wire, out var number)) {
                    wires[wire] = number;
                    continue;
                }
                wiresToProcess.Push(this.ParsedInput.Single(i => i.output == wire));
            }
        }


        return wires["a"].ToString();
    }

    protected override ParsedInputType ParseInput()
    {
        return this.Input.Split('\n')
            .Select(r => r.Split(" -> "))
            .Select(l => (l[0], l[1]))
            .ToArray();
    }
}