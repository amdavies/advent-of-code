﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace AdventOfCode.Y2015.Solution
{
    public class Day4 : APuzzle
    {
        public Day4() : base(4, 2015, "This is a bitcoin miner right?")
        {
        }

        /// <inheritdoc/>
        protected override async Task<string> SolvePartOne()
        {
            return this.FindOffset(1, "00000").ToString();
        }

        /// <inheritdoc/>
        protected override async Task<string> SolvePartTwo()
        {
            return this.FindOffset(int.Parse(this.Part1), "000000").ToString();
        }

        private int FindOffset(int start, string target)
        {
            using var hasher = MD5.Create();
            int offset = start - 1;

            while (true) {
                string hash = BitConverter.ToString(hasher.ComputeHash(Encoding.UTF8.GetBytes(this.Input + (++offset).ToString())));

                if (hash.StartsWith(target)) {
                    return offset;
                }
            }
        }
    }
}
