﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AdventOfCode.Y2015.Solution
{
    using ParsedType = IEnumerable<string>;

    public class Day5 : APuzzle
    {
        /// <summary>
        /// Parsed input stream
        /// </summary>
        private ParsedType ParsedInput => this.parsedInputLoader.Value;

        /// <summary>
        /// Lazy loader for parsed input
        /// </summary>
        private readonly Lazy<ParsedType> parsedInputLoader;

        public Day5() : base(5, 2015, "TITLE")
        {
            this.parsedInputLoader = new Lazy<ParsedType>(this.ParseInput);
        }

        /// <summary>
        /// Parse the input string to be more useful
        /// </summary>
        private ParsedType ParseInput()
        {
            return this.Input.Split('\n');
        }

        /// <inheritdoc/>
        protected override async Task<string> SolvePartOne()
        {
            string[] badString = {"ab", "cd", "pq", "xy"};

            Func<string, bool> hasDouble = delegate(string s)
            {
                for (int i = 0; i < s.Length - 1; i++) {
                    if (s[i] == s[i + 1]) {
                        return true;
                    }
                }

                return false;
            };

            return this.ParsedInput.Count(s => !badString.Any(s.Contains) && s.Count(c => c is 'a' or 'e' or 'i' or 'o' or 'u') >= 3 && hasDouble(s)).ToString();
        }

        /// <inheritdoc/>
        protected override async Task<string> SolvePartTwo()
        {
            Func<string, bool> hasRepeatedPair = delegate(string s)
            {
                Dictionary<string, int> pairs = new();
                for (int i = 0; i < s.Length - 1; i++) {
                    string pair = "" + s[i] + s[i + 1];
                    if (pairs.ContainsKey(pair)) {
                        if (pairs[pair] != i - 1) {
                            return true;
                        }

                        continue;
                    }

                    pairs[pair] = i;
                }

                return false;
            };

            Func<string, bool> hasSplitDouble = delegate(string s)
            {
                for (int i = 0; i < s.Length - 2; i++) {
                    if (s[i] == s[i + 2]) {
                        return true;
                    }
                }

                return false;
            };

            return this.ParsedInput.Count(s => hasSplitDouble(s) && hasRepeatedPair(s)).ToString();
        }
    }
}
