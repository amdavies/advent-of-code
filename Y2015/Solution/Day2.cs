﻿namespace AdventOfCode.Y2015.Solution
{
    using ParsedType = IEnumerable<(int length, int width, int height)>;

    public class Day2 : APuzzle
    {
        /// <summary>
        /// Parsed input stream
        /// </summary>
        private ParsedType ParsedInput => this.parsedInputLoader.Value;

        /// <summary>
        /// Lazy loader for parsed input
        /// </summary>
        private readonly Lazy<ParsedType> parsedInputLoader;

        public Day2() : base(2, 2015, "I Was Told There Would Be No Math")
        {
            this.parsedInputLoader = new Lazy<ParsedType>(this.ParseInput);
        }

        /// <summary>
        ///  Parse the input string to be more useful
        /// </summary>
        private ParsedType ParseInput()
        {
            return this.Input.Split('\n').Select(s =>
            {
                var parts = s.Split('x').Select(int.Parse).ToArray();

                return (length: parts[0], width: parts[1], height: parts[2]);
            });
        }

        /// <inheritdoc/>
        protected override async Task<string> SolvePartOne()
        {
            return this.ParsedInput.Sum(box =>
            {
                var sides = new[]
                {
                    box.length * box.width,
                    box.width * box.height,
                    box.height * box.length,
                };
                return sides.Sum(s => 2 * s) + sides.Min();
            }).ToString();
        }

        /// <inheritdoc/>
        protected override async Task<string> SolvePartTwo()
        {
            return this.ParsedInput.Sum(box =>
            {
                var sides = new[]
                {
                    box.length,
                    box.width,
                    box.height,
                };
                return sides.OrderBy(s => s).Take(2).Sum(s => 2 * s) + sides.Aggregate(1, (a, b) => a * b);
            }).ToString();
        }
    }
}
