﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AdventOfCode.Grid;

namespace AdventOfCode.Y2015.Solution
{
    using ParsedType = IEnumerable<char>;

    public class Day3 : APuzzle
    {
        /// <summary>
        /// Parsed input stream
        /// </summary>
        private ParsedType ParsedInput => this.parsedInputLoader.Value;

        /// <summary>
        /// Lazy loader for parsed input
        /// </summary>
        private readonly Lazy<ParsedType> parsedInputLoader;

        public Day3() : base(3, 2015, "Perfectly Spherical Houses in a Vacuum")
        {
            // this.DebugInput = @"^v^v^v^v^v";
            this.parsedInputLoader = new Lazy<ParsedType>(this.ParseInput);
        }

        /// <summary>
        /// Parse the input string to be more useful
        /// </summary>
        private ParsedType ParseInput()
        {
            return this.Input.ToCharArray();
        }

        /// <inheritdoc/>
        protected override async Task<string> SolvePartOne()
        {
            var location = new Position(0, 0);
            var visited = new HashSet<Position>
            {
                location,
            };

            foreach (char direction in this.ParsedInput) {
                location = location.AfterMove(direction, 1);
                visited.Add(location);
            }

            return visited.Count.ToString();
        }

        /// <inheritdoc/>
        protected override async Task<string> SolvePartTwo()
        {
            Position santaLocation = new(0, 0);
            Position roboLocation = new(0, 0);
            HashSet<Position> visited = new()
            {
                roboLocation,
            };

            int step = 1;
            foreach (char direction in this.ParsedInput) {
                if (step++ % 2 == 1) {
                    santaLocation = santaLocation.AfterMove(direction, 1);
                    visited.Add(santaLocation);
                } else {
                    roboLocation = roboLocation.AfterMove(direction, 1);
                    visited.Add(roboLocation);
                }
            }

            return visited.Count.ToString();
        }
    }
}
