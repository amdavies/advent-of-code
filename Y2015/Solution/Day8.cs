using System.Text.RegularExpressions;

namespace AdventOfCode.Y2015.Solution;

using ParsedInputType = IEnumerable<string>;

public class Day8 : APuzzle<ParsedInputType>
{
    public Day8() : base(8, 2015, "Matchsticks")
    {}

    protected override async Task<string> SolvePartOne()
    {
        Regex escapeRegex = new(@"(\\\\|\\""|\\x[0-9A-Fa-f]{1,2})");

        return this.ParsedInput.Sum(l =>
        {
            var inMem = escapeRegex.Replace(l.Trim('"'), ".");
            return l.Length - inMem.Length;
        }).ToString();
    }

    protected override async Task<string> SolvePartTwo()
    {
        Regex escapeRegex = new(@"(\\|"")");

        return this.ParsedInput.Sum(l =>
        {
            var escapePoints = escapeRegex.Replace(l, "✖");

            return escapePoints.Count(c => c == '✖') + 2;
        }).ToString();
    }

    protected override ParsedInputType ParseInput()
    {
        return this.Input.Split('\n').ToArray();
    }
}
