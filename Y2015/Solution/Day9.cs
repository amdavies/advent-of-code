namespace AdventOfCode.Y2015.Solution;

using ParsedInputType = IEnumerable<string>;

public class Day9 : APuzzle<ParsedInputType>
{
    public Day9() : base(9, 2015, "All in a Single Night")
    {}

    protected override async Task<string> SolvePartOne()
    {
        return this.ParsedInput.Count().ToString();
    }

    protected override async Task<string> SolvePartTwo()
    {
        return "Not Implemented";
    }

    protected override ParsedInputType ParseInput()
    {
        return this.Input.Split('\n');
    }
}
