using System.Text.RegularExpressions;
using AdventOfCode.Enumeration;
using MoreLinq;

namespace AdventOfCode.Y2015.Solution;

using ParsedInputType = IEnumerable<(string command, (int x, int y) from, (int x, int y) to)>;

public class Day6 : APuzzle<ParsedInputType>
{
    public Day6() : base(6, 2015, "Probably a Fire Hazard")
    {}

    protected override async Task<string> SolvePartOne()
    {
        bool[,] state = new bool[1000, 1000];

        this.ParsedInput.ForEach(command =>
        {
            (command.from.x..command.to.x, command.from.y..command.to.y).Each((x, y) => {
                switch (command.command) {
                    case "turn on": state[x, y] = true;
                        break;
                    case "turn off": state[x, y] = false;
                        break;
                    case "toggle": state[x, y] = !state[x, y];
                        break;
                }
            });
        });

        return state.Flatten().OfType<bool>().Count(c => c).ToString();
    }

    protected override async Task<string> SolvePartTwo()
    {
        int[,] state = new int[1000, 1000];

        this.ParsedInput.ForEach(command =>
        {
            (command.from.x..command.to.x, command.from.y..command.to.y).Each((x, y) => {
                switch (command.command) {
                    case "turn on": state[x, y]++;
                        break;
                    case "turn off":
                        var newVal = state[x, y] - 1;
                        if (newVal < 0) newVal = 0;
                        state[x, y] = newVal;
                        break;
                    case "toggle": state[x, y]+=2;
                        break;
                }
            });
        });

        return state.Flatten().OfType<int>().Sum().ToString();
    }

    protected override ParsedInputType ParseInput()
    {
        var commandRegex = new Regex(@"(turn on|turn off|toggle) (\d+),(\d+) through (\d+),(\d+)");
        return this.Input.Split('\n').Select(c =>
        {
            var matches = commandRegex.Match(c);

            return (
                matches.Groups[1].Value,
                (int.Parse(matches.Groups[2].Value), int.Parse(matches.Groups[3].Value)),
                (int.Parse(matches.Groups[4].Value), int.Parse(matches.Groups[5].Value))
            );
        });
    }
}