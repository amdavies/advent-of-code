﻿using AdventOfCode.Y2020.Solution;

namespace AdventOfCode.Y2020.Test
{
    public class Day23Test : ADayTest<Day23>
    {
        protected override string GetPart1ExpectedAnswer() => "98742365";

        protected override string GetPart2ExpectedAnswer() => "294320513093";

        protected override string GetInput() => @"792845136";
    }
}
