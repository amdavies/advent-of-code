﻿using AdventOfCode.Y2020.Solution;

namespace AdventOfCode.Y2020.Test
{
    public class Day25Test : ADayTest<Day25>
    {
        protected override string GetPart1ExpectedAnswer() => "11328376";

        protected override string GetPart2ExpectedAnswer() => "You Win!";

        protected override string GetInput() => @"10705932
12301431";
    }
}
