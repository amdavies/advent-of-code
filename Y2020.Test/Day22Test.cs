﻿using AdventOfCode.Y2020.Solution;

namespace AdventOfCode.Y2020.Test
{
    public class Day22Test : ADayTest<Day22>
    {
        protected override string GetPart1ExpectedAnswer() => "35202";

        protected override string GetPart2ExpectedAnswer() => "32317";

        protected override string GetInput() => @"Player 1:
39
15
13
23
12
49
36
44
8
21
28
37
40
42
6
47
2
38
18
31
20
10
16
43
5

Player 2:
29
26
19
35
34
4
41
11
3
50
33
22
48
7
17
32
27
45
46
9
25
30
1
24
14";
    }
}
