﻿using AdventOfCode.Y2020.Solution;

namespace AdventOfCode.Y2020.Test
{
    public class Day15Test : ADayTest<Day15>
    {
        protected override string GetPart1ExpectedAnswer() => "1238";

        protected override string GetPart2ExpectedAnswer() => "3745954";

        protected override string GetInput() => @"9,6,0,10,18,2,1";
    }
}
