﻿using AdventOfCode.Y2020.Solution;

namespace AdventOfCode.Y2020.Test
{
    public class Day13Test : ADayTest<Day13>
    {
        protected override string GetPart1ExpectedAnswer() => "104";

        protected override string GetPart2ExpectedAnswer() => "842186186521918";

        protected override string GetInput() => @"1000186
17,x,x,x,x,x,x,x,x,x,x,37,x,x,x,x,x,907,x,x,x,x,x,x,x,x,x,x,x,19,x,x,x,x,x,x,x,x,x,x,23,x,x,x,x,x,29,x,653,x,x,x,x,x,x,x,x,x,41,x,x,13";
    }
}
