﻿using AdventOfCode.Y2020.Solution;

namespace AdventOfCode.Y2020.Test
{
    public class Day17Test : ADayTest<Day17>
    {
        protected override string GetPart1ExpectedAnswer() => "298";

        protected override string GetPart2ExpectedAnswer() => "1792";

        protected override string GetInput() => @"..#..##.
#.....##
##.#.#.#
..#...#.
.###....
######..
.###..#.
..#..##.";
    }
}
