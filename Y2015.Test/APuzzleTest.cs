﻿using NUnit.Framework;

namespace AdventOfCode.Y2015.Test;

public abstract class APuzzleTest<T> where T : APuzzle, new()
{
    private T Problem = new();

    [SetUp]
    public void Setup()
    {
        this.Problem = new();
    }

    [Test, TestCaseSource("Part1Data")]
    public void TestPart1(string input, string expected)
    {
        this.Problem.TestInput = input;
        Assert.AreEqual(expected, this.Problem.Part1);
    }

    [Test, TestCaseSource("Part2Data")]
    public void TestPart2(string input, string expected)
    {
        this.Problem.TestInput = input;
        Assert.AreEqual(expected, this.Problem.Part2);
    }
}