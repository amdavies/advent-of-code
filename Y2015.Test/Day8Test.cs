using AdventOfCode.Y2015.Solution;

namespace AdventOfCode.Y2015.Test;

public class Day8Test : APuzzleTest<Day8>
{
    public static object[] Part1Data => new object[] {
        new[] { TestData, "12" },
        // new[] { RealData, "345793" },
    };

    public static object[] Part2Data => new object[] {
        new[] { TestData, "19" },
        // new[] { RealData, "1572643095893" },
    };

    private static string TestData => @"""""
""abc""
""aaa\""aaa""
""\x27""";
    private static string RealData => @"";
}