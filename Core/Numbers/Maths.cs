namespace AdventOfCode;

public static class Maths
{
    public static ulong LeastCommonMultiple(this IEnumerable<ulong> enumerable)
    {
        return enumerable.Aggregate(LeastCommonMultiple);
    }

    // public static ulong LeastCommonMultiple(ulong a, ulong b)
    // {
    //     return a / GreatestCommonDivisor(a, b) * b;
    // }

    public static ulong LeastCommonMultiple(this ulong a, ulong b)
    {
        return a / GreatestCommonDivisor(a, b) * b;
    }

    public static ulong GreatestCommonDivisor(this IEnumerable<ulong> enumerable)
    {
        var result = enumerable.ElementAt(0);

        for (int i = 1; i < enumerable.Count(); i++) {
            result = GreatestCommonDivisor(enumerable.ElementAt(i), result);

            if (result == 1) {
                return 1;
            }
        }

        return result;
    }

    public static ulong LeastCommonMultiple(this int a, ulong b) => LeastCommonMultiple((ulong) a, b);
    public static ulong LeastCommonMultiple(this uint a, ulong b) => LeastCommonMultiple((ulong) a, b);

    public static ulong GreatestCommonDivisor(this ulong a, ulong b)
    {
        if (a == 0 || b == 0) {
            throw new ArgumentException("Arguments are expected to be greater than 0");
        }

        while (b != 0) {
            (a, b) = (b, a % b);
        }

        return a;
    }

    public static ulong GreatestCommonDivisor(this int a, ulong b) => GreatestCommonDivisor((ulong) a, b);
    public static ulong GreatestCommonDivisor(this uint a, ulong b) => GreatestCommonDivisor((ulong) a, b);

    public static double Power(this int n, int power) => Math.Pow(n, power);
    public static double Power(this uint n, int power) => Math.Pow(n, power);
    public static double Power(this long n, int power) => Math.Pow(n, power);
    public static double Power(this ulong n, int power) => Math.Pow(n, power);
    public static double Power(this double n, double power) => Math.Pow(n, power);

    public static int Abs(this int n) => Math.Abs(n);

    public static int Sign(this int n) => Math.Sign(n);

    public static int Triangle(this int n) => (n * (n + 1)) / 2;

    public static T Median<T>(this IList<T> list) where T : IComparable<T>
    {
        return list.NthOrderStatistic((list.Count - 1)/2);
    }

    public static T NthOrderStatistic<T>(this IList<T> list, int n, Random? rnd = null) where T : IComparable<T>
    {
        return NthOrderStatistic(list, n, 0, list.Count - 1, rnd);
    }
    private static T NthOrderStatistic<T>(this IList<T> list, int n, int start, int end, Random? rnd) where T : IComparable<T>
    {
        while (true)
        {
            var pivotIndex = list.Partition(start, end, rnd);
            if (pivotIndex == n)
                return list[pivotIndex];

            if (n < pivotIndex)
                end = pivotIndex - 1;
            else
                start = pivotIndex + 1;
        }
    }

    private static int Partition<T>(this IList<T> list, int start, int end, Random? rnd = null) where T : IComparable<T>
    {
        if (rnd != null)
            list.Swap(end, rnd.Next(start, end+1));

        var pivot = list[end];
        var lastLow = start - 1;
        for (var i = start; i < end; i++)
        {
            if (list[i].CompareTo(pivot) <= 0)
                list.Swap(i, ++lastLow);
        }
        list.Swap(end, ++lastLow);
        return lastLow;
    }

    public static void Swap<T>(this IList<T> list, int i, int j)
    {
        if (i==j)   //This check is not required but Partition function may make many calls so its for perf reason
            return;
        (list[i], list[j]) = (list[j], list[i]);
    }

    public static int Min(this (int, int) source) => Math.Min(source.Item1, source.Item2);
    public static int Max(this (int, int) source) => Math.Max(source.Item1, source.Item2);
}
