namespace AdventOfCode;

public static class Time
{
    public static TimeSpan Seconds(this int n) => TimeSpan.FromSeconds(n);
    public static TimeSpan Minutes(this int n) => TimeSpan.FromMinutes(n);
    public static TimeSpan Hours(this int n) => TimeSpan.FromHours(n);
    public static TimeSpan Days(this int n) => TimeSpan.FromDays(n);
}