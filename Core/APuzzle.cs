using System.Diagnostics;
using System.Net;

namespace AdventOfCode
{
    /// <summary>
    ///     Abstract puzzle solution implementation providing common logic
    /// </summary>
    public abstract class APuzzle
    {
        /// <summary>
        ///     The puzzle Year
        /// </summary>
        public readonly int Year;

        /// <summary>
        ///     The puzzle Day
        /// </summary>
        public readonly int Day;

        /// <summary>
        ///     The puzzle Title
        /// </summary>
        public readonly string Title;

        /// <summary>
        ///     A test input to be used instead of the real input
        /// </summary>
        public string? TestInput;

        /// <summary>
        ///     Puzzle input string
        /// </summary>
        protected string Input => string.IsNullOrEmpty(this.TestInput) ?
            this.lazyInput.Value :
            this.TestInput.Replace("\r","");

        /// <summary>
        ///     Solution to Part1
        /// </summary>
        public string Part1 => this.lazyPart1.Value.Result;

        /// <summary>
        ///     Solution to Part2
        /// </summary>
        public string Part2 => this.lazyPart2.Value.Result;

        /// <summary>
        ///     Session cookie for AoC requests
        /// </summary>
        private static Lazy<string> session => new(() => File.ReadAllText("./.AoCSession").Trim('\n'));

        /// <summary>
        ///     Lazy loader for Input string
        /// </summary>
        private readonly Lazy<string> lazyInput;

        /// <summary>
        ///     Lazy loader for Part 1 solution
        /// </summary>
        private readonly Lazy<Task<string>> lazyPart1;

        /// <summary>
        ///     Lazy loader for Part 2 solution
        /// </summary>
        private readonly Lazy<Task<string>> lazyPart2;

        /// <summary>
        ///     Construct a puzzle
        /// </summary>
        /// <param name="day">Puzzle day of year</param>
        /// <param name="year">Puzzle year</param>
        /// <param name="title">Puzzle title to display on wizard</param>
        protected APuzzle(int day, int year, string title) {
            this.Day = day;
            this.Year = year;
            this.Title = title;
            this.lazyInput = new Lazy<string>(this.LoadInput);
            this.lazyPart1 = new Lazy<Task<string>>(() => this.SolvePartOne());
            this.lazyPart2 = new Lazy<Task<string>>(this.SolvePartTwo);
        }

        /// <summary>
        ///     Solve the puzzle, and output the solution(s)
        /// </summary>
        /// <param name="part">The part to solve. 0 = Both parts</param>
        public void Solve(int part = 0)
        {
            string output = $"--- {this.Year} Day {this.Day}: {this.Title} --- \n";

            if (string.IsNullOrEmpty(this.Input)) {
                Console.WriteLine("No input specified");
                return;
            }

            if (part != 2) {
                var timer = new Stopwatch();
                timer.Start();
                var answer = this.Part1;
                timer.Stop();
                output += $"Part 1 Answer: {answer}\n";

                output += $"Took: {timer.ToFriendlyString()}\n";
            }

            if (part != 1) {
                var timer = new Stopwatch();
                timer.Start();
                var answer = this.Part2;
                timer.Stop();
                output += $"Part 2 Answer: {answer}\n";
                output += $"Took: {timer.ToFriendlyString()}\n\n";
            }

            Console.WriteLine(output);
        }

        /// <summary>
        ///     Load the input data, either from file cache or AoC servers
        /// </summary>
        /// <returns>The input string</returns>
        private string LoadInput() {
            string filepath = $"./input/{this.Year}/{this.Day}.in";

            if (!File.Exists(filepath)) {
                this.DownloadInput(filepath);
            }
            return File.ReadAllText(filepath).Trim('\n');
        }

        /// <summary>
        ///     Attempt to download the puzzle input
        /// </summary>
        /// <param name="filepath">The location to store the input string</param>
        private void DownloadInput(string filepath)
        {
            string inputUrl = $"https://adventofcode.com/{this.Year}/day/{this.Day}/input";

            try {
                using var client = new HttpClient();
                client.DefaultRequestHeaders.Add("UserAgent", "Aoc Input Downloader (https://gitlab.com/amdavies/advent-of-code)");
                client.DefaultRequestHeaders.Add("Cookie", $"session={session.Value}");
                File.WriteAllText(filepath, client.GetStringAsync(inputUrl).Result.TrimEnd());
            } catch(HttpRequestException e) {
                var statusCode = e.StatusCode;
                switch (statusCode) {
                    case HttpStatusCode.BadRequest:
                        Console.WriteLine($"Day {this.Day}: Error code 400 when attempting to retrieve puzzle input through the web client. Your session cookie is probably not recognized.");
                        break;
                    case HttpStatusCode.NotFound:
                        Console.WriteLine($"Day {this.Day}: Error code 404 when attempting to retrieve puzzle input through the web client. The puzzle is probably not available yet.");
                        break;
                    default:
                        Console.WriteLine($"Day {this.Day}: Unknown error occured when retrieving puzzle input: {e}");
                        break;
                }

                Environment.Exit(-1);
            }
        }

        /// <summary>
        ///     Solve the first part of the puzzle
        /// </summary>
        /// <returns>The puzzle output string</returns>
        protected abstract Task<string> SolvePartOne();

        /// <summary>
        ///     Solve the second part of the puzzle
        /// </summary>
        /// <returns>The puzzle output string</returns>
        protected abstract Task<string> SolvePartTwo();
    }

    /// <summary>
    ///     Abstract puzzle solution with a generic parameter to set the parsed input type
    /// </summary>
    /// <typeparam name="T">The data type of the parsed input</typeparam>
    public abstract class APuzzle<T> : APuzzle
    {
        /// <summary>
        /// Parsed input stream
        /// </summary>
        protected T ParsedInput => this.parsedInputLoader.Value;

        /// <summary>
        /// Lazy loader for parsed input
        /// </summary>
        private readonly Lazy<T> parsedInputLoader;

        /// <summary>
        ///     Construct the puzzle with input parser configured
        /// </summary>
        /// <inheritdoc cref="APuzzle"/>
        protected APuzzle(int day, int year, string title) : base(day, year, title)
        {
            this.parsedInputLoader = new Lazy<T>(this.ParseInput);
        }

        protected abstract T ParseInput();
    }

    public abstract class APuzzleSolver<T> : APuzzle<T>
    {
        protected bool IsSolved;
        protected string PartOneResult = "Unsolved";
        protected string PartTwoResult = "Unsolved";

        protected APuzzleSolver(int day, int year, string title) : base(day, year, title)
        {
        }

        /// <summary>
        ///     Solve the first part of the puzzle
        /// </summary>
        /// <returns>The puzzle output string</returns>
        protected override async Task<string> SolvePartOne()
        {
            if (!IsSolved)
            {
                await RunSolver();
                IsSolved = true;
            }

            return PartOneResult;
        }

        /// <summary>
        ///     Solve the second part of the puzzle
        /// </summary>
        /// <returns>The puzzle output string</returns>
        protected override async Task<string> SolvePartTwo()
        {
            if (!IsSolved)
            {
                await RunSolver();
                IsSolved = true;
            }

            return PartTwoResult;
        }

        protected abstract Task RunSolver();

    }
}
