﻿namespace AdventOfCode.Grid
{
    /// <summary>
    ///     Representation of a 2D position within an evenly spaced grid
    /// </summary>
    public readonly record struct Position(int X, int Y)
    {
        /// <summary>
        ///     Move a given direction and return the new position
        /// </summary>
        /// <param name="direction">The direction to travel: (U,D,L,R or ^,v,&lt;,>)</param>
        /// <param name="distance">The number of units per movement</param>
        public Position AfterMove(char direction, int distance = 1)
        {
            return direction switch
            {
                'U' => this.AfterMove(Direction.Up, distance),
                'D' => this.AfterMove(Direction.Down, distance),
                'L' => this.AfterMove(Direction.Left, distance),
                'R' => this.AfterMove(Direction.Right, distance),
                '^' => this.AfterMove(Direction.Up, distance),
                'v' => this.AfterMove(Direction.Down, distance),
                '<' => this.AfterMove(Direction.Left, distance),
                '>' => this.AfterMove(Direction.Right, distance),
                'N' => this.AfterMove(Direction.Down, distance),
                'S' => this.AfterMove(Direction.Up, distance),
                'W' => this.AfterMove(Direction.Left, distance),
                'E' => this.AfterMove(Direction.Right, distance),
                _ => this with {},
            };
        }

        /// <summary>
        ///     Move a given direction and return the new position
        /// </summary>
        /// <param name="direction">A Direction enum value indicating direction of travel</param>
        /// <param name="distance">The (whole) number of units per movement</param>
        public Position AfterMove(Direction direction, int distance)
        {
            return direction switch
            {
                Direction.Up => this with {Y = this.Y + distance},
                Direction.Down => this with {Y = this.Y - distance},
                Direction.Left => this with {X = this.X - distance},
                Direction.Right => this with {X = this.X + distance},
                _ => this with {},
            };
        }

        public double DistanceTo(Position target) => (target.X - this.X).Abs() + (target.Y - this.Y).Abs();

        public double DistanceTo((int X, int Y) target) => (target.X - this.X).Abs() + (target.Y - this.Y).Abs();

        public static Position operator -(Position pos) => pos with {
            X = -pos.X,
            Y = -pos.Y,
        };

        public static Position operator +(Position pos, Position delta) => pos with {
            X = pos.X + delta.X,
            Y = pos.Y + delta.Y,
        };

        public static Position operator -(Position pos, Position delta) => pos with {
            X = pos.X - delta.X,
            Y = pos.Y - delta.Y,
        };

        public static Position operator *(Position pos, Position delta) => pos with {
            X = pos.X * delta.X,
            Y = pos.Y * delta.Y,
        };

        public static Position operator -(Position pos, (int X, int Y) delta) => pos with {
            X = pos.X - delta.X,
            Y = pos.Y - delta.Y,
        };

        public static Position operator *(Position pos, (int X, int Y) delta) => pos with {
            X = pos.X * delta.X,
            Y = pos.Y * delta.Y,
        };

        public static Position operator *(Position pos, int delta) => pos with {
            X = pos.X * delta,
            Y = pos.Y * delta,
        };

        public static bool operator >(Position pos, Position cmp) => (pos.X > cmp.X || pos.Y > cmp.Y) && pos.X >= cmp.X && pos.Y >= cmp.Y;

        public static bool operator <(Position pos, Position cmp) => (pos.X < cmp.X || pos.Y < cmp.Y) && pos.X <= cmp.X && pos.Y <= cmp.Y;

        public static bool operator >(Position pos, (int X, int Y) cmp) => (pos.X > cmp.X || pos.Y > cmp.Y) && pos.X >= cmp.X && pos.Y >= cmp.Y;

        public static bool operator <(Position pos, (int X, int Y) cmp) => (pos.X < cmp.X || pos.Y < cmp.Y) && pos.X <= cmp.X && pos.Y <= cmp.Y;

        public static bool operator >=(Position pos, Position cmp) => pos.X >= cmp.X && pos.Y >= cmp.Y;

        public static bool operator <=(Position pos, Position cmp) => pos.X <= cmp.X && pos.Y <= cmp.Y;

        public override string ToString() => $"[{this.X}, {this.Y}]";

        public static implicit operator string(Position pos) => pos.ToString();

        public static Position FromString(string input)
        {
            var parts = input.Split(',');

            return new Position(int.Parse(parts[0]), int.Parse(parts[1]));
        }

        public static implicit operator Position((int X, int Y) pos) => new(pos.X, pos.Y);
        public static implicit operator (int,int)(Position pos) => (pos.X, pos.Y);
    }
}
