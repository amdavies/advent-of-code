using System.Collections.Generic;

namespace AdventOfCode.Grid {
    public class TwoD<T> : Dictionary<Position, T>
    {
        public bool PositionEquals(Position position, T value)
        {
            return this.ContainsKey(position) && this[position]!.Equals(value);
        }
    }
}
