﻿namespace AdventOfCode
{
    public class CircularListNode<T>
    {
        public readonly T Value;
        public CircularListNode<T>? Next;

        public CircularListNode(T value)
        {
            this.Value = value;
        }

        public CircularListNode(T value, CircularListNode<T> next)
        {
            this.Value = value;
            this.Next = next;
        }
    }
}
