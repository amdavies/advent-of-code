namespace AdventOfCode.Enumeration;

public static partial class Enumeration
{
    public static IEnumerable<T> Dequeue<T>(this Queue<T> source, int count)
    {
        for (int i = 0; i < count; i++) yield return source.Dequeue();
    }
}
