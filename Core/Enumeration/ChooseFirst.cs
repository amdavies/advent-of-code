namespace AdventOfCode.Enumeration;

public static partial class Enumeration
{
    public static TResult ChooseFirst<TSource, TResult>(this IEnumerable<TSource> source, Func<TSource, (bool, TResult)> chooser)
    {
        foreach (var item in source) {
            var (valid, value) = chooser(item);

            if (valid) return value;
        }

        throw new InvalidDataException("Unable to choose an element");
    }
    
    public static TResult ChooseFirst<TSource, TResult>(this IEnumerable<TSource> source, Func<TSource, (bool, TResult)> chooser, TResult defaultValue)
    {
        foreach (var item in source) {
            var (valid, value) = chooser(item);

            if (valid) return value;
        }

        return defaultValue;
    }
}