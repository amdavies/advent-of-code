
namespace AdventOfCode.Enumeration;

public static partial class Enumeration
{
    public static Dictionary<TValue, TKey> Flip<TKey, TValue>(this Dictionary<TKey, TValue> source) where TValue : notnull where TKey : notnull
        => source.ToDictionary(a => a.Value, a => a.Key);
    
    public static Dictionary<TKey, TValue> Clone<TKey, TValue>(this Dictionary<TKey, TValue> source) where TValue : notnull where TKey : notnull
        => source.ToDictionary(a => a.Key, a => a.Value);
}