using MoreLinq;

namespace AdventOfCode.Enumeration;

public static partial class Enumeration
{
    public static IEnumerable<int> To(this int start, int end) => MoreEnumerable.Sequence(start, end);

    public static IEnumerable<int> Seq(this Range range) => MoreEnumerable.Sequence(range.Start.Value, range.End.Value);

    public static int Aggregate(this Range range, Func<int, int, int> func)
        => range.Seq().Aggregate(func);

    public static TAccumulate Aggregate<TAccumulate>(this Range range, TAccumulate seed, Func<TAccumulate, int, TAccumulate> func)
        => range.Seq().Aggregate(seed, func);

    public static TResult Aggregate<TAccumulate, TResult>(this Range range, TAccumulate seed, Func<TAccumulate, int, TAccumulate> func, Func<TAccumulate, TResult> resultSelector)
        => range.Seq().Aggregate(seed, func, resultSelector);

    public static IEnumerable<TResult> Select<TResult>(this Range range, Func<int, TResult> func)
        => range.Seq().Select(func);

    public static void Each(this Range range, Action<int> func) => range.Seq().ForEach(func);

    public static void Each(this (Range, Range) ranges, Action<int, int> func)
    {
        ranges.Item1.Seq().ForEach(a => ranges.Item2.Seq().ForEach(b => func(a, b)));
    }

    public static IEnumerable<(int a, int b)> Enumerate(this (Range, Range) ranges)
    {
        for (int a = ranges.Item1.Start.Value; a <= ranges.Item1.End.Value; a++)
            for (int b = ranges.Item2.Start.Value; b <= ranges.Item2.End.Value; b++)
                yield return (a, b);
    }
}