namespace AdventOfCode.Enumeration;

public static partial class Enumeration
{
    public static int CountWhile<TSource>(this IEnumerable<TSource> source, Func<TSource, bool> predicate)
    {
        int count = 0;
        foreach (TSource element in source)
        {
            checked
            {
                if (predicate(element))
                {
                    count++;
                }
                else
                {
                    return count;
                }
            }
        }

        return count;
    }

    public static int CountUntil<TSource>(this IEnumerable<TSource> source, Func<TSource, bool> predicate)
    {
        int count = 0;
        foreach (TSource element in source)
        {
            checked
            {
                count++;
                if (!predicate(element)) return count;
            }
        }

        return count;
    }
}