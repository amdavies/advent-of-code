namespace AdventOfCode.Enumeration;

public static partial class Enumeration
{
    /// <summary>
    /// Enumerates over an enumerable with a slice growing by step, always starting at the beginning
    /// </summary>
    /// <param name="full"></param>
    /// <param name="step"></param>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    public static IEnumerable<T[]> EnumerateSlice<T>(this IEnumerable<T> full, int step = 1)
    {
        T[] enumerable = full as T [] ?? full.ToArray();

        for (int i = step; i < enumerable.Length; i += step) {
            yield return enumerable.Take(i).ToArray();
        }
    }

    /// <summary>
    /// Enumerates over an enumerable with a slice of a fixed size, moving by 1 step at a time
    /// </summary>
    public static IEnumerable<(int index, T[] value)> RollingSlice<T>(this IEnumerable<T> full, int size, int step = 1)
    {
        T[] enumerable = full as T [] ?? full.ToArray();

        for (int i = 0; i <= enumerable.Length - size; i += step) {
            yield return (i, enumerable[i..(i+size)]);
        }
    }
}