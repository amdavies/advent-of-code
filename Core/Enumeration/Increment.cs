namespace AdventOfCode.Enumeration;

public static partial class Enumeration
{
    public static void Increment<TKey>(this Dictionary<TKey, int> source, TKey key) where TKey : notnull
    {
        if (!source.ContainsKey(key))
            source[key] = 0;

        source[key]++;
    }
    
    public static void Increment<TKey>(this Dictionary<TKey, int> source, TKey key, int step) where TKey : notnull
    {
        if (!source.ContainsKey(key))
            source[key] = 0;

        source[key] += step;
    }
    
    public static void Increment<TKey>(this Dictionary<TKey, uint> source, TKey key) where TKey : notnull
    {
        if (!source.ContainsKey(key))
            source[key] = 0;

        source[key]++;
    }
    
    public static void Increment<TKey>(this Dictionary<TKey, uint> source, TKey key, uint step) where TKey : notnull
    {
        if (!source.ContainsKey(key))
            source[key] = 0;

        source[key] += step;
    }
    
    public static void Increment<TKey>(this Dictionary<TKey, long> source, TKey key) where TKey : notnull
    {
        if (!source.ContainsKey(key))
            source[key] = 0;

        source[key]++;
    }
    
    public static void Increment<TKey>(this Dictionary<TKey, long> source, TKey key, long step) where TKey : notnull
    {
        if (!source.ContainsKey(key))
            source[key] = 0;

        source[key] += step;
    }
    
    public static void Increment<TKey>(this Dictionary<TKey, ulong> source, TKey key) where TKey : notnull
    {
        if (!source.ContainsKey(key))
            source[key] = 0;

        source[key]++;
    }
    
    public static void Increment<TKey>(this Dictionary<TKey, ulong> source, TKey key, ulong step) where TKey : notnull
    {
        if (!source.ContainsKey(key))
            source[key] = 0;

        source[key] += step;
    }
}