using System.Runtime.CompilerServices;
using AdventOfCode.Grid;

namespace AdventOfCode;

public static class Utility
{
    public static string NewLine = "\n";

    public static int[] ToIntArray(this string input)
    {
        return input.ToCharArray().Select(c => int.Parse(c.ToString())).ToArray();
    }

    public static int[] ToIntArray(this string input, string delimiter)
    {
        return input.Split(delimiter, StringSplitOptions.RemoveEmptyEntries | StringSplitOptions.TrimEntries).Select(int.Parse).ToArray();
    }

    public static long[] ToLongArray(this string input, string? delimiter = null)
    {
        return delimiter is null ? input.ToCharArray().Select(c => long.Parse(c.ToString())).ToArray() : input.Split(delimiter).Select(long.Parse).ToArray();
    }

    public static IEnumerable<string> SplitBlock(this string input) => input.Split(NewLine + NewLine);

    public static IEnumerable<(string command, string operand)> ToCommandList(this string input)
    {
        return input.Split('\n')
            .Select(r => r.Split(' '))
            .Select(l => (l[0], l[1]))
            .ToArray();
    }

    public static IEnumerable<(string command, T operand)> ToCommandList<T>(this string input, Func<string, T> convertor)
    {
        return input.Split('\n')
            .Select(r => r.Split(' '))
            .Select(l => (l[0], convertor(l[1])))
            .ToArray();
    }

    public static IEnumerable<(Tc command, To operand)> ToCommandList<Tc, To>(this string input, Func<string, Tc> comConvertor, Func<string, To> opConvertor)
    {
        return input.Split('\n')
            .Select(r => r.Split(' '))
            .Select(l => (comConvertor(l[0]), opConvertor(l[1])))
            .ToArray();
    }

    public static string Implode(this IEnumerable<string> input, string delimiter = "")
    {
        return string.Join(delimiter, input);
    }

    public static string Implode(this IEnumerable<char> input)
    {
        return new string(input.ToArray());
    }

    public static string Implode(this IEnumerable<char> input, string delimiter)
    {
        return string.Join(delimiter, input);
    }

    public static int IntFromBin(this string input) => Convert.ToInt32(input, 2);
    public static int IntFromBin(this IEnumerable<char> input) => Convert.ToInt32(input.Implode(), 2);

    public static long LongFromBin(this string input) => Convert.ToInt64(input, 2);
    public static long LongFromBin(this IEnumerable<char> input) => Convert.ToInt64(input.Implode(), 2);

    public static Position ToPosition(this IEnumerable<int> input)
    {
        var arr = input.ToArray();
        return new(arr[0], arr[1]);
    }

    public static Position ToPosition(this IEnumerable<string> input)
    {
        var arr = input.Select(int.Parse).ToArray();
        return new(arr[0], arr[1]);
    }

    public static (T, T) ToTuple2<T>(this T[] input)
    {
        return (input[0], input[1]);
    }

    public static (T, T) ToTuple2<T>(this IEnumerable<T> input)
    {
        var arr = input.ToArray();
        return (arr[0], arr[1]);
    }

    public static (T, T, T) ToTuple3<T>(this IEnumerable<T> input)
    {
        var arr = input.ToArray();
        return (arr[0], arr[1], arr[2]);
    }

    public static Position ToPosition(this string input)
    {
        var parts = input.Split(',').Select(int.Parse).ToArray();
        if (parts.Length != 2) throw new Exception($"Can not represent {input} as an X,Y position");

        return new(parts[0], parts[1]);
    }

    public static bool Contains(this (int Start, int End) first, (int Start, int End) second) => second.Start >= first.Start && second.End <= first.End;
    public static bool Overlaps(this (int Start, int End) first, (int Start, int End) second) =>
        (first.Start >= second.Start && first.Start <= second.End) ||
        (first.End >= second.Start && first.End <= second.End) ||
        (second.Start >= first.Start && second.Start <= first.End);

    public static T[,] To2DArray<T>(this IEnumerable<T[]> source)
    {
        var sourceArray = source.ToArray();
        try
        {
            int FirstDim = sourceArray.Length;
            int SecondDim = sourceArray.GroupBy(row => row.Length).Single().Key; // throws InvalidOperationException if source is not rectangular

            var result = new T[FirstDim, SecondDim];
            for (int i = 0; i < FirstDim; ++i)
            for (int j = 0; j < SecondDim; ++j)
                result[i, j] = sourceArray[i][j];

            return result;
        }
        catch (InvalidOperationException)
        {
            throw new InvalidOperationException("The given jagged array is not rectangular.");
        }
    }

    public static TwoD<char> To2DGrid(this string input)
    {
        return input.Split("\n").Select(row => row.ToCharArray()).To2DGrid();
    }

    public static TwoD<T> To2DGrid<T>(this IEnumerable<T[]> source)
    {
        var sourceArray = source.ToArray();
        try
        {
            int FirstDim = sourceArray.Length;
            int SecondDim = sourceArray.GroupBy(row => row.Length).Single().Key; // throws InvalidOperationException if source is not rectangular

            var result = new TwoD<T>();
            for (int i = 0; i < FirstDim; ++i)
            for (int j = 0; j < SecondDim; ++j)
                result.Add(new(i, j), sourceArray[i][j]);

            return result;
        }
        catch (InvalidOperationException)
        {
            throw new InvalidOperationException("The given jagged array is not rectangular.");
        }
    }

    public static bool IsNumber(this char c) => char.IsNumber(c);


    public static char RotateRight(this char c)
    {
        return c switch
        {
            '^' => '>',
            '>' => 'v',
            'v' => '<',
            '<' => '^',
            _ => throw new Exception($"Unexpected character {c}")
        };
    }

    public static char RotateLeft(this char c)
    {
        return c switch
        {
            '^' => '<',
            '<' => 'v',
            'v' => '>',
            '>' => '^',
            _ => throw new Exception($"Unexpected character {c}")
        };
    }

    public static long Concat(this long a, long b)
    {
        return long.Parse($"{a}{b}");
    }
}
