using System;
using NUnit.Framework;

namespace AdventOfCode.Y2019.Test
{
    public abstract class ADayTest<T> where T : APuzzle, new()
    {
        private T Problem = new();

        [SetUp]
        public void Setup()
        {
            this.Problem.TestInput = this.GetInput();
        }

        protected abstract string GetPart1ExpectedAnswer();
        protected abstract string GetPart2ExpectedAnswer();

        protected abstract string GetInput();

        [Test]
        public void TestPart1()
        {
            Assert.AreEqual(GetPart1ExpectedAnswer(), this.Problem.Part1);
        }

        [Test]
        public void TestPart2()
        {
            Assert.AreEqual(GetPart2ExpectedAnswer(), this.Problem.Part2);
        }
    }
}
