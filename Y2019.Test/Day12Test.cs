﻿using AdventOfCode.Y2019.Solution;

namespace AdventOfCode.Y2019.Test
{
    public class Day12Test : ADayTest<Day12>
    {
        protected override string GetPart1ExpectedAnswer() => "9876";

        protected override string GetPart2ExpectedAnswer() => "307043147758488";

        protected override string GetInput() => @"<x=17, y=5, z=1>
<x=-2, y=-8, z=8>
<x=7, y=-6, z=14>
<x=1, y=-10, z=4>";
    }
}
