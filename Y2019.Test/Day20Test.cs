﻿using AdventOfCode.Y2019.Solution;

namespace AdventOfCode.Y2019.Test
{
    public class Day20Test : ADayTest<Day20>
    {
        protected override string GetPart1ExpectedAnswer() => "602";

        protected override string GetPart2ExpectedAnswer() => "6986";

        protected override string GetInput() => @"                                           I     U       O   M           N     Q       S                                         
                                           X     T       X   C           D     N       A                                         
  #########################################.#####.#######.###.###########.#####.#######.#######################################  
  #.....#.......#.....#.....#.....#...#.#...#...#...#.....#.#...........#.....#.....#.............#.#...#...#...#.......#.....#  
  #####.#######.#####.#####.#.###.###.#.#.###.#.###.#.#.###.###.###.#####.#.#######.#####.#.#######.###.#.###.###.#######.#####  
  #.#.#...#...#.....#.........#...........#.#.#...#.#.#.....#...#.......#.#...#.#.....#.#.#...............#.........#.........#  
  #.#.###.###.###.#####.#######.#####.#.###.#.###.#.#.#########.#.#########.###.#.#####.###.###.#####.#.#####.###.#########.###  
  #.....#.........#.....#...#.#.....#.#.....#.#...#.#.#...#...#.#.....#...#.#.#.........#.....#.....#.#.........#.#...#...#...#  
  #####.#####.#.#.###.#####.#.#.#####.#.#.#.#.###.#.#.###.###.#####.#####.#.#.#####.#######.###.#.#.#######.###.#####.###.#.###  
  #...#.......#.#.......#.......#.#.#.#.#.#.#...#.#.#.......#.#.#.....#.........#.....#.....#.#.#.#.......#.#.........#.......#  
  #.###############.#####.#####.#.#.#######.###.#.#.###.#.###.#.#.#.###.###.#####.#######.#.#.###.#.###.#.#.###.#####.#####.#.#  
  #...#.#.#...#.......#.......#...#.....#...#...#.#.#...#.....#...#...#.#...#.......#...#.#.....#.#...#.#.#.#.#.#.....#.#.#.#.#  
  #.###.#.#.#######.#.#.#.#####.###.###.#.#.#.###.#.#.###.#.###.###########.#####.#####.###.#.#.#.#.#########.###.#####.#.#.###  
  #.#.......#...#...#.#.#...#...#.#.#.....#.#...#...#.#...#.#.......#.#.#.#.#.........#.....#.#.#.#...........#.#...#.#.#.#...#  
  #.###.###.#.#####.###.#.###.###.###.#####.###.#.#.#.#############.#.#.#.#.#######.#.###.#.#####.#.#.#####.#.#.#####.#.#.#.###  
  #.#...#...#.#.#.#...#.#...#.#.....#.#.....#...#.#.#...#.#.#...........#.....#.....#.#...#.....#.#.#.....#.#.....#...#.#.#.#.#  
  #.#######.#.#.#.#.#############.#####.###.#.#######.#.#.#.#.###.#.#####.#########.#####.#################.#######.#.#.#.#.#.#  
  #.....#.......#.#.#...#...#...#...#.#.#...#.....#...#.#.....#.#.#.#.......#.#.#...#.......#.#...#.#.....#.....#.#.#.....#...#  
  ###.#########.#.###.#####.#.###.###.###.#.#.#######.###.###.#.#######.###.#.#.#.#.#.#####.#.#.###.#.#.#####.###.#######.#.###  
  #.#.....#.#...#...#...#.#.#...#...#...#.#.#.....#...#...#.......#...#.#.......#.#.#.#.........#.....#...#.#.#.#...#.........#  
  #.###.###.###.###.#.###.#.###.#.#####.###.#.#.###.#.#########.###.#.#####.#######.###.###.#####.#.#.#####.###.###.#######.###  
  #.........#.#...#.#.#...........#.#.......#.#...#.#.#...#.........#.#.....#.#.......#.#.#.#.#...#.#.#.....#.#...#.#.....#...#  
  ###.#.#####.#.###.#.#####.#####.#.#######.#.###.###.#.#####.#.#.#########.#.#####.#####.#.#.#.#.#######.###.#.###.#.#.###.###  
  #.#.#.#...#.........#.#.#.#.#.#...#.#.....#.#.#.#.......#.#.#.#.#...........#.#.......#.....#.#.#.#...#...#...#.....#...#.#.#  
  #.#.#####.#.###.###.#.#.###.#.#.#.#.#####.#.#.#.###.#.###.#####.#####.#######.###.#######.###.#.#.#.###.#####.###.#.###.#.#.#  
  #...#.#.#.#.#.#.#.....#.........#.#...#...#...#.#...#.#...#.......#...#...#...........#.#.....#.#...#.#.#...#...#.#.#.......#  
  ###.#.#.#.###.#####.#.#####.#.#.###.###.#.#.#.#####.#####.#.#.#.#.#.###.#.#.###.#.#.###.#.#.#####.###.#.###.#.#.#########.###  
  #...#.#...#...#.#.#.#.......#.#.........#.#.#.#...........#.#.#.#.#.#...#.#...#.#.#.#.#.#.#.#.............#.#.#.#.....#.#.#.#  
  ###.#.#.###.###.#.###.#.#.#####.###.#####.#.#######.#############.#.#.###.###.#######.#.#.###.###.#.#######.#.#######.#.#.#.#  
  #.#.....#.#.#.#.#.....#.#.#.#...#.......#.#...#...........#...#...#...#...#...#...#...........#...#.#.#...#.....#...#.#.....#  
  #.#####.#.#.#.#.#########.#.#.#.#.#.###.###.#######.#######.###.###.###.#.###.#.#####.###.#.#########.#.###.#######.#.###.###  
  #...#.....#.#.#.#.#.#...#.#...#.#.#.#.#...#.......#.........#.....#.#...#.#.....#.#.#...#.#.#.#...#.....#.....#.#...#.#.....#  
  #.#######.#.#.#.#.#.#.###.#.#.#.#.###.###.#.#.#####.#.###.#####.#######.###.#.###.#.#.#.#.#.#.###.###.#####.#.#.#.###.#.#####  
  #...#.#...#...........#...#.#.#.#.#.......#.#...#...#.#...#.......#.......#.#...#.....#.#.#...#.......#...#.#.......#.....#.#  
  #.#.#.###.###.###.#############.#########.###.#########.#####.#######.#####.#######.#################.#.#######.#####.###.#.#  
  #.#.....#.#.#.#.#.....#.#.....#.#        C   S         P     R       W     B       A        #...#.#...#...#.#.....#...#...#.#  
  ###.#.###.#.#.#.#######.###.###.#        S   A         P     D       A     E       D        #.###.###.#.###.#.###########.#.#  
  #...#.#...........#...#.....#...#                                                           #.#.......#.........#.#.#.....#.#  
  ###.#####.#####.###.###.###.#####                                                           #.#####.#.#.#.#.#.###.#.###.#.#.#  
  #...#.#...#.........#.....#.#...#                                                           #.....#.#...#.#.#.#.#.......#...#  
  ###.#.###.#.###########.#####.###                                                           #.#.#.#.###.###.#.#.#######.#.#.#  
EY........#.#...#...#.#.......#.#.#                                                           #.#.#.#...#.#.#.#...#.......#.#.#  
  #.###.#.#.#.#.#.#.#.###.#####.#.#                                                           #.#.#####.###.###.#####.#.###.###  
  #.#...#...#.#...#................RB                                                       VN..#.......#.....#.......#.#......CS
  #############.#########.#.#######                                                           #############.#.###.#######.###.#  
  #...#.#...#.....#.#.#...#.#.....#                                                           #.........#...#.#...#.#...#...#.#  
  #.###.###.###.###.#.###.#####.#.#                                                           ###.###.###.#.#######.#.#########  
BE....#...#...#.#.......#.#.#...#.#                                                         QN..#.#...#...#.#...#...#.#.#...#.#  
  #.#####.###.#######.#####.###.#.#                                                           #.#.#.#####.#.#.#####.#.#.#.###.#  
  #.#.#.......#.........#.....#.#.#                                                           #.#.#.#...#.#.#.#.........#.#.#..MR
  #.#.#.#####.#####.#.#.###.###.#.#                                                           #.#.#.#####.#.#.###.#.#####.#.#.#  
  #.....#...#.......#.#.........#..QG                                                       WP....#.......#.......#............VN
  #######.###.###.###.#############                                                           ###.###########.#.#######.#######  
ZM........#.....#...#.#...#.....#.#                                                           #.#...#...#...#.#.#.....#.#.....#  
  #.#.#########.#.#####.#######.#.#                                                           #.#####.#.###.#######.#.###.#.###  
  #.#.......#.#.#.....#.#.....#....ND                                                       UE........#...........#.#.#...#...#  
  ###.#######.#########.###.#.#.###                                                           #.###.#.#.#####.###.#.#.###.#.###  
  #...#...#...#...#.......#.#.....#                                                           #.#.#.#.#...#...#...#.#.....#....GN
  #.#####.###.#.###.###.#####.#.###                                                           #.#.###########.#.###.#####.#.#.#  
  #...................#.......#...#                                                           #.#.......#.#.#.#.......#...#.#.#  
  #####.#####.###.#.#####.#########                                                           ###.#.#.###.#.###.###############  
  #...#.....#.#...#.#...#.#.#.....#                                                         UT....#.#.#.#.#...#.#.#.......#....WA
  #.#.###############.###.#.#.#####                                                           ###.#####.#.#.###.#.#.###.#.#.#.#  
WP..#...#.....#.#.#.....#.#.......#                                                           #.....#.....#.#.....#.#...#...#.#  
  #.#.#.###.###.#.###.#.###.#.#####                                                           #.#.#.###.###.#########.###.#.###  
  #.#.#.....#.....#.#.#.....#.....#                                                           #.#.#.#.......#.#.#.....#...#.#.#  
  #######.#.#.#.###.#.#.#.#.#######                                                           #.#####.###.#.#.#.#####.###.#.#.#  
  #...#.#.#...#.......#.#.#........OX                                                         #.........#.#...........#...#.#.#  
  #.###.###########.###.###.#.###.#                                                           #.#######.#####.###########.###.#  
  #.......#.....#...#...#...#.#...#                                                           #.#.....#.#.#.#.#.......#...#....PP
  #.###.#.###.#.###################                                                           ###.###.###.#.###.#.###.#####.###  
NO..#...#.#...#.#.#.....#...#.....#                                                         ZR..#.#.#...#.....#.#.#.....#.....#  
  ###.#####.###.#.#.#####.###.###.#                                                           #.#.#.#.#####.#####.###.#####.#.#  
  #...#...#...#.....#.#.........#..FK                                                         #.....#.....#.#...#.#...#...#.#.#  
  #.###.#.###.#####.#.###.#.#.#.#.#                                                           #.###.###.###.###.#.###.#.#.###.#  
ZZ......#.......#.#.......#.#.#.#.#                                                           #.#...#.............#.....#.....#  
  #.###.###.#.###.###.#############                                                           ###.#####.###.#################.#  
  #.#...#.#.#.......#.#.#.........#                                                           #.....#.....#.#...............#.#  
  #######.#####.#.#.###.#.###.###.#                                                           #########.#####.###.#######.#####  
RX..#.#.#.....#.#.#.#.#.....#.#....GN                                                         #.#...#...#.....#.....#.#........UE
  #.#.#.#.#.#########.###.#.###.#.#                                                           #.#.#####.###.#.###.###.###.###.#  
  #.....#.#.....#.#.....#.#...#.#.#                                                         ZM....#...#.#...#.#.#.......#...#.#  
  #.#.#.#.#.#####.###.#####.#.#.#.#                                                           #.#.###.###.#.###.#.#.#####.###.#  
  #.#.#...#.................#.#.#.#                                                           #.#.........#...#...#...#.....#.#  
  #.###.#.#####.###.#.#####.#####.#    M       P           I     R     E     N           M    #.###.#.#.###.#######.#####.###.#  
  #.#...#.....#...#.#...#.#...#.#.#    C       G           X     X     Y     O           R    #...#.#.#.#...#.....#.....#...#.#  
  #####.###.###.#.#.#####.#.###.#######.#######.###########.#####.#####.#####.###########.#########.#######.#.#####.#.#.###.###  
  #.#.#...#.#...#.#.#.............#.....#.#.#...#...#.........#...#.......#.......#.#...#.#.......#.#.............#.#.#.#.#...#  
  #.#.#.#.#.###.###.###.###.###.###.#.#.#.#.###.#.#.###.#########.#######.#######.#.###.#.###.#.#####.###.#.###.#########.#####  
  #...#.#.#...#...#...#...#...#.#...#.#...#.#.....#.#...#.....#...#.#.#.#.....#.#.#.#.#.#.....#.#...#...#.#.#.............#...#  
  ###.#.#######.#.#####.###.#.#####.#.###.#.#######.#.###.#.###.#.#.#.#.###.###.#.#.#.#.#.#######.#######.#####.###.#######.###  
  #.......#.....#.#.......#.#...#...#.#.....#...#.#.#.....#...#.#.#...........#.#...#...#...........#.#.#...#.....#...........#  
  #.#.#.#.#.#.#.#.#.#.#.#.###.#.###.#.#.###.###.#.#.#######.#####.###.###.#.###.#.###.#.#.#.###.#.#.#.#.#.###.#.###.###.###.###  
  #.#.#.#.#.#.#.#.#.#.#.#.#...#...#.#.#.#.#.#.#.........#...#.#...#.....#.#...#...#...#...#...#.#.#.#.#...#...#.#.....#.#.#...#  
  ###.#.#####.#######.#.###.###.###.#####.#.#.#.#####.#####.#.###.#####.#.#.#####.#.#.###.#######.#.#.#.#######.#######.#.#####  
  #...#...#...#.#.#...#...#.#.#...#.#.......#...#.#...#.....#.....#.....#.#...#...#.#.#.........#.#.#...#.........#...........#  
  ###.#####.###.#.#.#.#######.#.#########.#.#.###.#.#####.#.#.#.#####.#####.#####.#.#########.#########.#####.#.###.#####.###.#  
  #.....#.....#.....#.......#.....#.......#.#.....#...#...#.#.#.....#.#.#.#.#...#.#.......#...........#.#.#.#.#...#...#.....#.#  
  ###.#.###.#######.###.###.###.###.###.###.#.#.###.###.###.#######.#.#.#.#####.#.#.#.#########.###.#.#.#.#.#####.#.#.#.#.#.#.#  
  #...#...#...#.#.....#.#.....#.#...#.#.#...#.#...#.#.#.#.#.#.#...#.#.....#.#...#.#.#.........#.#.#.#.#.........#.#.#.#.#.#.#.#  
  #.###.#.###.#.#.#.#.#####.#########.#####.#.#######.###.#.#.###.#.#.###.#.###.#.#####.#######.#.#######.#.#########.###.#.#.#  
  #.#...#.#...#...#.#.#.......#...#.#.......#.#.......#...#...#...#.#...#.#.#.......#...#.#.#.#...#.....#.#.#.#...#.....#.#.#.#  
  #.#.#.#.#####.#.#.#####.#####.###.#.#####.#.###.#######.#.#####.#.#####.#.###.#######.#.#.#.###.#.#####.###.#.#####.#.#######  
  #.#.#.#...#...#.#.#.......#.#.#.#.#.#.#.#.#.#...#.#.#...#...#.#.......#.....#...#...........#.#...#.#.....#...#...#.#...#.#.#  
  ###.###.#.#.#########.#####.#.#.#.###.#.#.#.###.#.#.###.#.###.#.###.#.###.###.#####.###.#####.#####.#.#.###.#####.###.###.#.#  
  #.....#.#.#.#.........#.......#.#...#.....#.....#.....#...#.......#.#...#.#.......#...#.......#.....#.#...#...#.....#.#.#...#  
  #.#.###.#.#.#.###.#.#######.#.#.#.#####.#.#.###.###.#.#.#####.#.#####.###.###.#########.###.#.###.#####.###.###.#.#####.#.###  
  #.#...#.#.#.#.#.#.#...#.....#.........#.#.#.#.....#.#.#...#.#.#...#.#.#.#.#.......#.......#.#...#...#.#.........#...........#  
  #.#.###.#######.#.#.#####.#########.#####.#######.#.#.#.###.###.#.#.###.#.#####.#######.#########.###.#.###.#.#.###.#.#.###.#  
  #.#.#.......#...#.#.#.#...#...#.....#.#...#.......#.#.......#...#.#.........#...#...............#.#.#.#.#.#.#.#...#.#.#.#.#.#  
  #.#####.#######.#####.#######.#.###.#.###.#.#######.###########.#.#.###########.#####.###.###.###.#.#.###.###.#.#####.###.###  
  #.#.....#...............#.......#.....#...#.......#...#...#...#.#.#...#.......#.....#...#...#...#.......#...#.#.....#.#...#.#  
  #.#.###.#.#.#.#.#######.#.#.#.#######.###.#######.###.###.#.#.###.###.#####.#.#.#.###.###########.###.###.#####.#.#####.###.#  
  #.#.#...#.#.#.#.#.#.......#.#...#.........#.........#...#.#.#...#.#.#...#.#.#...#.#.......#.........#.........#.#.#...#...#.#  
  #.#.###.###.#####.#.#####.#.#####.###.#.###.#.#######.###.###.#.#.#.###.#.#.###.#####.#.###.#####.#.#.###.#####.#####.#.###.#  
  #.#.#.#.#...#...#...#.....#.#.....#...#.#.#.#...#...#...#.#...#...#.......#...#.....#.#.......#.#.#.#.#.....#.#.............#  
  #####.#.#.#.#.#.###.#.###.#####.#####.###.#.###.#.#.#.###.#.#######.###.#####.#######.#.#.#.###.#.#.###.#####.###.#.#.###.#.#  
  #.......#.#.#.#.....#.#.....#...#.......#.....#.#.#.......#.......#...#.#...........#.#.#.#.....#.#.#...........#.#.#...#.#.#  
  #####################################.#######.#########.#.#######.#.###########.#######.#####################################  
                                       Z       A         R A       F Q           R       P                                       
                                       R       D         B A       K G           D       G";
    }
}
