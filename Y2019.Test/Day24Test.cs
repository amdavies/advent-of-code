﻿using AdventOfCode.Y2019.Solution;

namespace AdventOfCode.Y2019.Test
{
    public class Day24Test : ADayTest<Day24>
    {
        protected override string GetPart1ExpectedAnswer() => "3186366";

        protected override string GetPart2ExpectedAnswer() => "2031";

        protected override string GetInput() => @"###..
#...#
.#.##
##.#.
#.###";
    }
}
