﻿using AdventOfCode.Y2019.Solution;

namespace AdventOfCode.Y2019.Test
{
    public class Day04Test : ADayTest<Day4>
    {
        protected override string GetPart1ExpectedAnswer() => "1764";

        protected override string GetPart2ExpectedAnswer() => "1196";

        protected override string GetInput() => @"152085-670283";
    }
}
