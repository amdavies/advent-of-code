﻿using System.Diagnostics;
using System.Drawing;
using System.Reflection.Metadata;
using Pastel;
using Sharprompt;

[assembly: MetadataUpdateHandler(typeof(AdventOfCode.RunnerUpdater))]

namespace AdventOfCode;

internal static class RunnerUpdater
{
    public static Runner Runner { get; set; } = new Runner();

    static void UpdateApplication(IEnumerable<Type>? updatedTypes)
    {
        Runner.ReloadSolutions();
    }
}

internal class Runner
{
    private static Lazy<string> session => new(() => File.ReadAllText("./.AoCSession").Trim('\n'));
    private static HttpClient client = new ();
    private Dictionary<int, Dictionary<int, APuzzle>> Solutions = new ();

    public void Run(string[] args)
    {
        RunnerUpdater.Runner = this;

        this.ReloadSolutions();
        APuzzle day;
        int year = this.Solutions.Keys.Max();
        int part;

        var orderedSolutions = this.Solutions[year].OrderBy(s => s.Value.Day).ToList();
        if (args.Length == 0) {
            ProblemPrompt(this.Solutions);
        } else if (args[0] == "all") {
            if (args.Length == 2) {
                var selectedYear = int.Parse(args[1]);
                if (selectedYear != year) {
                    orderedSolutions = this.Solutions[selectedYear].OrderBy(s => s.Value.Day).ToList();
                }
            }

            var timer = new Stopwatch();
            timer.Start();
            foreach (var solution in orderedSolutions) {
                solution.Value.Solve();
            }

            timer.Stop();
            Console.WriteLine($"All problems solved in {timer.ToFriendlyString()}");
        } else if (args[0] == "today") {
            var puzzle = orderedSolutions.Last().Value;
            puzzle.Solve();

            if (!args.Contains("--submit")) return;

            if (Prompt.Confirm("Submit answer to part 1?", false)) {
                Console.WriteLine(this.submitAnswer(puzzle.Year, puzzle.Day, 1, puzzle.Part1).Result);
            }

            if (Prompt.Confirm("Submit answer to part 2?", false)) {
                Console.WriteLine(this.submitAnswer(puzzle.Year, puzzle.Day, 2, puzzle.Part2).Result);
            }
        } else {
            foreach (var arg in args) {
                Console.WriteLine(arg);
                part = 0;
                var parts = arg.Split('.');
                day = this.Solutions[int.Parse(parts[0])][int.Parse(parts[1])];

                if (parts.Length > 2) {
                    part = int.Parse(parts[2]);
                }

                day.Solve(part);
            }
        }

        // Prompt.Confirm("Done!", true);
    }

    public void ReloadSolutions()
    {
        this.Solutions = new()
        {
            {2015, Y2015.Loader.GetProblems()},
            {2019, Y2019.Loader.GetProblems()},
            {2020, Y2020.Loader.GetProblems()},
            {2021, Y2021.Loader.GetProblems()},
            {2022, Y2022.Loader.GetProblems()},
            {2023, Y2023.Loader.GetProblems()},
            {2024, Y2024.Loader.GetProblems()},
        };
    }

    private static void ProblemPrompt(Dictionary<int, Dictionary<int, APuzzle>> solutions)
    {
        while (true) {
            APuzzle day;
            int part;
            var years = solutions.Keys;
            var selectedYear = Prompt.Select("Select a year to run", years, years.Last(), 10, y => $"Year {y}");

            var orderedSolutions = solutions[selectedYear].OrderBy(s => s.Value.Day).ToList();

            var days = orderedSolutions.Select(s => s.Value).ToArray();
            day = Prompt.Select("Select day to run", days, days.Last(), 26, d => $"Day {d.Day} -- {d.Title}");

            part = Prompt.Select("Select part to run",
                new [] {0, 1, 2},
                0,
                5,
                p => p == 0 ? "Run All" : $"Part {p}");

            day.Solve(part);

            if (Prompt.Confirm("Submit answers?", false)) {
                // Console.WriteLine(submitAnswer(day.Year, day.Day, part, puzzle.Part1).Result);
            }

            if (!Prompt.Confirm("Run Another?", true)) {
                break;
            }
        }
    }

    private async Task<string> submitAnswer(int year, int day, int part, string answer)
    {
        var content = new FormUrlEncodedContent(new Dictionary<string, string>()
        {
            {"level", part.ToString()},
            {"answer", answer}
        });
        content.Headers.Add("cookie", $"session={session.Value}");

        Console.WriteLine("Submitting....");

        var response = await client.PostAsync(
            $"https://adventofcode.com/{year}/day/{day}/answer",
            content);

        if ((await response.Content.ReadAsStringAsync()).Contains("That's not the right answer")) {
            return "Uh Oh! That's not the right answer".Pastel(Color.Red);
        }

        // Open part 2 in browser
        if (part == 1) {
            // https://adventofcode.com/2021/day/10#part2
            Process.Start($"https://adventofcode.com/{year}/day/{day}#part2");
        }

        return "Awesome! Have a ".Pastel(Color.Green) + "Gold Star".Pastel(Color.Goldenrod);
    }
}
