namespace AdventOfCode;

internal class Program
{
    public static void Main(string[] args)
    {
        var runner = new Runner();
        runner.Run(args.Where(l => l != "watch" && l != "run").ToArray());
    }
}